package adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import auroraservicios.es.aurora.ActivityCompany;
import auroraservicios.es.aurora.R;
import modelos.Company;

public class CompanyAdapter extends BaseAdapter {

    ArrayList<Company> companies;
    Activity activity;
    String cp;

    public CompanyAdapter(Activity activity, ArrayList<Company> companies, String cp) {
        this.companies = companies;
        this.activity = activity;
        this.cp = cp;
    }

    @Override
    public int getCount() {
        return companies.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (view==null){
            LayoutInflater layoutInflater=(LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=layoutInflater.inflate(R.layout.item_section,null);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            view.setLayoutParams(params);
        }

        final Company company = companies.get(i);
        final ImageView bCompany;
        final TextView tvCompany;
        final ConstraintLayout rlItem;

        rlItem = view.findViewById(R.id.rlItem);

        bCompany = view.findViewById(R.id.bSection);
        tvCompany = view.findViewById(R.id.tvSection);
        tvCompany.setText(company.getName());
        tvCompany.setGravity(Gravity.CENTER | Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

        if(company.getImage() == null || company.getImage() == ""
                || company.getImage().equals(null) || company.getImage().equals("")){
            bCompany.setVisibility(View.GONE);
            rlItem.setBackgroundResource(R.drawable.button_radius_gradient_02);
        }else{
            String urlPhoto = "http://www.servicios.auroraservicios.es/uploads/images/empresas/"+
                    company.getPath()+company.getImage();
            urlPhoto = urlPhoto.replaceAll(" ", "%20");
            Picasso.get()
                    .load(urlPhoto)
                    .resize(900, 350)
                    .centerInside()
                    .into(bCompany);

            ViewGroup.LayoutParams params = bCompany.getLayoutParams();
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            bCompany.setLayoutParams(params);
            tvCompany.setVisibility(View.GONE);
        }

        bCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, ActivityCompany.class);
                i.putExtra("company", (Parcelable) company);
                activity.startActivity(i);
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, ActivityCompany.class);
                i.putExtra("company", (Parcelable) company);
                activity.startActivity(i);
            }
        });

        return view;
    }
}
