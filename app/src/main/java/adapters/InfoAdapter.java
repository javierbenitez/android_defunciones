package adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import auroraservicios.es.aurora.ActivityInfo;
import auroraservicios.es.aurora.R;
import modelos.Info;

public class InfoAdapter extends BaseAdapter {

    ArrayList<Info> infos;
    Activity activity;
    String cp;

    public InfoAdapter(Activity activity, ArrayList<Info> info, String cp) {
        this.infos = info;
        this.activity = activity;
        this.cp = cp;
    }

    @Override
    public int getCount() {
        return infos.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.item_info, null);
        }

        final Info info = infos.get(i);
        final TextView bPhone;
        final TextView tvName, tvAddress, tvSchedule, tvFilibeg, tv_Filibeg;
        final ImageView ivInfo;

        tvName = view.findViewById(R.id.tvName);
        tvName.setText(info.getName());
        bPhone = view.findViewById(R.id.tvPhone);
        ivInfo = view.findViewById(R.id.ivInfo);

        SpannableString textUnderline = new SpannableString(info.getPhone1());
        textUnderline.setSpan(new UnderlineSpan(), 0, textUnderline.length(), 0);
        bPhone.setText(textUnderline);
        tvAddress = view.findViewById(R.id.tvAddress);
        tvAddress.setText(info.getAddress());
        tvSchedule = view.findViewById(R.id.tvSchedule);
        tvSchedule.setText(info.getSchedule());
        tvFilibeg = view.findViewById(R.id.tvFilibeg);
        tv_Filibeg = view.findViewById(R.id.tv_Filibeg);

        if(info.getFilibeg() != "" && info.getFilibeg() != null){
            tvFilibeg.setText(info.getFilibeg());
        }else{
            tv_Filibeg.setVisibility(View.GONE);
            tvFilibeg.setVisibility(View.GONE);
        }

        Log.w("PATH", String.valueOf(info.getPath()));
        Log.w("IMG", String.valueOf(info.getImage()));
        if(info.getImage() == null || info.getImage() == ""
                || info.getImage().equals(null) || info.getImage().equals("")){
            ivInfo.setVisibility(View.GONE);
        }else{
            String urlPhoto = "http://www.servicios.auroraservicios.es/uploads/images/info/"+info.getPath()+info.getImage();
            urlPhoto = urlPhoto.replaceAll(" ", "%20");

            Picasso.get()
                    .load(urlPhoto)
                    .resize(320, 320)
                    .centerCrop()
                    .into(ivInfo);
        }

        bPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int permissionCheck = ContextCompat.checkSelfPermission(
                        activity, Manifest.permission.CALL_PHONE);
                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, 225);
                } else {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + bPhone.getText().toString()));
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(callIntent);
                }
            }
        });

        tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, ActivityInfo.class);
                i.putExtra("info", info);
                activity.startActivity(i);
            }
        });

        tvAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, ActivityInfo.class);
                i.putExtra("info", info);
                activity.startActivity(i);
            }
        });

        return view;
    }
}
