package adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import auroraservicios.es.aurora.ActivityEvent;
import auroraservicios.es.aurora.ActivityPartOfPlace;
import auroraservicios.es.aurora.ActivityPlace;
import auroraservicios.es.aurora.R;
import modelos.Event;
import modelos.Place;

public class PlaceAdapter extends BaseAdapter {

    ArrayList<Place> places;
    Context context ;
    Activity activity;
    String cp;

    public PlaceAdapter(Activity activity, ArrayList<Place> places, String cp) {
        this.places = places;
        this.activity = activity;
        this.context = activity;
        this.cp = cp;
    }

    @Override
    public int getCount() {
        return places.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        final Intent intent;

        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.item_place, null);
        }

        final Place place = places.get(i);
        final TextView tvTitle, tvDescription, tvLocation;
        final ImageView ivImg;

        tvTitle = view.findViewById(R.id.tvPlace);
        tvDescription = view.findViewById(R.id.tvDescription);
        ivImg = view.findViewById(R.id.ivImg);
        tvLocation = view.findViewById(R.id.tvLocation);

        tvTitle.setText(place.getTitle());
        if(place.getDescription() != "") {
            String description = place.getDescription();
            if(place.getDescription().length() > 50){
                description = place.getDescription().substring(0, 50)+"...";
            }
            tvDescription.setText(description);
        }
//        if(place.getLatitud() != "" && place.getLongitud() != ""){
//            tvLocation.setText(place.getLatitud()+" "+place.getLongitud());
//            tvLocation.setMovementMethod(LinkMovementMethod.getInstance());
//            tvLocation.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    String uri = String.format(Locale.ENGLISH, "geo:%f,%f", place.getLatitud(), place.getLongitud());
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
//                    context.startActivity(intent);
//                }
//            });
//        }

        if(place.getImage() == null || place.getImage() == "" ){
            ivImg.setVisibility(View.GONE);
        }else{
            String urlPhoto = "http://www.servicios.auroraservicios.es/uploads/images/lugares/"+place.getPath()
//            String urlPhoto = "https://www.auroraservicios.es/jbdodev1/auroraservicios/web/uploads/images/lugares/"+place.getPath()
                    +place.getImage();
            urlPhoto = urlPhoto.replaceAll(" ", "%20");

            Picasso.get()
                    .load(urlPhoto)
                    .resize(150, 230)
                    .centerCrop()
                    .into(ivImg);

        }


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eventClick(place);
            }
        });

        return view;
    }

    private void eventClick(Place place) {
        Intent i;
        if(place.getSubsection().getName() != "LUGAR") {
            i = new Intent(activity, ActivityPlace.class);
        }else{
            i = new Intent(activity, ActivityPartOfPlace.class);
        }
        i.putExtra("cp", cp);
        i.putExtra("place", (Parcelable) place);
        activity.startActivity(i);
    }
}
