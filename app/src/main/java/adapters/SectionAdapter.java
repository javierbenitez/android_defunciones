package adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import auroraservicios.es.aurora.ActivityAsociations;
import auroraservicios.es.aurora.ActivityCalendar;
import auroraservicios.es.aurora.ActivitySubsections;
import auroraservicios.es.aurora.ActivityNewspaper;
import auroraservicios.es.aurora.R;
import modelos.Section;

public class SectionAdapter extends BaseAdapter {

    ArrayList<Section> sections;
    Activity activity;
    String cp;

    public SectionAdapter(Activity activity, ArrayList<Section> sections, String cp) {
        this.sections = sections;
        this.activity = activity;
        this.cp = cp;
    }

    @Override
    public int getCount() {
        return sections.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;

        if (view==null){
            LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.item_section,null);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            view.setLayoutParams(params);
        }

        final Section section = sections.get(i);
        final ImageView bSection;
        final TextView tvSection;
        final ConstraintLayout rlItem;

        rlItem = view.findViewById(R.id.rlItem);
        bSection = view.findViewById(R.id.bSection);
        tvSection = view.findViewById(R.id.tvSection);
        tvSection.setText(section.getName());
        tvSection.setGravity(Gravity.CENTER | Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

        if(section.getImage() == null || section.getImage() == ""
                || section.getImage().equals(null) || section.getImage().equals("")){
            bSection.setVisibility(View.GONE);
            rlItem.setBackgroundResource(R.drawable.button_radius_gradient_02);
        }else{
            String urlPhoto = "http://www.servicios.auroraservicios.es/uploads/images/secciones/"
//            String urlPhoto = "https://www.auroraservicios.es/jbdodev1/auroraservicios/web/uploads/images/secciones/"
                    +section.getPath()+section.getImage();
            urlPhoto = urlPhoto.replaceAll(" ", "%20");

            Picasso.get()
                    .load(urlPhoto)
                    .resize(900, 350)
                    .centerInside()
                    .into(bSection);

            ViewGroup.LayoutParams params = bSection.getLayoutParams();
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            bSection.setLayoutParams(params);
            tvSection.setVisibility(View.GONE);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eventClick(section);
            }
        });

//        bSection.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                eventClick(section);
//            }
//        });

        return view;
    }

    private void eventClick(Section section) {
        Intent i;
        if(section.isBrochure()){
            i = new Intent(activity, ActivityNewspaper.class);
            i.putExtra("section", section.getName());
            i.putExtra("path", section.getPath());
            i.putExtra("newspaper", section.getNewspaper());
            i.putExtra("id_newspaper", section.getId());
            i.putExtra("cp", cp);
            activity.startActivity(i);
        }else{
            switch (section.getName()){
                case "Asociaciones":
                    i = new Intent(activity, ActivityCalendar.class);
                    i.putExtra("subsection", section.getName());
                    i.putExtra("cp", cp);
                    activity.startActivity(i);
                    break;
                default:
                    i = new Intent(activity, ActivitySubsections.class);
                    i.putExtra("section", section.getName());
                    i.putExtra("cp", cp);
                    activity.startActivity(i);
                    break;
            }
        }

    }
}
