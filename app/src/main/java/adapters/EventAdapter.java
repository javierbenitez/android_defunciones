package adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import auroraservicios.es.aurora.ActivityEvent;
import auroraservicios.es.aurora.ActivityEvents;
import auroraservicios.es.aurora.ActivityInfo;
import auroraservicios.es.aurora.R;
import modelos.Event;
import modelos.Info;
import modelos.Subsection;

public class EventAdapter extends BaseAdapter {

    ArrayList<Event> events;
    Context context ;
    Activity activity;
    String cp;

    public EventAdapter(Activity activity, ArrayList<Event> event, String cp) {
        this.events = event;
        this.activity = activity;
        this.context = activity;
        this.cp = cp;
    }

    @Override
    public int getCount() {
        return events.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        final Intent intent;

        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.item_event, null);
        }

        final Event event = events.get(i);
        final TextView tvTitle, tvSubtitle, tvTimeEvent, tvLocation;
        final ImageView ivImg;
        final String date;

        tvTitle = view.findViewById(R.id.tvTitle);
        ivImg = view.findViewById(R.id.ivImg);
        tvSubtitle = view.findViewById(R.id.tvSubtitle);
        tvTimeEvent = view.findViewById(R.id.tvTimeEvent);
        tvLocation = view.findViewById(R.id.tvLocation);

        tvTitle.setText(event.getTitle());
        tvSubtitle.setText(event.getSubtitle());
        if(event.getDateEndEvent() != null && event.getDateEndEvent() != ""){
            date = "De "+event.getTimeEvent()+" del "+event.getDateEvent()+" a "+event.getTimeEndEvent()+
            " del "+event.getDateEndEvent();
        }else{
            date = "El "+event.getDateEvent()+", todo el día";
        }
        tvTimeEvent.setText(date);
        String location = "En "+event.getLocalidad();
        if(event.getLocation() != "" && event.getLocation() != null){
            location += ", "+event.getLocation();
        }
        tvLocation.setText(location);

        if(event.getImage() == null || event.getImage() == "" ){
            ivImg.setVisibility(View.GONE);
        }else{
            String urlPhoto;
            if(event.getSubsection().getName().equals("Asociaciones")){
                urlPhoto = "http://www.servicios.auroraservicios.es/uploads/images/"+event.getPath()+event.getImage();
            }else{
                urlPhoto = "http://www.servicios.auroraservicios.es/uploads/images/eventos/"+event.getPath()+event.getImage();
            }
            urlPhoto = urlPhoto.replaceAll(" ", "%20");

            Picasso.get()
                    .load(urlPhoto)
                    .resize(330, 470)
                    .centerCrop()
                    .into(ivImg);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eventClick(event);
            }
        });

        return view;
    }

    private void eventClick(Event event) {
        Intent i;
        i = new Intent(activity, ActivityEvent.class);
        i.putExtra("cp", cp);
        i.putExtra("event", (Parcelable) event);
        i.putExtra("subsection", event.getSubsection().getName());
        activity.startActivity(i);
    }
}
