package adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import auroraservicios.es.aurora.ActivityCalendar;
import auroraservicios.es.aurora.ActivityCompanies;
import auroraservicios.es.aurora.ActivityInfos;
import auroraservicios.es.aurora.ActivityPlaces;
import auroraservicios.es.aurora.CrearMisa;
import auroraservicios.es.aurora.Listar_Misas;
import auroraservicios.es.aurora.Listar_Publicaciones;
import auroraservicios.es.aurora.R;
import modelos.Subsection;

public class SubsectionAdapter extends BaseAdapter {

    ArrayList<Subsection> subsections;
    Activity activity;
    String cp;

    public SubsectionAdapter(Activity activity, ArrayList<Subsection> subsections, String cp) {
        this.subsections = subsections;
        this.activity = activity;
        this.cp = cp;
    }

    @Override
    public int getCount() {
        return subsections.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (view==null){
            LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=layoutInflater.inflate(R.layout.item_section,null);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            view.setLayoutParams(params);
        }

        final Subsection subsection = subsections.get(i);
        final ImageView bSubsection = view.findViewById(R.id.bSection);
        final TextView tvSubsection = view.findViewById(R.id.tvSection);
        final ConstraintLayout rlItem;

        rlItem = view.findViewById(R.id.rlItem);

        if(subsection.getImage() == null || subsection.getImage() == ""
                || subsection.getImage().equals(null) || subsection.getImage().equals("")){
            bSubsection.setVisibility(View.GONE);
            rlItem.setBackgroundResource(R.drawable.button_radius_gradient_02);
        }else{
            String urlPhoto = "http://www.servicios.auroraservicios.es/uploads/images/secciones/"
//            String urlPhoto = "https://www.auroraservicios.es/jbdodev1/auroraservicios/web/uploads/images/secciones/"
                    +subsection.getPath()+subsection.getImage();
            urlPhoto = urlPhoto.replaceAll(" ", "%20");
            Picasso.get()
                    .load(urlPhoto)
                    .resize(900, 350)
                    .centerInside()
                    .into(bSubsection);

            ViewGroup.LayoutParams params = bSubsection.getLayoutParams();
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            bSubsection.setLayoutParams(params);
            tvSubsection.setVisibility(View.GONE);
        }

        tvSubsection.setText(subsection.getName());
        tvSubsection.setGravity(Gravity.CENTER | Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eventClick(subsection);
            }
        });

        return view;
    }

    private void eventClick(Subsection subsection) {

        Intent i;
        switch (subsection.getName()){
            case "Defunciones":
                i = new Intent(activity, Listar_Publicaciones.class);
                i.putExtra("cp", cp);
                activity.startActivity(i);
                break;
            case "Misas":
                i = new Intent(activity, Listar_Misas.class);
                i.putExtra("cp", cp);
                activity.startActivity(i);
                break;
            case "Crear misa":
            case "Crear Misa":
                i = new Intent(activity, CrearMisa.class);
                activity.startActivity(i);
                break;
            case "Farmacias":
            case "Teléfonos de interés":
                i = new Intent(activity, ActivityInfos.class);
                i.putExtra("subsection", subsection.getName());
                i.putExtra("cp", cp);
                activity.startActivity(i);
                break;
            case "Agenda":
            case "Fiestas":
            case "Información local":
                i = new Intent(activity, ActivityCalendar.class);
                i.putExtra("subsection", subsection.getName());
                i.putExtra("cp", cp);
                activity.startActivity(i);
                break;
            case "Turismo":
                i = new Intent(activity, ActivityPlaces.class);
                i.putExtra("subsection", subsection.getName());
                i.putExtra("cp", cp);
                activity.startActivity(i);
                break;
            case "Crear festividad":
                openWebAurora(2);
                break;
            case "Crear evento":
                openWebAurora(1);
                break;
            case "Crear":
                openWebAurora(3);
                break;
            case "Crear punto":
                openWebAurora(4);
                break;
            default:
                i = new Intent(activity, ActivityCompanies.class);
                i.putExtra("subsection", subsection.getName());
                i.putExtra("cp", cp);
                activity.startActivity(i);
                break;
        }
    }

    private void openWebAurora(int type){
        String url = "http://servicios.auroraservicios.es/eventos/";
        switch (type){
            case 1:
                url = "http://www.servicios.auroraservicios.es/eventos/evento/35";
                break;
            case 2:
                url = "http://www.servicios.auroraservicios.es/eventos/evento/34";
                break;
            case 3:
                url = "http://www.servicios.auroraservicios.es/eventos/evento/36";
                break;
            case 4:
                url = "http://www.servicios.auroraservicios.es/eventos/evento/37";
                break;
        }
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        activity.startActivity(i);
    }
}
