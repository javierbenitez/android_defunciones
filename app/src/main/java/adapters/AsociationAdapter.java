package adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import auroraservicios.es.aurora.ActivityAsociation;
import auroraservicios.es.aurora.R;
import modelos.Asociation;

public class AsociationAdapter extends BaseAdapter {

    ArrayList<Asociation> asociations;
    Context context ;
    Activity activity;
    String cp;

    public AsociationAdapter(Activity activity, ArrayList<Asociation> asociations, String cp) {
        this.asociations = asociations;
        this.activity = activity;
        this.context = activity;
        this.cp = cp;
    }

    @Override
    public int getCount() {
        return asociations.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        final Intent intent;

        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.item_asociation, null);
        }

        final Asociation asociation = asociations.get(i);
        final TextView tvTitle;
        final ImageView ivImg;
        final ConstraintLayout rlItem;

        rlItem = view.findViewById(R.id.rlItem);
        tvTitle = view.findViewById(R.id.tvAsociation);
        ivImg = view.findViewById(R.id.ivImg);

        tvTitle.setText(asociation.getTitle());

        if(asociation.getImages() == null || asociation.getImages().length < 1 ){
            ivImg.setVisibility(View.GONE);
            rlItem.setBackgroundResource(R.drawable.button_radius_gradient_02);
        }else{
            String urlPhoto = "http://www.servicios.auroraservicios.es/uploads/images/asociaciones/"+asociation.getPath()
                    +asociation.getImages()[0];
            urlPhoto = urlPhoto.replaceAll(" ", "%20");

            Picasso.get()
                    .load(urlPhoto)
                    .resize(900, 350)
                    .centerInside()
                    .into(ivImg);

            ViewGroup.LayoutParams params = ivImg.getLayoutParams();
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            ivImg.setLayoutParams(params);
            ivImg.setVisibility(View.GONE);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eventClick(asociation);
            }
        });

        return view;
    }

    private void eventClick(Asociation asociation) {
        Intent i = new Intent(activity, ActivityAsociation.class);
        i.putExtra("cp", cp);
        i.putExtra("asociation", (Parcelable) asociation);
        activity.startActivity(i);
    }
}
