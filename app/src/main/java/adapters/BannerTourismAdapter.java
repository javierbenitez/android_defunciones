package adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import auroraservicios.es.aurora.ActivityImage;
import auroraservicios.es.aurora.ActivityImageEvent;
import auroraservicios.es.aurora.R;
import auroraservicios.es.aurora.WebViewPublish;

public class BannerTourismAdapter extends PagerAdapter {

    private ArrayList<String> Imagenes;
    private LayoutInflater inflater;
    private Context context;

    public BannerTourismAdapter(Context context, ArrayList<String> Imagenes) {
        this.context = context;
        this.Imagenes = Imagenes;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        int t = Imagenes.size();
        return t;
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View myImageLayout = inflater.inflate(R.layout.banner, view, false);
        ImageButton myImage = myImageLayout.findViewById(R.id.imagen);

        final String imagePlace = Imagenes.get(position);
        String foto = "http://www.servicios.auroraservicios.es/uploads/images/lugares/"+imagePlace;
//        String foto = "https://www.auroraservicios.es/jbdodev1/auroraservicios/web/uploads/images/lugares/"+imagePlace;

        if(foto.indexOf("./") > 0){
            foto = foto.replace("./", "/");
        }

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int width = metrics.widthPixels; // ancho absoluto en pixels
        int height = myImageLayout.getHeight();

        Picasso.get()
                .load(foto)
                .resize(width, height)
                .centerCrop()
                .into(myImage);

        myImage.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                String pathImage = "https://servicios.auroraservicios.es/uploads/images/lugares/"+imagePlace;
//                String pathImage = "https://auroraservicios.es/jbdodev1/auroraservicios/web/uploads/images/lugares/"+imagePlace;
                Intent i = new Intent(context, WebViewPublish.class);
                i.putExtra("url", pathImage);
                context.startActivity(i);
            }
        });

        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

}