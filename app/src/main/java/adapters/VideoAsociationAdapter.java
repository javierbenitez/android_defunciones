package adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Parcelable;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import auroraservicios.es.aurora.ActivityEvent;
import auroraservicios.es.aurora.ActivityVideoAsociation;
import auroraservicios.es.aurora.R;
import modelos.Place;
import modelos.VideoAsociation;

public class VideoAsociationAdapter extends BaseAdapter {

    ArrayList<VideoAsociation> videoAsociations;
    Context context ;
    Activity activity;
    String cp;

    public VideoAsociationAdapter(Activity activity, ArrayList<VideoAsociation> videoAsociations, String cp) {
        this.videoAsociations = videoAsociations;
        this.activity = activity;
        this.context = activity;
        this.cp = cp;
    }

    @Override
    public int getCount() {
        return videoAsociations.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        final Intent intent;

        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.item_videoasociation, null);
        }

        final VideoAsociation videoAsociation = videoAsociations.get(i);
        final TextView tvTitle, tvDescription, tvUpdated;

        tvTitle = view.findViewById(R.id.tvTitle);
        tvDescription = view.findViewById(R.id.tvDescription);
        tvUpdated = view.findViewById(R.id.tvUpdated);
        tvTitle.setText(videoAsociation.getTitle());
        tvUpdated.setText(videoAsociation.getUpdated());

        if(videoAsociation.getDescription() != "") {
            tvDescription.setText(videoAsociation.getDescription());
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eventClick(videoAsociation);
            }
        });

        return view;
    }

    private void eventClick(VideoAsociation videoAsociation) {
//        Toast.makeText("Pulsado video:"+String.valueOf(videoAsociation.getTitle()), Toast.LENGTH_LONG).show();
        Intent i;
        i = new Intent(activity, ActivityVideoAsociation.class);
        i.putExtra("video_src", videoAsociation.getPath()+videoAsociation.getVideo());
        i.putExtra("path_asociation", videoAsociation.getAsociation().getPath());
        activity.startActivity(i);
    }
}
