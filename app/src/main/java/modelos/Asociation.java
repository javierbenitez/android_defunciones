package modelos;

import android.os.Parcel;
import android.os.Parcelable;

public class Asociation implements Parcelable {

    private int id;
    private String title;
    private String description;
    private String location;
    private String localidad;
    private String image1;
    private String image2;
    private String image3;
    private String video;
    private String[] images;
    private String updated;
    private Section section;
    private String path;

    public Asociation(){
    };
    public Asociation(String name){
        this.title = name;
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String[] getImages() {
        return images;
    }

    public String getImage1() {
        return image1;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.title);
        parcel.writeString(this.location);
        parcel.writeString(this.description);
        parcel.writeString(this.video);
        parcel.writeString(this.image1);
        parcel.writeString(this.image2);
        parcel.writeString(this.image3);
//        parcel.writeString(this.getSubsection().getName());
        parcel.writeString(this.localidad);
        parcel.writeString(this.path);
    }

    public Asociation(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.location = in.readString();
        this.description = in.readString();
        this.video = in.readString();
        this.image1 = in.readString();
        this.image2 = in.readString();
        this.image3 = in.readString();
        this.localidad = in.readString();
        this.path = in.readString();
    }

    public static final Parcelable.Creator<Asociation> CREATOR = new Parcelable.Creator<Asociation>() {

        @Override
        public Asociation createFromParcel(Parcel parcel) {
            return new Asociation(parcel);
        }

        @Override
        public Asociation[] newArray(int i) {
            return new Asociation[0];
        }
    };
}
