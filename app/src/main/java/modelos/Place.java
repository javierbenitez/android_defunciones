package modelos;

import android.os.Parcel;
import android.os.Parcelable;

public class Place implements Parcelable {

    private int id;
    private String title;
    private String latitud;
    private String longitud;
    private String description;
    private String video;
    private String audio;
    private String[] images;
    private String image;
    private String localidad;
    private String cp;
    private String path;
    private String updated;
    private String link;
    private int totalParts;
    private Subsection subsection;

    public Place() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getLink() {
        return link;
    }

    public int getTotalParts() {
        return totalParts;
    }

    public void setTotalParts(int totalParts) {
        this.totalParts = totalParts;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Subsection getSubsection() {
        return subsection;
    }

    public void setSubsection(Subsection subsection) {
        this.subsection = subsection;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.title);
        parcel.writeString(this.description);
        parcel.writeString(this.video);
        parcel.writeString(this.audio);
        parcel.writeString(this.latitud);
        parcel.writeString(this.longitud);
        parcel.writeString(this.localidad);
        parcel.writeString(this.path);
        parcel.writeString(this.image);
        parcel.writeString(this.cp);
        parcel.writeString(this.link);
        parcel.writeInt(this.totalParts);
    }

    public Place(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.description = in.readString();
        this.video = in.readString();
        this.audio = in.readString();
        this.latitud = in.readString();
        this.longitud = in.readString();
        this.localidad = in.readString();
        this.path = in.readString();
        this.image = in.readString();
        this.cp = in.readString();
        this.link = in.readString();
        this.totalParts = in.readInt();
    }

    public static final Creator<Place> CREATOR = new Creator<Place>() {

        @Override
        public Place createFromParcel(Parcel parcel) {
            return new Place(parcel);
        }

        @Override
        public Place[] newArray(int i) {
            return new Place[0];
        }
    };
}
