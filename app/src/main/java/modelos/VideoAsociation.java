package modelos;

import android.os.Parcel;
import android.os.Parcelable;

public class VideoAsociation implements Parcelable {

    private int id;
    private String title;
    private String description;
    private String video;
    private String path;
    private String updated;
    private Asociation asociation;

    public VideoAsociation() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Asociation getAsociation() {
        return asociation;
    }

    public void setAsociation(Asociation asociation) {
        this.asociation = asociation;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.title);
        parcel.writeString(this.description);
        parcel.writeString(this.video);
        parcel.writeString(this.path);
        parcel.writeString(this.updated);
    }

    public VideoAsociation(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.description = in.readString();
        this.video = in.readString();
        this.path = in.readString();
        this.updated = in.readString();
    }

    public static final Creator<VideoAsociation> CREATOR = new Creator<VideoAsociation>() {

        @Override
        public VideoAsociation createFromParcel(Parcel parcel) {
            return new VideoAsociation(parcel);
        }

        @Override
        public VideoAsociation[] newArray(int i) {
            return new VideoAsociation[0];
        }
    };
}
