package modelos;

import android.os.Parcel;
import android.os.Parcelable;

public class Info implements Parcelable {

    private int id;
    private String name;
    private String description;
    private String filibeg;
    private String schedule;
    private String phone1;
    private String phone2;
    private String address;
    private String url;
    private String localidad;
    private String cp;
    private Subsection subsection;
    private String image;
    private String path;
    private String updated;

    public Info(){};

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFilibeg() {
        return filibeg;
    }

    public void setFilibeg(String filibeg) {
        this.filibeg = filibeg;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public Subsection getSubsection() {
        return subsection;
    }

    public void setSubsection(Subsection subsection) {
        this.subsection = subsection;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.name);
        parcel.writeString(this.description);
        parcel.writeString(this.filibeg);
        parcel.writeString(this.schedule);
        parcel.writeString(this.phone1);
        parcel.writeString(this.phone2);
        parcel.writeString(this.address);
        parcel.writeString(this.url);
        parcel.writeString(this.localidad);
        parcel.writeString(this.image);
        parcel.writeString(this.path);
        parcel.writeString(this.cp);
    }

    public Info(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.description = in.readString();
        this.filibeg = in.readString();
        this.schedule = in.readString();
        this.phone1 = in.readString();
        this.phone2 = in.readString();
        this.address = in.readString();
        this.url = in.readString();
        this.localidad = in.readString();
        this.image = in.readString();
        this.path = in.readString();
        this.cp = in.readString();
    }

    public static final Parcelable.Creator<Info> CREATOR = new Parcelable.Creator<Info>() {

        @Override
        public Info createFromParcel(Parcel parcel) {
            return new Info(parcel);
        }

        @Override
        public Info[] newArray(int i) {
            return new Info[0];
        }
    };
}
