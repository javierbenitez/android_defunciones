package modelos;

public class Section {

    private int id;
    private String name;
    private String updated;
    private String image;
    private String path;
    private String newspaper = "";
    private boolean brochure = false;

    public Section(String name ){
        this.name = name;
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isBrochure() {
        return brochure;
    }

    public void setBrochure(boolean brochure) {
        this.brochure = brochure;
    }

    public String getNewspaper() {
        return newspaper;
    }

    public void setNewspaper(String newspaper) {
        this.newspaper = newspaper;
    }
}
