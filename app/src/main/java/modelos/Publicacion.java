package modelos;

import java.io.Serializable;

public class Publicacion implements Serializable {
    String identificador,nombre, apellidos, apodo, direccion, fecha_fallecimiento, marido, esposa, hijos, hijos_politicos, hermanos,
            hermanos_politicos, familiares, pareja, foto, foto2, ceremonia, lugar, hora, texto,d_recibe,d_despide,lugar_iglesia,edad,
            localidadCerem2, iglesiaCerem2, fechaCerem2, horaCerem2, padres, phone1, phone2;

    public Publicacion(String identificador, String nombre, String apellidos, String apodo, String direccion, String fecha_fallecimiento,
                       String marido, String esposa, String hijos, String hijos_politicos, String hermanos, String hermanos_politicos,
                       String familiares, String pareja, String foto, String ceremonia, String lugar, String hora, String texto,
                       String localidadCerem2, String iglesiaCerem2, String fechaCerem2, String horaCerem2, String padres) {
        this.identificador = identificador;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.apodo = apodo;
        this.direccion = direccion;
        this.fecha_fallecimiento = fecha_fallecimiento;
        this.marido = marido;
        this.esposa = esposa;
        this.hijos = hijos;
        this.hijos_politicos = hijos_politicos;
        this.hermanos = hermanos;
        this.hermanos_politicos = hermanos_politicos;
        this.familiares = familiares;
        this.pareja = pareja;
        this.foto = foto;
        this.ceremonia = ceremonia;
        this.lugar = lugar;
        this.hora = hora;
        this.texto = texto;
        this.localidadCerem2 = localidadCerem2;
        this.iglesiaCerem2 = iglesiaCerem2;
        this.fechaCerem2 = fechaCerem2;
        this.horaCerem2 = horaCerem2;
        this.padres = padres;
    }

    public Publicacion(String identificador, String nombre, String apellidos, String apodo, String fecha_fallecimiento,
                       String marido, String esposa, String hijos, String hijos_politicos, String hermanos,
                       String hermanos_politicos, String familiares, String pareja, String foto, String ceremonia,
                       String lugar, String hora, String texto, String d_recibe, String d_despide, String lugar_iglesia,
                       String edad) {
        this.identificador = identificador;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.apodo = apodo;
        this.fecha_fallecimiento = fecha_fallecimiento;
        this.marido = marido;
        this.esposa = esposa;
        this.hijos = hijos;
        this.hijos_politicos = hijos_politicos;
        this.hermanos = hermanos;
        this.hermanos_politicos = hermanos_politicos;
        this.familiares = familiares;
        this.pareja = pareja;
        this.foto = foto;
        this.ceremonia = ceremonia;
        this.lugar = lugar;
        this.hora = hora;
        this.texto = texto;
        this.d_recibe = d_recibe;
        this.d_despide = d_despide;
        this.lugar_iglesia = lugar_iglesia;
        this.edad = edad;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFoto2() {
        return foto2;
    }

    public void setFoto2(String foto2) {
        this.foto2 = foto2;
    }

    public String getLocalidadCerem2() {
        return localidadCerem2;
    }

    public void setLocalidadCerem2(String localidadCerem2) {
        this.localidadCerem2 = localidadCerem2;
    }

    public String getIglesiaCerem2() {
        return iglesiaCerem2;
    }

    public void setIglesiaCerem2(String iglesiaCerem2) {
        this.iglesiaCerem2 = iglesiaCerem2;
    }

    public String getFechaCerem2() {
        return fechaCerem2;
    }

    public void setFechaCerem2(String fechaCerem2) {
        this.fechaCerem2 = fechaCerem2;
    }

    public String getHoraCerem2() {
        return horaCerem2;
    }

    public void setHoraCerem2(String horaCerem2) {
        this.horaCerem2 = horaCerem2;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getD_recibe() {
        return d_recibe;
    }

    public void setD_recibe(String d_recibe) {
        this.d_recibe = d_recibe;
    }

    public String getD_despide() {
        return d_despide;
    }

    public void setD_despide(String d_despide) {
        this.d_despide = d_despide;
    }

    public String getLugar_iglesia() {
        return lugar_iglesia;
    }

    public void setLugar_iglesia(String lugar_iglesia) {
        this.lugar_iglesia = lugar_iglesia;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public Publicacion() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getApodo() {
        return apodo;
    }

    public void setApodo(String apodo) {
        this.apodo = apodo;
    }

    public String getFecha_fallecimiento() {
        return fecha_fallecimiento;
    }

    public void setFecha_fallecimiento(String fecha_fallecimiento) {
        this.fecha_fallecimiento = fecha_fallecimiento;
    }

    public String getMarido() {
        return marido;
    }

    public void setMarido(String marido) {
        this.marido = marido;
    }

    public String getEsposa() {
        return esposa;
    }

    public void setEsposa(String esposa) {
        this.esposa = esposa;
    }

    public String getHijos() {
        return hijos;
    }

    public void setHijos(String hijos) {
        this.hijos = hijos;
    }

    public String getHijos_politicos() {
        return hijos_politicos;
    }

    public void setHijos_politicos(String hijos_politicos) {
        this.hijos_politicos = hijos_politicos;
    }

    public String getHermanos() {
        return hermanos;
    }

    public void setHermanos(String hermanos) {
        this.hermanos = hermanos;
    }

    public String getHermanos_politicos() {
        return hermanos_politicos;
    }

    public void setHermanos_politicos(String hermanos_politicos) {
        this.hermanos_politicos = hermanos_politicos;
    }

    public String getFamiliares() {
        return familiares;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public void setFamiliares(String familiares) {
        this.familiares = familiares;
    }

    public String getPareja() {
        return pareja;
    }

    public void setPareja(String pareja) {
        this.pareja = pareja;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getCeremonia() {
        return ceremonia;
    }

    public void setCeremonia(String ceremonia) {
        this.ceremonia = ceremonia;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getPadres() {
        return padres;
    }

    public void setPadres(String padres) {
        this.padres = padres;
    }

    @Override
    public String toString() {
        return "Publicacion{" +
                "identificador='" + identificador + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", apodo='" + apodo + '\'' +
                ", fecha_fallecimiento='" + fecha_fallecimiento + '\'' +
                ", marido='" + marido + '\'' +
                ", esposa='" + esposa + '\'' +
                ", hijos='" + hijos + '\'' +
                ", hijos_politicos='" + hijos_politicos + '\'' +
                ", hermanos='" + hermanos + '\'' +
                ", hermanos_politicos='" + hermanos_politicos + '\'' +
                ", familiares='" + familiares + '\'' +
                ", pareja='" + pareja + '\'' +
                ", foto='" + foto + '\'' +
                ", ceremonia='" + ceremonia + '\'' +
                ", lugar='" + lugar + '\'' +
                ", hora='" + hora + '\'' +
                ", texto='" + texto + '\'' +
                ", d_recibe='" + d_recibe + '\'' +
                ", d_despide='" + d_despide + '\'' +
                ", lugar_iglesia='" + lugar_iglesia + '\'' +
                ", edad='" + edad + '\'' +
                ", foto2='" + foto2 + '\'' +
                ", contact_phone1='" + phone1 + '\'' +
                ", contact_phone2='" + phone2 + '\'' +
                '}';
    }
}
