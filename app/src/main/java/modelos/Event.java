package modelos;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Event implements Parcelable {

    private int id;
    private String title;
    private String location;
    private String subtitle;
    private String description;
    private String video;
    private String localidad;
    private String cp;
    private Subsection subsection;
    private String path;
    private String image;
    private String image2;
    private String image3;
    private String dateEvent;
    private String timeEvent;
    private String dateEndEvent;
    private String timeEndEvent;
    private String updated;
    private String link;
    private String asociation;

    public Event() {
    }

    public Event(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public Subsection getSubsection() {
        return subsection;
    }

    public void setSubsection(Subsection subsection) {
        this.subsection = subsection;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDateEvent() {
        return dateEvent;
    }

    public void setDateEvent(String dateEvent) {
        this.dateEvent = dateEvent;
    }

    public String getTimeEvent() {
        return timeEvent;
    }

    public void setTimeEvent(String timeEvent) {
        this.timeEvent = timeEvent;
    }

    public String getDateEndEvent() {
        return dateEndEvent;
    }

    public void setDateEndEvent(String dateEndEvent) {
        this.dateEndEvent = dateEndEvent;
    }

    public String getTimeEndEvent() {
        return timeEndEvent;
    }

    public void setTimeEndEvent(String timeEndEvent) {
        this.timeEndEvent = timeEndEvent;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAsociation() {
        return asociation;
    }

    public void setAsociation(String asociation) {
        this.asociation = asociation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.title);
        parcel.writeString(this.location);
        parcel.writeString(this.subtitle);
        parcel.writeString(this.description);
        parcel.writeString(this.video);
        parcel.writeString(this.image);
        parcel.writeString(this.image2);
        parcel.writeString(this.image3);
//        parcel.writeString(this.getSubsection().getName());
        parcel.writeString(this.localidad);
        parcel.writeString(this.path);
        parcel.writeString(this.dateEvent);
        parcel.writeString(this.timeEvent);
        parcel.writeString(this.dateEndEvent);
        parcel.writeString(this.timeEndEvent);
        parcel.writeString(this.cp);
        parcel.writeString(this.link);
        parcel.writeString(this.asociation);
    }

    public Event(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.location = in.readString();
        this.subtitle = in.readString();
        this.description = in.readString();
        this.video = in.readString();
        this.image = in.readString();
        this.image2 = in.readString();
        this.image3 = in.readString();
        this.localidad = in.readString();
        this.path = in.readString();
        this.dateEvent = in.readString();
        this.timeEvent = in.readString();
        this.dateEndEvent = in.readString();
        this.timeEndEvent = in.readString();
        this.cp = in.readString();
        this.link = in.readString();
        this.asociation = in.readString();
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {

        @Override
        public Event createFromParcel(Parcel parcel) {
            return new Event(parcel);
        }

        @Override
        public Event[] newArray(int i) {
            return new Event[0];
        }
    };
}
