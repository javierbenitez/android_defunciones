package modelos;

import android.os.Parcel;
import android.os.Parcelable;

public class PartOfPlace implements Parcelable {

    private int id;
    private String title;
    private String description;
    private String video;
    private String audio;
    private String[] images;
    private String path;
    private String updated;
    private String link;
    private Place place;

    public PartOfPlace() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.title);
        parcel.writeString(this.description);
        parcel.writeString(this.video);
        parcel.writeString(this.path);
        parcel.writeString(this.link);
    }

    public PartOfPlace(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.description = in.readString();
        this.video = in.readString();
        this.path = in.readString();
        this.link = in.readString();
    }

    public static final Creator<PartOfPlace> CREATOR = new Creator<PartOfPlace>() {

        @Override
        public PartOfPlace createFromParcel(Parcel parcel) {
            return new PartOfPlace(parcel);
        }

        @Override
        public PartOfPlace[] newArray(int i) {
            return new PartOfPlace[0];
        }
    };
}
