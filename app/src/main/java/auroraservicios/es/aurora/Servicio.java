package auroraservicios.es.aurora;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.RingtoneManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

public class Servicio extends Service implements Devolver_Nuevos, Devolver_Nuevos_misa{
    P1 prueba;

    P1_misa prueba_misa;
    Valores valores;
    int minutos=5;

    private static final String NOTIFICATION_CHANNEL_ID_CP_1 = "my_notification_channel_CP_1";
    private static final String NOTIFICATION_CHANNEL_ID_CP_2 = "my_notification_channel_CP_2";
    private static final String NOTIFICATION_CHANNEL_ID_CP_3 = "my_notification_channel_CP_3";
    private static final String NOTIFICATION_CHANNEL_ID_CP_4 = "my_notification_channel_CP_4";
    private static final String NOTIFICATION_CHANNEL_ID_CP_5 = "my_notification_channel_CP_5";
    private static final int Notificacion_ID_CP_1= 1;
    private static final int Notificacion_ID_CP_2= 2;
    private static final int Notificacion_ID_CP_3= 3;
    private static final int Notificacion_ID_CP_4= 4;
    private static final int Notificacion_ID_CP_5= 5;
    private static final int Notificacion_ID_CP_1_misa= 10;
    private static final int Notificacion_ID_CP_2_misa= 20;
    private static final int Notificacion_ID_CP_3_misa= 30;
    private static final int Notificacion_ID_CP_4_misa= 40;

    public Servicio() {
    }

    @Override
    public void onCreate() {
        valores=new Valores(getApplicationContext());
        Log.i("Servicio_Notificacion","Servicio creado");


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

//        prueba=new P1(this,getApplicationContext());
//        prueba.execute();
        Log.i("Servicio_Notificacion","Servicio Arrancado");
//        doTimerTask();
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Log.i("Servicio_Notificacion_2","Prueba lanzada");
                prueba=new P1(Servicio.this,getApplicationContext());
                prueba.execute();

                prueba_misa=new P1_misa(Servicio.this,getApplicationContext());
                prueba_misa.execute();
            }
//        }, (1000), (60)*1000);
        }, (3*60*1000), (minutos*60)*1000);

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("Servicio_Notificacion","Servicio cerrado");
        try{
            prueba.cancel(true);
            prueba_misa.cancel(true);
        }catch (Exception e){
            Log.i("Servicio_Notificacion", "onDestroy: "+e.getMessage());
        }


    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void primer_CP(Boolean listo) {
        if (listo){
            notificacion(valores.get_Primer_cp());
        }
    }

    @Override
    public void segundo_CP(Boolean listo) {
        if (listo){
            notificacion(valores.get_Segundo_cp());
        }
    }

    @Override
    public void tercer_CP(Boolean listo) {
        if (listo){
            notificacion(valores.get_Tercer_cp());
        }
    }

    @Override
    public void cuarto_CP(Boolean listo) {
        if (listo){
            notificacion(valores.get_Guarto_cp());
        }
    }
    protected  void notificacion(String CP){

        Intent Activity_Abrir = new Intent(getApplicationContext(), Listar_Publicaciones.class);
        if (CP.equals("Global")){
            Activity_Abrir.putExtra("cp", valores.get_Primer_cp());
        }else {
            Activity_Abrir.putExtra("cp", CP);
        }
        PendingIntent contIntent =PendingIntent.getActivity(getApplicationContext(), 0, Activity_Abrir, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            Log.i("Servicio_GPS","Version Oreo");
            NotificationChannel notificationChannel;
            if (CP.equals(valores.get_Primer_cp())){
                notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_CP_1, "My Notifications_1", NotificationManager.IMPORTANCE_DEFAULT);

            }else if(CP.equals(valores.get_Segundo_cp())){
                notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_CP_2, "My Notifications_2", NotificationManager.IMPORTANCE_DEFAULT);

            }else if(CP.equals(valores.get_Tercer_cp())){
                notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_CP_3, "My Notifications_3", NotificationManager.IMPORTANCE_DEFAULT);

            }else{
                notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_CP_4, "My Notifications_4", NotificationManager.IMPORTANCE_DEFAULT);

            }

            // Configure the notification channel.
            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        Log.i("Servicio_GPS","Version Estandar");
        NotificationCompat.Builder builder;
        if (CP.equals(valores.get_Primer_cp())){
            builder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID_CP_1)
                    .setSmallIcon(R.drawable.logo)
                    .setLargeIcon((((BitmapDrawable)getResources().getDrawable(R.drawable.logo)).getBitmap()))
                    .setContentTitle("Aurora")
                    .setContentText("Nuevo fallecido en "+CP)
                    .setContentInfo("")
                    .setTicker("Alguien ha fallecido")
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setAutoCancel(true)
                    .setVibrate(new long[]{0, 1000, 500, 1000})
                    .setContentIntent(contIntent);

        }else if(CP.equals(valores.get_Segundo_cp())){
            builder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID_CP_2)
                    .setSmallIcon(R.drawable.logo)
                    .setLargeIcon((((BitmapDrawable)getResources().getDrawable(R.drawable.logo)).getBitmap()))
                    .setContentTitle("Aurora")
                    .setContentText("Nuevo fallecido en "+CP)
                    .setContentInfo("")
                    .setTicker("Alguien ha fallecido")
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setAutoCancel(true)
                    .setVibrate(new long[]{0, 1000, 500, 1000})
                    .setContentIntent(contIntent);

        }else if(CP.equals(valores.get_Tercer_cp())){
            builder= new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID_CP_3)
                    .setSmallIcon(R.drawable.logo)
                    .setLargeIcon((((BitmapDrawable)getResources().getDrawable(R.drawable.logo)).getBitmap()))
                    .setContentTitle("Aurora")
                    .setContentText("Nuevo fallecido en "+CP)
                    .setContentInfo("")
                    .setTicker("Alguien ha fallecido")
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setAutoCancel(true)
                    .setVibrate(new long[]{0, 1000, 500, 1000})
                    .setContentIntent(contIntent);

        }else{
            builder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID_CP_4)
                    .setSmallIcon(R.drawable.logo)
                    .setLargeIcon((((BitmapDrawable)getResources().getDrawable(R.drawable.logo)).getBitmap()))
                    .setContentTitle("Aurora")
                    .setContentText("Nuevo fallecido")
                    .setContentInfo("")
                    .setTicker("Alguien ha fallecido")
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setAutoCancel(true)
                    .setVibrate(new long[]{0, 1000, 500, 1000})
                    .setContentIntent(contIntent);

        }


        NotificationCompat.InboxStyle noti=new NotificationCompat.InboxStyle(builder);
        if (CP.equals(valores.get_Primer_cp())){
            noti.addLine("Nuevo fallecido en "+CP);


        }else if(CP.equals(valores.get_Segundo_cp())){
            noti.addLine("Nuevo fallecido en "+CP);


        }else if(CP.equals(valores.get_Tercer_cp())){
            noti.addLine("Nuevo fallecido en "+CP);


        }else{
            noti.addLine("Nuevo fallecido");

        }
        noti.setBigContentTitle("Aurora");
        noti.build();
        Notification notification = noti.build();

        if (CP.equals(valores.get_Primer_cp())){
            notificationManager.notify(Notificacion_ID_CP_1, notification);

        }else if(CP.equals(valores.get_Segundo_cp())){
            notificationManager.notify(Notificacion_ID_CP_2, notification);

        }else if(CP.equals(valores.get_Tercer_cp())){
            notificationManager.notify(Notificacion_ID_CP_3, notification);

        }else{
            notificationManager.notify(Notificacion_ID_CP_4, notification);
        }
    }
    protected  void notificacion_misa(String CP){

        Intent Activity_Abrir = new Intent(getApplicationContext(), Listar_Misas.class);
        if (CP.equals("Global")){
            Activity_Abrir.putExtra("cp", valores.get_Primer_cp());
        }else {
            Activity_Abrir.putExtra("cp", CP);
        }
        PendingIntent contIntent =PendingIntent.getActivity(getApplicationContext(), 0, Activity_Abrir, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            Log.i("Servicio_GPS","Version Oreo");
            NotificationChannel notificationChannel;
            if (CP.equals(valores.get_Primer_cp())){
                notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_CP_1, "My Notifications_1", NotificationManager.IMPORTANCE_DEFAULT);

            }else if(CP.equals(valores.get_Segundo_cp())){
                notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_CP_2, "My Notifications_2", NotificationManager.IMPORTANCE_DEFAULT);

            }else if(CP.equals(valores.get_Tercer_cp())){
                notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_CP_3, "My Notifications_3", NotificationManager.IMPORTANCE_DEFAULT);

            }else{
                notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_CP_4, "My Notifications_4", NotificationManager.IMPORTANCE_DEFAULT);

            }

            // Configure the notification channel.
            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        Log.i("Servicio_GPS","Version Estandar");
        NotificationCompat.Builder builder;
        if (CP.equals(valores.get_Primer_cp())){
            builder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID_CP_1)
                    .setSmallIcon(R.drawable.logo)
                    .setLargeIcon((((BitmapDrawable)getResources().getDrawable(R.drawable.logo)).getBitmap()))
                    .setContentTitle("Aurora")
                    .setContentText("Nueva misa en "+CP)
                    .setContentInfo("")
                    .setTicker("Alguien ha fallecido")
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setAutoCancel(true)
                    .setVibrate(new long[]{0, 1000, 500, 1000})
                    .setContentIntent(contIntent);

        }else if(CP.equals(valores.get_Segundo_cp())){
            builder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID_CP_2)
                    .setSmallIcon(R.drawable.logo)
                    .setLargeIcon((((BitmapDrawable)getResources().getDrawable(R.drawable.logo)).getBitmap()))
                    .setContentTitle("Aurora")
                    .setContentText("Nueva misa en "+CP)
                    .setContentInfo("")
                    .setTicker("Alguien ha fallecido")
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setAutoCancel(true)
                    .setVibrate(new long[]{0, 1000, 500, 1000})
                    .setContentIntent(contIntent);

        }else if(CP.equals(valores.get_Tercer_cp())){
            builder= new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID_CP_3)
                    .setSmallIcon(R.drawable.logo)
                    .setLargeIcon((((BitmapDrawable)getResources().getDrawable(R.drawable.logo)).getBitmap()))
                    .setContentTitle("Aurora")
                    .setContentText("Nueva misa en "+CP)
                    .setContentInfo("")
                    .setTicker("Alguien ha fallecido")
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setAutoCancel(true)
                    .setVibrate(new long[]{0, 1000, 500, 1000})
                    .setContentIntent(contIntent);

        }else{
            builder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID_CP_4)
                    .setSmallIcon(R.drawable.logo)
                    .setLargeIcon((((BitmapDrawable)getResources().getDrawable(R.drawable.logo)).getBitmap()))
                    .setContentTitle("Aurora")
                    .setContentText("Nueva misa")
                    .setContentInfo("")
                    .setTicker("Nueva misa")
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setAutoCancel(true)
                    .setVibrate(new long[]{0, 1000, 500, 1000})
                    .setContentIntent(contIntent);

        }


        NotificationCompat.InboxStyle noti=new NotificationCompat.InboxStyle(builder);
        if (CP.equals(valores.get_Primer_cp())){
            noti.addLine("Nueva misa en "+CP);


        }else if(CP.equals(valores.get_Segundo_cp())){
            noti.addLine("Nueva misa en "+CP);


        }else if(CP.equals(valores.get_Tercer_cp())){
            noti.addLine("Nueva misa en "+CP);


        }else{
            noti.addLine("Nueva misa");

        }
        noti.setBigContentTitle("Aurora");
        noti.build();
        Notification notification = noti.build();

        if (CP.equals(valores.get_Primer_cp())){
            notificationManager.notify(Notificacion_ID_CP_1_misa, notification);

        }else if(CP.equals(valores.get_Segundo_cp())){
            notificationManager.notify(Notificacion_ID_CP_2_misa, notification);

        }else if(CP.equals(valores.get_Tercer_cp())){
            notificationManager.notify(Notificacion_ID_CP_3_misa, notification);

        }else{
            notificationManager.notify(Notificacion_ID_CP_4_misa, notification);
        }
    }

    protected  void notificationEvent(String CP){

        Intent Activity_Abrir = new Intent(getApplicationContext(), ActivityCalendar.class);

        Activity_Abrir.putExtra("cp", CP);

        PendingIntent contIntent =PendingIntent.getActivity(getApplicationContext(), 0, Activity_Abrir, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            Log.i("Servicio_GPS","Version Oreo");
            NotificationChannel notificationChannel;
            if (CP.equals(valores.get_Primer_cp())){
                notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_CP_1, "My Notifications_1", NotificationManager.IMPORTANCE_DEFAULT);

            }else if(CP.equals(valores.get_Segundo_cp())){
                notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_CP_2, "My Notifications_2", NotificationManager.IMPORTANCE_DEFAULT);

            }else if(CP.equals(valores.get_Tercer_cp())){
                notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_CP_3, "My Notifications_3", NotificationManager.IMPORTANCE_DEFAULT);

            }else{
                notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_CP_4, "My Notifications_4", NotificationManager.IMPORTANCE_DEFAULT);

            }

            // Configure the notification channel.
            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        Log.i("Servicio_GPS","Version Estandar");
        NotificationCompat.Builder builder;
        if (CP.equals(valores.get_Primer_cp())){
            builder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID_CP_1)
                    .setSmallIcon(R.drawable.logo)
                    .setLargeIcon((((BitmapDrawable)getResources().getDrawable(R.drawable.logo)).getBitmap()))
                    .setContentTitle("Aurora")
                    .setContentText("Nuevo evento en "+CP)
                    .setContentInfo("")
                    .setTicker("Nuevo evento")
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setAutoCancel(true)
                    .setVibrate(new long[]{0, 1000, 500, 1000})
                    .setContentIntent(contIntent);

        }else if(CP.equals(valores.get_Segundo_cp())){
            builder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID_CP_2)
                    .setSmallIcon(R.drawable.logo)
                    .setLargeIcon((((BitmapDrawable)getResources().getDrawable(R.drawable.logo)).getBitmap()))
                    .setContentTitle("Aurora")
                    .setContentText("Nuevo evento en "+CP)
                    .setContentInfo("")
                    .setTicker("Nuevo evento")
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setAutoCancel(true)
                    .setVibrate(new long[]{0, 1000, 500, 1000})
                    .setContentIntent(contIntent);

        }else if(CP.equals(valores.get_Tercer_cp())){
            builder= new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID_CP_3)
                    .setSmallIcon(R.drawable.logo)
                    .setLargeIcon((((BitmapDrawable)getResources().getDrawable(R.drawable.logo)).getBitmap()))
                    .setContentTitle("Aurora")
                    .setContentText("Nuevo evento en "+CP)
                    .setContentInfo("")
                    .setTicker("Nuevo evento")
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setAutoCancel(true)
                    .setVibrate(new long[]{0, 1000, 500, 1000})
                    .setContentIntent(contIntent);

        }else{
            builder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID_CP_4)
                    .setSmallIcon(R.drawable.logo)
                    .setLargeIcon((((BitmapDrawable)getResources().getDrawable(R.drawable.logo)).getBitmap()))
                    .setContentTitle("Aurora")
                    .setContentText("Nuevo evento en "+CP)
                    .setContentInfo("")
                    .setTicker("Nuevo evento")
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setAutoCancel(true)
                    .setVibrate(new long[]{0, 1000, 500, 1000})
                    .setContentIntent(contIntent);

        }

        NotificationCompat.InboxStyle noti=new NotificationCompat.InboxStyle(builder);
        if (CP.equals(valores.get_Primer_cp())){
            noti.addLine("Nuevo evento en "+CP);
        }else if(CP.equals(valores.get_Segundo_cp())){
            noti.addLine("Nuevo evento en "+CP);
        }else if(CP.equals(valores.get_Tercer_cp())){
            noti.addLine("Nuevo evento en "+CP);
        }else{
            noti.addLine("Nuevo evento en "+CP);
        }
        noti.setBigContentTitle("Aurora");
        noti.build();
        Notification notification = noti.build();

        if (CP.equals(valores.get_Primer_cp())){
            notificationManager.notify(Notificacion_ID_CP_1_misa, notification);

        }else if(CP.equals(valores.get_Segundo_cp())){
            notificationManager.notify(Notificacion_ID_CP_2_misa, notification);

        }else if(CP.equals(valores.get_Tercer_cp())){
            notificationManager.notify(Notificacion_ID_CP_3_misa, notification);

        }else{
            notificationManager.notify(Notificacion_ID_CP_4_misa, notification);
        }
    }

    @Override
    public void primer_CP_misa(Boolean listo) {
        notificacion_misa(valores.get_Primer_cp());
    }

    @Override
    public void segundo_CP_misa(Boolean listo) {
        notificacion_misa(valores.get_Segundo_cp());
    }

    @Override
    public void tercer_CP_misa(Boolean listo) {
        notificacion_misa(valores.get_Tercer_cp());
    }

    @Override
    public void cuarto_CP_misa(Boolean listo) {
        notificacion_misa(valores.get_Guarto_cp());
    }

}
