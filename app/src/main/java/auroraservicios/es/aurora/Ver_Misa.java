package auroraservicios.es.aurora;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class Ver_Misa extends AppCompatActivity {

    Misa  publicacion;
    TextView E_Nombre,Nombre,E_Apellidos,Apellidos,E_Apodo,Apodo,E_F_Fallecimiento,F_Fallecimineto,E_Edad,Edad, E_Viudo, Viudo,E_Hijos,Hijos,E_Hijos_Politicos,Hijos_Politicos,
            E_Hermanos,Hermanos,E_Hermanos_Politicos,Hermanos_Politicos,E_Familiares,Familiares,E_Pareja,Pareja,E_D_Recibe,D_Recibe,E_D_Despide,D_Despide,E_Lugar_Tanatorio,Lugar_Tanatorio,
            E_Lugar_Iglesia,Lugar_Iglesia,E_Fecha_Ceremonia,Fecha_Ceremonia,E_Hora,Hora,E_Aniversario,Aniversario, E_localidadCerem2,
            localidadCerem2, E_fechaCerem2, fechaCerem2, E_horaCerem2, horaCerem2, tvDatosCerem2, E_Padres, tvPadres;;
    WebView Texto;

    TextView tvSection;

    ImageView Foto;
    Valores Valores;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver__publicacion_misas);

        Toolbar toolbar = findViewById(R.id.toolbarD);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvSection = findViewById(R.id.tvSection);
        tvSection.setText("Misa");

        Valores=new Valores(this);

        publicacion = (Misa) getIntent().getExtras().getSerializable("publicacion");

//        String url="https://auroraservicios.es/"+publicacion.getFoto();
//        Foto=findViewById(R.id.IV_Foto);
//        Picasso.get()
//                .load(url)
//                .fit()
//                .centerInside()
//                .into(Foto);

        E_Nombre=findViewById(R.id.TV_E_Nombre);
        Nombre=findViewById(R.id.TV_Nombre);

        E_Apellidos=findViewById(R.id.TV_E_Apellidos);
        Apellidos=findViewById(R.id.TV_Apellidos);

        E_Apodo=findViewById(R.id.TV_E_Apodo);
        Apodo=findViewById(R.id.TV_Apodo);

        E_F_Fallecimiento=findViewById(R.id.TV_E_F_Fallecimiento);
        F_Fallecimineto=findViewById(R.id.TV_F_Fallecimiento);

        E_Edad=findViewById(R.id.TV_E_Edad);
        Edad=findViewById(R.id.TV_Edad);

        E_Padres = findViewById(R.id.TV_E_Padres);
        tvPadres = findViewById(R.id.TV_Padres);

        E_Viudo =findViewById(R.id.TV_E_Viudo);
        Viudo =findViewById(R.id.TV_Viudo);

        E_Hijos=findViewById(R.id.TV_E_Hijos);
        Hijos=findViewById(R.id.TV_Hijos);

        E_Hijos_Politicos=findViewById(R.id.TV_E_Hijos_Politicos);
        Hijos_Politicos=findViewById(R.id.TV_Hijos_Politicos);

        E_Hijos_Politicos=findViewById(R.id.TV_E_Hijos_Politicos);
        Hijos_Politicos=findViewById(R.id.TV_Hijos_Politicos);

        E_Hermanos=findViewById(R.id.TV_E_Hermanos);
        Hermanos=findViewById(R.id.TV_Hermanos);

        E_Hermanos_Politicos=findViewById(R.id.TV_E_Hermanos_Politicos);
        Hermanos_Politicos=findViewById(R.id.TV_Hermanos_Politicos);

        E_Familiares=findViewById(R.id.TV_E_Familiares);
        Familiares=findViewById(R.id.TV_Familiares);

        E_Pareja=findViewById(R.id.TV_E_Pareja);
        Pareja=findViewById(R.id.TV_Pareja);

        E_D_Recibe=findViewById(R.id.TV_E_D_Recibe);
        D_Recibe=findViewById(R.id.TV_D_Recibe);

        E_D_Despide=findViewById(R.id.TV_E_D_Despide);
        D_Despide=findViewById(R.id.TV_D_Despide);

        E_Lugar_Tanatorio=findViewById(R.id.TV_E_Lugar);
        Lugar_Tanatorio=findViewById(R.id.TV_Lugar);

        E_Lugar_Iglesia=findViewById(R.id.TV_E_Lugar_Iglesia);
        Lugar_Iglesia=findViewById(R.id.TV_Lugar_Iglesia);

        E_Fecha_Ceremonia=findViewById(R.id.TV_E_F_Ceremonio);
        Fecha_Ceremonia=findViewById(R.id.TV_F_Ceremonia);

        E_Hora=findViewById(R.id.TV_E_Hora);
        Hora=findViewById(R.id.TV_Hora);

        E_Aniversario=findViewById(R.id.TV_E_Aniversario);
        Aniversario=findViewById(R.id.TV_Aniversario);

        tvDatosCerem2 = findViewById(R.id.tvDatosCerem2);
        E_localidadCerem2 = findViewById(R.id.TV_E_LocalidadCerem2);
        localidadCerem2 = findViewById(R.id.TV_LocalidadCerem2);
        E_fechaCerem2 = findViewById(R.id.TV_E_FechaCerem2);
        fechaCerem2 = findViewById(R.id.TV_FechaCerem2);
        E_horaCerem2 = findViewById(R.id.TV_E_HorCerem2);
        horaCerem2 = findViewById(R.id.TV_HoraCerem2);

        Log.w("Publicacion", publicacion.getPadres());
        if (publicacion.getApodo().trim().equals("")){
            E_Apodo.setVisibility(View.GONE);
            Apodo.setVisibility(View.GONE);
        }else {
            Apodo.setText(publicacion.getApodo());
        }

        if (publicacion.getPadres().trim().equals("")){
            E_Padres.setVisibility(View.GONE);
            tvPadres.setVisibility(View.GONE);
        }else {
            tvPadres.setText(publicacion.getPadres());
        }

        if (publicacion.getHijos().trim().equals("")){
            E_Hijos.setVisibility(View.GONE);
            Hijos.setVisibility(View.GONE);
        }else {
            Hijos.setText(publicacion.getHijos());
        }

        if (isNull(publicacion.getMarido())){
            E_Viudo.setVisibility(View.GONE);
            Viudo.setVisibility(View.GONE);
        }else {
            Viudo.setText(publicacion.getMarido());
        }
        if (publicacion.getHijos_politicos().trim().equals("")){
            E_Hijos_Politicos.setVisibility(View.GONE);
            Hijos_Politicos.setVisibility(View.GONE);
        }else {
            Hijos_Politicos.setText(publicacion.getHijos_politicos());
        }
        if (publicacion.getHermanos().trim().equals("")){
            E_Hermanos.setVisibility(View.GONE);
            Hermanos.setVisibility(View.GONE);
        }else {
            Hermanos.setText(publicacion.getHermanos());
        }
        if (publicacion.getHermanos_politicos().trim().equals("")){
            E_Hermanos_Politicos.setVisibility(View.GONE);
            Hermanos_Politicos.setVisibility(View.GONE);
        }else {
            Hermanos_Politicos.setText(publicacion.getHermanos_politicos());
        }
        if (publicacion.getFamiliares().trim().equals("")){
            E_Familiares.setVisibility(View.GONE);
            Familiares.setVisibility(View.GONE);
        }else {
            Familiares.setText(publicacion.getFamiliares());
        }
        if (publicacion.getPareja().trim().equals("")){
            E_Pareja.setVisibility(View.GONE);
            Pareja.setVisibility(View.GONE);
        }else {
            Pareja.setText(publicacion.getPareja());
        }

        // Inicio Ceremonia 2
        if(isNull(publicacion.getLugar2())){
            E_localidadCerem2.setVisibility(View.GONE);
            localidadCerem2.setVisibility(View.GONE);
            tvDatosCerem2.setVisibility(View.INVISIBLE);
        }else {
            localidadCerem2.setText(publicacion.getLugar2().trim().replace("\"", ""));
        }
        if (isNull(publicacion.getCeremonia2())){
            E_fechaCerem2.setVisibility(View.GONE);
            fechaCerem2.setVisibility(View.GONE);
        }else {
            fechaCerem2.setText(publicacion.getCeremonia2().trim().replace("\"", ""));
        }

        if (isNull(publicacion.getHora2())){
            E_horaCerem2.setVisibility(View.GONE);
            horaCerem2.setVisibility(View.GONE);
        }else {
            horaCerem2.setText(publicacion.getHora2().trim().replace("\"", ""));
        }
        // Fin de datos de ceremonia 2

        Nombre.setText(publicacion.getNombre());
        Apellidos.setText(publicacion.getApellidos());
        F_Fallecimineto.setText(publicacion.getFecha_fallecimiento());
        Edad.setText(publicacion.getEdad());
        Lugar_Tanatorio.setText(publicacion.getLugar());
        Fecha_Ceremonia.setText(publicacion.getCeremonia());
        Hora.setText(publicacion.getHora());
        Aniversario.setText(publicacion.getAniversario().equals("1")?"Si":"No");

        if (Valores.get_P_I(Valores.PUBLICACION)){
            new dialogo_tutorial_fotos(this ,Valores.PUBLICACION);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isNull(String value){
        if(value == null || value.equals(null) || value.equals("") || value == ""){
            return true;
        }
        return false;
    }
}
