package auroraservicios.es.aurora;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CrearMisa extends AppCompatActivity {

    private static final String URL_INSERT_MISA = "https://auroraservicios.es/api/insertar_misa.php";
    private static final String URL_PAYMENT_MISA = "https://auroraservicios.es/payment_misa.php";

    RadioGroup rgAniversario;
    RadioButton rbAniversario;
    EditText etNombre, etApellidos, etApodo, etEdad, etCp1, etCp2, etCp3, etViudo, etHijos, etHijosPoliticos,
            etHermanos, etHermanosPoliticos, etFamiliares, etPareja, etLugarMisa, etLugarMisa2;
    DatePicker dpFechaFallecimiento, dpFechaCeremonia, dpFechaCeremonia2;
    CheckBox cbPremium, cbSecondCeremony, cbValidate;
    TimePicker tpHoraCeremonia, tpHoraCeremonia2;
    TextView tvSection;
    Button bCrearMisa;

    Misa misa;

    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String nameKey = "payment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_misa);

        Log.w("Crear Misa", String.valueOf("MISA"));

        Toolbar toolbar = findViewById(R.id.toolbarCreateCeremony);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvSection = findViewById(R.id.tvSection);
        tvSection.setText("Crear Misa");

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        rgAniversario = findViewById(R.id.rgAniversario);
        etNombre = findViewById(R.id.etNombre);
        etApellidos = findViewById(R.id.etApellidos);
        etApodo = findViewById(R.id.etApodo);
        dpFechaFallecimiento = findViewById(R.id.dpFallecimiento);
        etEdad = findViewById(R.id.etEdad);
        etCp1 = findViewById(R.id.etCp1);
        etCp2 = findViewById(R.id.etCp2);
        etCp3 = findViewById(R.id.etCp3);
        cbPremium = findViewById(R.id.cbPremium);
        etViudo = findViewById(R.id.etViudo);
        etHijos = findViewById(R.id.etHijos);
        etHijosPoliticos = findViewById(R.id.etHijosPoliticos);
        etHermanos = findViewById(R.id.etHermanos);
        etHermanosPoliticos = findViewById(R.id.etHermanosPoliticos);
        etFamiliares = findViewById(R.id.etFamiliares);
        etPareja = findViewById(R.id.etPareja);
        etLugarMisa = findViewById(R.id.etLugarCerem);
        dpFechaCeremonia = findViewById(R.id.dpFechaCerem);
        tpHoraCeremonia = findViewById(R.id.tpHora);

        cbSecondCeremony = findViewById(R.id.cbSecondCeremony);
        cbValidate = findViewById(R.id.cbValidate);
        etLugarMisa2 = findViewById(R.id.etLugarCerem2);
        dpFechaCeremonia2 = findViewById(R.id.dpFechaCerem2);
        tpHoraCeremonia2 = findViewById(R.id.tpHora2);
        bCrearMisa = findViewById(R.id.bCrearMisa);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        dpFechaFallecimiento.setMaxDate(System.currentTimeMillis());
        dpFechaCeremonia.setMinDate(System.currentTimeMillis() - 1000);
        dpFechaCeremonia2.setMinDate(System.currentTimeMillis() - 1000);

        bCrearMisa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(cbValidate.isChecked()) {
                    if (!etNombre.getText().toString().trim().equals("") && !etApellidos.getText().toString().trim().equals("") &&
                            !etCp1.getText().toString().trim().equals("") && !dpFechaFallecimiento.equals("") &&
                            !etEdad.getText().toString().trim().equals("") && !dpFechaCeremonia.equals("")) {
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(nameKey, "_.loperthjnmaty*mnvsiuo?!wer");
                        editor.commit();

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(etNombre.getWindowToken(), 0);

                        // Obtener radioButton seleccionado
                        int radioButtonId = rgAniversario.getCheckedRadioButtonId();
                        View radioButton = rgAniversario.findViewById(radioButtonId);
                        int index = rgAniversario.indexOfChild(radioButton);
                        RadioButton rb = (RadioButton) rgAniversario.getChildAt(index);
                        final String textAniversario = rb.getText().toString();

                        String aniv = "0";
                        if (textAniversario.equals("Si")) {
                            aniv = "1";
                        }
                        String fF = dpFechaFallecimiento.getDayOfMonth() + "-" + (dpFechaFallecimiento.getMonth() + 1) + "-" + dpFechaFallecimiento.getYear();
                        String fC = dpFechaCeremonia.getDayOfMonth() + "-" + (dpFechaCeremonia.getMonth() + 1) + "-" + dpFechaCeremonia.getYear();
                        String hC = String.valueOf(tpHoraCeremonia.getCurrentHour() + ":" + tpHoraCeremonia.getCurrentMinute());
                        String premium = (cbPremium.isChecked() ? "1" : "0");
                        String lugar2 = "", fC2 = "", hC2 = "";
                        lugar2 = etLugarMisa2.getText().toString().trim();
                        fC2 = dpFechaCeremonia2.getDayOfMonth() + "-" + (dpFechaCeremonia2.getMonth() + 1) + "-" + dpFechaCeremonia2.getYear();
                        hC2 = String.valueOf(tpHoraCeremonia2.getCurrentHour() + ":" + tpHoraCeremonia2.getCurrentMinute());


                        if (cbSecondCeremony.isChecked()) {
                            if (etLugarMisa2.getText().toString().trim() != "" && etLugarMisa2.getText().toString().trim().length() > 1) {
                                createCeremony(aniv, fF, fC, hC, premium, lugar2, fC2, hC2);
                            } else {
//                            getDialogSecurity(aniv, fF, fC, hC, premium, lugar2, fC2, hC2);
                                Toast.makeText(CrearMisa.this, "Si quieres una segunda ceremonia, debes marcar la casilla y completar los tres campos siguientes: Lugar, fecha y hora de la ceremonia", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            if (etLugarMisa2.getText().toString().trim().length() < 1) {
                                createCeremony(aniv, fF, fC, hC, premium, lugar2, fC2, hC2);
                            } else {
//                      getDialogSecurity(aniv, fF, fC, hC, premium, lugar2, fC2, hC2);
                                Toast.makeText(CrearMisa.this, "Si quieres una segunda ceremonia, debes marcar la casilla y completar los tres campos siguientes: Lugar, fecha y hora de la ceremonia", Toast.LENGTH_LONG).show();
                            }
                        }
                    } else {
                        Toast.makeText(CrearMisa.this, "Debe rellenar los campos obligatorios", Toast.LENGTH_LONG).show();
                        return;
                    }
                }else{
                    Toast.makeText(CrearMisa.this, "Debe confirmar que los datos son verídicos y tienes el total consentimiento de los familiares para hacer esta publicación.", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void createCeremony(String aniv, String fF, String fC, String hC, String premium, String lugar2, String fC2, String hC2) {
        misa = new Misa(aniv, etNombre.getText().toString().trim(), etApellidos.getText().toString().trim(), etApodo.getText().toString().trim(),
                fF, etEdad.getText().toString().trim(), premium, etViudo.getText().toString().trim(),
                etHijos.getText().toString().trim(), etHijosPoliticos.getText().toString().trim(), etHermanos.getText().toString().trim(),
                etHermanosPoliticos.getText().toString().trim(), etFamiliares.getText().toString().trim(), etPareja.getText().toString().trim(),
                etLugarMisa.getText().toString().trim(), fC, hC, etCp1.getText().toString().trim(), etCp2.getText().toString().trim(), etCp3.getText().toString().trim(),
                lugar2, fC2, hC2);

        new WebServiceTask().execute(misa);
    }

    public AlertDialog getDialogSecurity(final String aniv, final String fF, final String fC,final String hC,
                                         final String premium, final String lugar2, final String fC2, final String hC2) {

        AlertDialog.Builder builder = new AlertDialog.Builder(CrearMisa.this);
        builder.setTitle("Error");
        builder.setMessage("Si quieres una segunda ceremonia, debes marcar la casilla y completar los tres campos siguientes: Lugar, fecha y hora de la ceremonia.");
        builder.setCancelable(false);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
//                createCeremony(aniv, fF, fC, hC, premium, lugar2, fC2, hC2);
            }
        });
        return builder.create();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    class WebServiceTask extends AsyncTask<Misa, Void, Boolean> {

        Context ctx;
        ProgressDialog pDialog;
        String resultJSON = null;

        @Override
        protected Boolean doInBackground(Misa... misas) {

            Boolean result = false;

            try {
                URL url=new URL(URL_INSERT_MISA);
                HttpURLConnection httpURLConnection=(HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                OutputStream os=httpURLConnection.getOutputStream();

                String data = URLEncoder.encode("aniversario", "UTF-8")+"="+URLEncoder.encode(misa.getAniversario(), "UTF-8")
                        +"&"+URLEncoder.encode("nombre", "UTF-8")+"="+URLEncoder.encode(misa.getNombre(), "UTF-8")+"&"+
                        URLEncoder.encode("apellidos", "UTF-8")+"="+URLEncoder.encode(misa.getApellidos(), "UTF-8")+"&"+
                        URLEncoder.encode("apodo", "UTF-8")+"="+URLEncoder.encode(misa.getApodo(), "UTF-8")+"&"+
                        URLEncoder.encode("fecha_fallecimiento", "UTF-8")+"="+URLEncoder.encode(misa.getFecha_fallecimiento(), "UTF-8")+"&"+
                        URLEncoder.encode("edad", "UTF-8")+"="+URLEncoder.encode(misa.getEdad(), "UTF-8")+"&"+
                        URLEncoder.encode("premium","UTF-8")+"="+URLEncoder.encode(misa.getPremium(), "UTF-8")+"&"+
                        URLEncoder.encode("viudo","UTF-8")+"="+URLEncoder.encode(misa.getViudo(), "UTF-8")+"&"+
                        URLEncoder.encode("hijos", "UTF-8")+"="+URLEncoder.encode(misa.getHijos(), "UTF-8")+"&"+
                        URLEncoder.encode("hijos_politicos", "UTF-8")+"="+URLEncoder.encode(misa.getHijos_politicos(), "UTF-8")+"&"+
                        URLEncoder.encode("hermanos", "UTF-8")+"="+URLEncoder.encode(misa.getHermanos(), "UTF-8")+"&"+
                        URLEncoder.encode("hermanos_politicos", "UTF-8")+"="+URLEncoder.encode(misa.getHermanos_politicos(),"UTF-8")+"&"+
                        URLEncoder.encode("familiares", "UTF-8")+"="+URLEncoder.encode(misa.getFamiliares(), "UTF-8")+"&"+
                        URLEncoder.encode("pareja","UTF-8")+"="+URLEncoder.encode(misa.getPareja(), "UTF-8")+"&"+
                        URLEncoder.encode("lugar_misa", "UTF-8")+"="+URLEncoder.encode(misa.getLugar(), "UTF-8")+"&"+
                        URLEncoder.encode("fecha_misa", "UTF-8")+"="+URLEncoder.encode(misa.getCeremonia(),"UTF-8")+"&"+
                        URLEncoder.encode("hora_misa", "UTF-8")+"="+URLEncoder.encode(misa.getHora(), "UTF-8")+"&"+
                        URLEncoder.encode("cp_1", "UTF-8")+"="+URLEncoder.encode(misa.getCp1(), "UTF-8")+"&"+
                        URLEncoder.encode("cp_2", "UTF-8")+"="+URLEncoder.encode(misa.getCp2(), "UTF-8")+"&"+
                        URLEncoder.encode("cp_3", "UTF-8")+"="+URLEncoder.encode(misa.getCp3(), "UTF-8")+"&"+
                        URLEncoder.encode("payment", "UTF-8")+"="+URLEncoder.encode("0", "UTF-8")+"&"+
                        URLEncoder.encode("lugar2", "UTF-8")+"="+URLEncoder.encode(misa.getLugar2(), "UTF-8")+"&"+
                        URLEncoder.encode("fecha_misa2", "UTF-8")+"="+URLEncoder.encode(misa.getCeremonia2(), "UTF-8")+"&"+
                        URLEncoder.encode("hora_misa2", "UTF-8")+"="+URLEncoder.encode(misa.getHora2(), "UTF-8");

                BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
                bufferedWriter.write(data);
                bufferedWriter.flush();
                int statusCode = httpURLConnection.getResponseCode();
                if (statusCode == 200) {

                    BufferedReader reader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null){
                        sb.append(line).append("\n");
                    }

                    bufferedWriter.close();
                    result = true;
                }


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(CrearMisa.this);
            pDialog.setMessage("Procesando, espere...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result){
                Toast.makeText(CrearMisa.this, "Misa creada, redirigiendo a tramitar el pago",Toast.LENGTH_LONG).show();
                Date d = new Date();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(d);
                SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
                String date = sdf.format(calendar.getTime());
                Intent intent = new Intent(CrearMisa.this, WebPayment.class);
                intent.putExtra("payment", "_.loperthjnmaty*mnvsiuo?!wer");
                intent.putExtra("name", misa.getNombre());
                intent.putExtra("lastname", misa.getApellidos());
                intent.putExtra("age", misa.getEdad());
                intent.putExtra("date", date);
                startActivity(intent);
            }else{
                Toast.makeText(CrearMisa.this, "Error, no se ha podido crear la misa",Toast.LENGTH_LONG).show();
            }

            pDialog.dismiss();
        }

    }
}
