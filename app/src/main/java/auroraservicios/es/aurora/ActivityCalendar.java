package auroraservicios.es.aurora;

import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import adapters.EventAdapter;
import modelos.Event;
import modelos.Subsection;
import sun.bob.mcalendarview.MCalendarView;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.listeners.OnMonthChangeListener;
import sun.bob.mcalendarview.vo.DateData;

public class ActivityCalendar extends AppCompatActivity implements Devolver_Listado_Publicidad, View.OnClickListener {

    private GridView gvEvents;
    private Event event;
    private String stringSubsection, stringCp;
    private int day, month, year;

    private TextView tvSubsection;
    private ImageView ivPrevious, ivNext;
    private Button tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12;

    private ArrayList<Event> itemsEvents;
    private EventAdapter eventAdapter;

    private Calendar cal;
    private MCalendarView calendarView;

    //Publicidad
    private Conseguir_Lista_Publicidad conseguir_lista_publicidad;
    private ViewPager Banner;
    private ArrayList<String> Fotos;
    private Display display;
    private int currentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        Toolbar toolbar = findViewById(R.id.toolbarDiary);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        stringSubsection = getIntent().getExtras().getString("subsection");
        stringCp = getIntent().getExtras().getString("cp");

        // Publicidad
        conseguir_lista_publicidad=new Conseguir_Lista_Publicidad(this,this, stringCp);
        conseguir_lista_publicidad.execute();
        Banner = findViewById(R.id.banner);
        Fotos = new ArrayList<>();
        display = getWindowManager().getDefaultDisplay();

        tvSubsection = findViewById(R.id.tvSubsection);
        gvEvents = findViewById(R.id.gvEvents);
        tvSubsection.setText(stringSubsection +" de "+stringCp);

        cal = Calendar.getInstance();
        year = cal.get(Calendar.YEAR); // get the current year
        month = cal.get(Calendar.MONTH); // month...
        day = cal.get(Calendar.DAY_OF_MONTH); // current day in the month


        tv1 = findViewById(R.id.tvJanuary);
        tv2 = findViewById(R.id.tvFebruary);
        tv3 = findViewById(R.id.tvMarch);
        tv4 = findViewById(R.id.tvApril);
        tv5 = findViewById(R.id.tvMay);
        tv6 = findViewById(R.id.tvJune);
        tv7 = findViewById(R.id.tvJuly);
        tv8 = findViewById(R.id.tvAugust);
        tv9 = findViewById(R.id.tvSeptember);
        tv10 = findViewById(R.id.tvOctober);
        tv11 = findViewById(R.id.tvNovember);
        tv12 = findViewById(R.id.tvDecember);

        tv1.setOnClickListener(this);
        tv2.setOnClickListener(this);
        tv3.setOnClickListener(this);
        tv4.setOnClickListener(this);
        tv5.setOnClickListener(this);
        tv6.setOnClickListener(this);
        tv7.setOnClickListener(this);
        tv8.setOnClickListener(this);
        tv9.setOnClickListener(this);
        tv10.setOnClickListener(this);
        tv11.setOnClickListener(this);
        tv12.setOnClickListener(this);

        itemsEvents = new ArrayList<Event>();

//        new WebServiceEvents().execute("Calendar");
    }

    private String getDateFormatToURL(DateData date) {
        String day = String.valueOf(date.getDay());
        String month = String.valueOf(date.getMonth());
        if(day.length() == 1){
            day = "0"+day;
        }
        if(month.length() == 1){
            month = "0"+month;
        }
        return day+month+String.valueOf(date.getYear());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menuWeb:
                openWebAurora();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openWebAurora(){
        String url = "https://servicios.auroraservicios.es";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        this.startActivity(i);
    }

    private String getNameMonth(String numberMonth){
        String stringMonth;
        switch (numberMonth) {
            case "1":  stringMonth = "Enero";
                break;
            case "2":  stringMonth  = "Febrero";
                break;
            case "3":  stringMonth = "Marzo";
                break;
            case "4":  stringMonth = "Abril";
                break;
            case "5":  stringMonth = "Mayo";
                break;
            case "6":  stringMonth = "Junio";
                break;
            case "7":  stringMonth = "Julio";
                break;
            case "8":  stringMonth = "Agosto";
                break;
            case "9":  stringMonth = "Septiembre";
                break;
            case "10": stringMonth = "Octubre";
                break;
            case "11": stringMonth = "Noviembre";
                break;
            case "12": stringMonth = "Diciembre";
                break;
            default: stringMonth = "Mes inválido";
                break;
        }
        return stringMonth;
    }

    private int getMonth(int currentMonth, int val){
        int value = currentMonth + val;
        if(val == -1 && value == 0){
            value = 12;
        }
        if(val == +1 && value == 13){
            value = 1;
        }
        return value;
    }

    private void addEventToCalendar(String date){
        int day, month, year;
        String[] dateArr = date.split("/");
        day = Integer.parseInt(dateArr[0]);
        month = Integer.parseInt(dateArr[1]);
        year = Integer.parseInt(dateArr[2]);
        calendarView.markDate(year, month, day);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onClick(View view) {
        int monthPressed = 1;
        switch (view.getId()){
            case R.id.tvJanuary:
                monthPressed = 1;
                break;
            case R.id.tvFebruary:
                monthPressed = 2;
                break;
            case R.id.tvMarch:
                monthPressed = 3;
                break;
            case R.id.tvApril:
                monthPressed = 4;
                break;
            case R.id.tvMay:
                monthPressed = 5;
                break;
            case R.id.tvJune:
                monthPressed = 6;
                break;
            case R.id.tvJuly:
                monthPressed = 7;
                break;
            case R.id.tvAugust:
                monthPressed = 8;
                break;
            case R.id.tvSeptember:
                monthPressed = 9;
                break;
            case R.id.tvOctober:
                monthPressed = 10;
                break;
            case R.id.tvNovember:
                monthPressed = 11;
                break;
            case R.id.tvDecember:
                monthPressed = 12;
                break;
        }
        Intent i;
        i = new Intent(ActivityCalendar.this, ActivityEvents.class);
        i.putExtra("subsection", stringSubsection);
        i.putExtra("cp", stringCp);
        i.putExtra("month", String.valueOf(monthPressed));
        startActivity(i);
    }

    public class WebServiceSsubsections extends AsyncTask<String, Integer, Void> {

        String urlConsulta = "http://www.servicios.auroraservicios.es/api/v1/events";
        URL connectURL;

        @Override
        protected void onPreExecute() {

            try {
                String encodedurl = urlConsulta+
                        "?cp="+URLEncoder.encode(stringCp, "utf-8")+
                        "&s="+URLEncoder.encode(stringSubsection,"UTF-8");
                connectURL = new URL(encodedurl);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... objects) {

            try {
                //            ------------------ CODIGO GROBAL----------------------------
                HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");//GET
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.connect();

                //extraemos los datos
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String resultadoConsulta = new String();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    resultadoConsulta += line;
                }

                JsonParser parser = new JsonParser();
                JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
                try {
                    itemsEvents.clear();
                } catch (Exception e) {
                }

                for (JsonElement elemento : jsonArray) {
                    JsonObject objeto = elemento.getAsJsonObject();
                    Subsection subsection = new Subsection(objeto.get("subsection").getAsString());
                    Event event = new Event(objeto.get("title").getAsString());
                    event.setId(objeto.get("id").getAsInt());
                    event.setTitle(checkValueNull(objeto, "title"));
                    event.setLocation(checkValueNull(objeto, "location"));
                    event.setDescription(checkValueNull(objeto, "description"));
                    event.setVideo(checkValueNull(objeto, "video"));
                    event.setSubsection(subsection);
                    event.setLocalidad(checkValueNull(objeto, "localidad"));
                    event.setImage(checkValueNull(objeto, "image"));
                    event.setImage2(checkValueNull(objeto, "image2"));
                    event.setImage3(checkValueNull(objeto, "image3"));
                    event.setPath(checkValueNull(objeto, "path"));
                    event.setCp(checkValueNull(objeto, "cp"));
                    event.setDateEvent(checkValueNull(objeto, "date_event"));
                    event.setTimeEvent(checkValueNull(objeto, "time_event"));
                    event.setDateEndEvent(checkValueNull(objeto, "date_end_event"));
                    event.setTimeEndEvent(checkValueNull(objeto, "time_end_event"));
                    itemsEvents.add(event);
                }
                eventAdapter = new EventAdapter(ActivityCalendar.this, itemsEvents, stringCp);
                connection.disconnect();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            for (int i = 0; i<itemsEvents.size(); i++){
                event = itemsEvents.get(i);
                addEventToCalendar(event.getDateEvent());
            }
        }
    }

    private String checkValueNull(JsonObject objeto, String value){
        if(!objeto.has(value) || objeto.get(value).isJsonNull())return "";
        if(objeto.get(value).getAsString().equals("null") || objeto.get(value).getAsString() == "null")
            return "";
        return objeto.get(value).getAsString();
    }

    @Override
    public void onTaskCompleted_publicidad(ArrayList<String> Publicidad) {
        if (Publicidad!=null){

            Fotos = Publicidad;
            if(Fotos.size() < 1) {
                Banner.setVisibility(View.GONE);
            }else{
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.height = FrameLayout.LayoutParams.MATCH_PARENT; //left, top, right, bottom
                Banner.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
                Point size = new Point();
                display.getSize(size);
            }

            Banner.setAdapter(new Adaptador_Banner(this,Fotos));

            Banner.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    super.onPageScrolled(position, positionOffset, positionOffsetPixels);

                }

                @Override
                public void onPageSelected(final int position) {
                    super.onPageSelected(position);
                    currentPage=position;
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                    super.onPageScrollStateChanged(state);
                }
            });
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == Fotos.size()) {
                        currentPage = 0;
                    }else{
                        currentPage++;
                    }
                    Banner.setCurrentItem(currentPage, true);
                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 5000, 5000);
        }
    }
}
