package auroraservicios.es.aurora;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class Base_de_datos_misa {
    public final String CP_1="publicaciones_1_misa";
    public final String CP_2="publicaciones_2_misa";
    public final String CP_3="publicaciones_3_misa";
    public final String CP_4="publicaciones_4_misa";
    SQLiteDatabase Base_Datos;
    ArrayList<Misa> Lista_Misas;
    Misa misa;
    Context context;
    String Nombre_Base_Datos="Base_Datos_Publicaciones_2_misa";
    Valores valores;
    String tabla_1="CREATE TABLE if not exists [publicaciones_1_misa] (\n" +
            "[identificador] INTEGER  ," +
            "[nombre] TEXT  ,\n" +
            "[apellidos] TEXT  ,\n" +
            "[apodo] TEXT  ,\n" +
            "[fecha_fallecimiento] TEXT  ,\n" +
            "[conyuge] TEXT  ,\n" +
            "[hijos] TEXT  ,\n" +
            "[hijos_politicos] TEXT  ,\n" +
            "[hermanos] TEXT  ,\n" +
            "[hermanos_politicos] TEXT  ," +
            "[familiares] TEXT  ," +
            "[pareja] TEXT  ," +
            "[ceremonia] TEXT  ," +
            "[lugar] TEXT  ," +
            "[hora] TEXT  ," +
            "[edad] TEXT  ," +
            "[aniversario] TEXT  ," +
            "[lugar2] TEXT ," +
            "[fecha_cerem2] TEXT ," +
            "[hora2] TEXT ," +
            "[padres] TEXT " +
            ")";
    String tabla_2="CREATE TABLE if not exists [publicaciones_2_misa] (\n" +
            "[identificador] INTEGER  ," +
            "[nombre] TEXT  ,\n" +
            "[apellidos] TEXT  ,\n" +
            "[apodo] TEXT  ,\n" +
            "[fecha_fallecimiento] TEXT  ,\n" +
            "[conyuge] TEXT  ,\n" +
            "[hijos] TEXT  ,\n" +
            "[hijos_politicos] TEXT  ,\n" +
            "[hermanos] TEXT  ,\n" +
            "[hermanos_politicos] TEXT  ," +
            "[familiares] TEXT  ," +
            "[pareja] TEXT  ," +
            "[ceremonia] TEXT  ," +
            "[lugar] TEXT  ," +
            "[hora] TEXT  ," +
            "[edad] TEXT  ," +
            "[aniversario] TEXT  ," +
            "[lugar2] TEXT ," +
            "[fecha_cerem2] TEXT ," +
            "[hora2] TEXT ," +
            "[padres] TEXT " +
            ")";
    String tabla_3="CREATE TABLE if not exists [publicaciones_3_misa] (\n" +
            "[identificador] INTEGER  ," +
            "[nombre] TEXT  ,\n" +
            "[apellidos] TEXT  ,\n" +
            "[apodo] TEXT  ,\n" +
            "[fecha_fallecimiento] TEXT  ,\n" +
            "[conyuge] TEXT  ,\n" +
            "[hijos] TEXT  ,\n" +
            "[hijos_politicos] TEXT  ,\n" +
            "[hermanos] TEXT  ,\n" +
            "[hermanos_politicos] TEXT  ," +
            "[familiares] TEXT  ," +
            "[pareja] TEXT  ," +
            "[ceremonia] TEXT  ," +
            "[lugar] TEXT  ," +
            "[hora] TEXT  ," +
            "[edad] TEXT  ," +
            "[aniversario] TEXT  ," +
            "[lugar2] TEXT ," +
            "[fecha_cerem2] TEXT ," +
            "[hora2] TEXT ," +
            "[padres] TEXT " +
            ")";
    String tabla_4="CREATE TABLE if not exists [publicaciones_4_misa] (\n" +
            "[identificador] INTEGER  ," +
            "[nombre] TEXT  ,\n" +
            "[apellidos] TEXT  ,\n" +
            "[apodo] TEXT  ,\n" +
            "[fecha_fallecimiento] TEXT  ,\n" +
            "[conyuge] TEXT  ,\n" +
            "[hijos] TEXT  ,\n" +
            "[hijos_politicos] TEXT  ,\n" +
            "[hermanos] TEXT  ,\n" +
            "[hermanos_politicos] TEXT  ," +
            "[familiares] TEXT  ," +
            "[pareja] TEXT  ," +
            "[ceremonia] TEXT  ," +
            "[lugar] TEXT  ," +
            "[hora] TEXT  ," +
            "[edad] TEXT  ," +
            "[aniversario] TEXT  ," +
            "[lugar2] TEXT ," +
            "[fecha_cerem2] TEXT ," +
            "[hora2] TEXT ," +
            "[padres] TEXT " +
            ")";

    public Base_de_datos_misa(Context context) {
        this.context = context;
        Base_Datos=context.openOrCreateDatabase(Nombre_Base_Datos,Context.MODE_PRIVATE,null);
        Base_Datos.execSQL("DROP TABLE IF EXISTS "+CP_1);
        Base_Datos.execSQL("DROP TABLE IF EXISTS "+CP_2);
        Base_Datos.execSQL("DROP TABLE IF EXISTS "+CP_3);
        Base_Datos.execSQL("DROP TABLE IF EXISTS "+CP_4);
        Base_Datos.execSQL(tabla_1);
        Base_Datos.execSQL(tabla_2);
        Base_Datos.execSQL(tabla_3);
        Base_Datos.execSQL(tabla_4);

        valores=new Valores(context);

        Lista_Misas=new ArrayList<>();
    }

    public boolean insertar(Misa misa,String CP){

        Log.i("Cosneguir_Lista_guard","Identificador - "+misa.getIdentificador());
        ContentValues valores=new ContentValues();

        valores.put("identificador",misa.getIdentificador());
        valores.put("nombre",misa.getNombre());
        valores.put("apellidos",misa.getApellidos());
        valores.put("apodo",misa.getApodo());
        valores.put("fecha_fallecimiento",misa.getFecha_fallecimiento());
        valores.put("conyuge",misa.getMarido());
        valores.put("hijos",misa.getHijos());
        valores.put("hijos_politicos",misa.getHijos_politicos());
        valores.put("hermanos",misa.getHermanos());
        valores.put("hermanos_politicos",misa.getHermanos_politicos());
        valores.put("familiares",misa.getFamiliares());
        valores.put("pareja",misa.getPareja());
        valores.put("ceremonia",misa.getCeremonia());
        valores.put("lugar",misa.getLugar());
        valores.put("hora",misa.getHora());
        valores.put("edad",misa.getEdad());
        valores.put("aniversario",misa.getAniversario());
        valores.put("lugar2",misa.getLugar2());
        valores.put("fecha_cerem2",misa.getCeremonia2());
        valores.put("hora2",misa.getHora2());
        valores.put("padres",misa.getPadres());

        Base_Datos.insert(CP,null,valores);
        Log.i("Cosneguir_Lista_guard","Publicacion - "+misa.toString()
        );



        return true;
    }
    public boolean eliminar(String CP){
        Base_Datos.delete(CP,null,null);
        return true;
    }
    public ArrayList<Misa> getLista_Misas(){

        return Lista_Misas;
    }
    public Misa getMisa(String DNI){

        return misa;
    }
    public ArrayList<Misa> getMisas(String CP){
        Lista_Misas.clear();
        Cursor cursor=Base_Datos.rawQuery("select * from publicaciones_4_misa",null);
        Log.i("Cosneguir_Lista_3","entra - "+cursor.getCount());

        if (cursor!=null && cursor.getCount()>0){
            cursor.moveToFirst();
            do {
                setDataMisa(cursor);

            }while (cursor.moveToNext());
        }
        cursor.close();
        Cursor segundo_cursor;
        if (CP.equals(valores.get_Primer_cp())){
            segundo_cursor=Base_Datos.rawQuery("select * from publicaciones_1_misa",null);
            Log.i("Cosneguir_Lista_carga_b","Cursor tamaño - "+segundo_cursor.getCount());
            if (segundo_cursor!=null && segundo_cursor.getCount()>0){
                segundo_cursor.moveToFirst();
                Log.i("Cosneguir_Lista_carga_b","Tabla - publicaciones_1");
                do {
                   setDataMisa(segundo_cursor);
                }while (segundo_cursor.moveToNext());
            }
        }else if (CP.equals(valores.get_Segundo_cp())){
            segundo_cursor=Base_Datos.rawQuery("select * from "+CP_2,null);
            if (segundo_cursor!=null && segundo_cursor.getCount()>0){
                segundo_cursor.moveToFirst();
                do {
                    setDataMisa(segundo_cursor);

                }while (segundo_cursor.moveToNext());
            }
        }else if (CP.equals(valores.get_Tercer_cp())){
            segundo_cursor=Base_Datos.rawQuery("select * from "+CP_3,null);
            if (segundo_cursor!=null && segundo_cursor.getCount()>0){
                segundo_cursor.moveToFirst();
                do {
                    setDataMisa(segundo_cursor);

                }while (segundo_cursor.moveToNext());
            }
        }
        return Lista_Misas;
    }

    private void setDataMisa(Cursor cursor) {
        Misa misa = new Misa();
        misa.setIdentificador(cursor.getString(0));
        Log.i("Cosneguir_Lista_carga_b","Identificador - "+misa.getIdentificador());
        misa.setNombre(cursor.getString(1));
        misa.setApellidos(cursor.getString(2));
        misa.setApodo(cursor.getString(3));
        misa.setFecha_fallecimiento(cursor.getString(4));
        misa.setMarido(cursor.getString(5));
        misa.setHijos(cursor.getString(6));
        misa.setHijos_politicos(cursor.getString(7));
        misa.setHermanos(cursor.getString(8));
        misa.setHermanos_politicos(cursor.getString(9));
        misa.setFamiliares(cursor.getString(10));
        misa.setPareja(cursor.getString(11));
        misa.setCeremonia(cursor.getString(12));
        misa.setLugar(cursor.getString(13));
        misa.setHora(cursor.getString(14));
        misa.setEdad(cursor.getString(15));
        misa.setAniversario(cursor.getString(16));
        misa.setLugar2(cursor.getString(17));
        misa.setCeremonia2(cursor.getString(18));
        misa.setHora2(cursor.getString(19));
        misa.setPadres(cursor.getString(20));
        Lista_Misas.add(misa);
    }

    public int Mas_Grande(String CP){
        Cursor cursor;
        if (CP.equals(valores.get_Primer_cp())){
            cursor=Base_Datos.rawQuery("select MAX(identificador) from "+CP_1,null);
        }else if(CP.equals(valores.get_Segundo_cp())){
            cursor=Base_Datos.rawQuery("select MAX(identificador) from "+CP_2,null);
        }else if(CP.equals(valores.get_Tercer_cp())){
            cursor=Base_Datos.rawQuery("select MAX(identificador) from "+CP_3,null);
        }else{
            cursor=Base_Datos.rawQuery("select MAX(identificador) from "+CP_4,null);
        }
        if (cursor!=null&&cursor.getCount()>0){
            cursor.moveToFirst();
            try {
                return Integer.parseInt(cursor.getString(0));
            }catch (Exception e){
                return 0;
            }
        }else{
            return 0;
        }
    }
}
