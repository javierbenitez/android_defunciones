package auroraservicios.es.aurora;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import adapters.AsociationAdapter;
import adapters.EventAdapter;
import adapters.VideoAsociationAdapter;
import modelos.Asociation;
import modelos.Event;
import modelos.Section;
import modelos.Subsection;
import modelos.VideoAsociation;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class ActivityAsociation extends AppCompatActivity implements View.OnClickListener {

    private static final String URL_ASOCIATION = "http://www.servicios.auroraservicios.es/uploads/images/asociaciones/";
    private static final float APPBAR_ELEVATION = 14f;

    private Asociation asociation;
    private VideoAsociationAdapter videoAsociationAdapter;

    private Toolbar toolbar;
    private TextView tvLocality, tvTitle, tvDescription, tvLocation;
    private VideoView videoView;
    private GridView gvVideosAsociation;
    private LinearLayout llVideoAsociation, llNoVideos;

    private ArrayList itemsVideos;
    private boolean b = false;
    private String stringCp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asociation);

        int orientation = getResources().getConfiguration().orientation;

        toolbar = findViewById(R.id.toolbarAsociation);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        asociation = getIntent().getExtras().getParcelable("asociation");
        stringCp = getIntent().getStringExtra("cp");

        gvVideosAsociation = findViewById(R.id.gvVideosAsociation);
        tvTitle = findViewById(R.id.tvTitle);
        tvLocality = findViewById(R.id.tvLocality);
        tvLocation = findViewById(R.id.tvLocation);
        tvDescription = findViewById(R.id.tvDescription);
        llVideoAsociation = findViewById(R.id.llVideoAsociation);
        llNoVideos = findViewById(R.id.llNoVideos);


        tvTitle.setText(asociation.getTitle());
        tvDescription.setText(asociation.getDescription());
        tvLocation.setText(asociation.getLocation());
//        tvLocality.setText(asociation.getLocalidad());

        itemsVideos = new ArrayList();
        tvTitle.setText(asociation.getTitle());
//        tvLocality.setText(asociation.getLocalidad());
        tvLocation.setText(asociation.getLocation());
        tvDescription.setText(asociation.getDescription());

        new WebServiceCompany().execute("info_asociation");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menuWeb:
                openWebAurora();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openWebAurora(){
        String url = "https://servicios.auroraservicios.es";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        this.startActivity(i);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putInt("age", 24);
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        int age = savedInstanceState.getInt("age");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        final int rotation = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (rotation) {
            case Surface.ROTATION_0:
            case Surface.ROTATION_180:
                originalSize();
                break;
            case Surface.ROTATION_90:
            case Surface.ROTATION_270:
            default:
                fullScreen();
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.bPlay:
                videoView.start();
                break;
            case R.id.bStop:
                videoView.stopPlayback();
                videoView.resume();
                break;
            case R.id.bPause:
                videoView.pause();
                break;
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final int rotation = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (rotation) {
            case Surface.ROTATION_0:
            case Surface.ROTATION_180:
                toolbar.setVisibility(View.VISIBLE);
//                originalSize();
                break;
            case Surface.ROTATION_90:
            default:
                View decorView = getWindow().getDecorView();
                int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN;
                decorView.setSystemUiVisibility(uiOptions);
                break;
        }

        return super.onTouchEvent(event);
    }

    public class WebServiceCompany extends AsyncTask<String, Integer, Void> {

        String urlConsulta = "http://www.servicios.auroraservicios.es/api/v1/asociations/asociation/info" +
                "?asoc="+ asociation.getId();
        URL connectURL;

        @Override
        protected void onPreExecute() {
            try {
                connectURL = new URL(urlConsulta);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... objects) {

            try {
                //            ------------------ CODIGO GROBAL----------------------------
                HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");//GET
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.connect();

                //extraemos los datos
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String resultadoConsulta = new String();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    resultadoConsulta += line;
                }

                JsonParser parser = new JsonParser();
                JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
                try {
                    itemsVideos.clear();
                } catch (Exception e) {
                }

                for (JsonElement elemento : jsonArray) {
                    JsonObject objeto = elemento.getAsJsonObject();
                    Section section = new Section(objeto.get("section").getAsString());
                    VideoAsociation videoAsociation = new VideoAsociation();
                    videoAsociation.setAsociation(asociation);
                    videoAsociation.setTitle(checkValueNull(objeto, "title_va"));
                    videoAsociation.setDescription(checkValueNull(objeto, "description_va"));
                    videoAsociation.setPath(checkValueNull(objeto, "path_va"));
                    videoAsociation.setVideo(checkValueNull(objeto, "video_src"));
                    videoAsociation.setUpdated(checkValueNull(objeto, "updated_va"));
                    itemsVideos.add(videoAsociation);
                }
                videoAsociationAdapter = new VideoAsociationAdapter(ActivityAsociation.this,
                        itemsVideos, stringCp);
                connection.disconnect();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            VideoAsociation vAsociation = (VideoAsociation) itemsVideos.get(0);
            if(vAsociation.getPath() != "") {
                llVideoAsociation.setVisibility(View.VISIBLE);
                llNoVideos.setVisibility(View.GONE);
                gvVideosAsociation.setAdapter(videoAsociationAdapter);
            }else{
                llVideoAsociation.setVisibility(View.GONE);
                llNoVideos.setVisibility(View.VISIBLE);
            }
        }
    }

    private void fullScreen(){
        if(asociation.getVideo() != "" && asociation.getVideo() != null) {
            toolbar.setVisibility(View.GONE);
            getSupportActionBar().hide();
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            ConstraintLayout.LayoutParams params2 = (ConstraintLayout.LayoutParams) videoView.getLayoutParams();
            params2.width = MATCH_PARENT;
            params2.height = MATCH_PARENT;
            params2.leftMargin = 0;
            params2.rightMargin = 0;
            params2.topMargin = 0;
            MediaController mediaController = new MediaController(this);
            videoView.setMediaController(mediaController);
            videoView.setLayoutParams(params2);
        }
    }

    private void originalSize(){
        if(asociation.getVideo() != "" && asociation.getVideo() != null) {
            toolbar.setVisibility(View.VISIBLE);
            getSupportActionBar().show();
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            ConstraintLayout.LayoutParams params2 = (ConstraintLayout.LayoutParams) videoView.getLayoutParams();
            params2.width = MATCH_PARENT;
            params2.height = (int) (200 * metrics.density);
            params2.topMargin = 200;
            videoView.setLayoutParams(params2);
        }
    }

    private String checkValueNull(JsonObject objeto, String value){
        if(objeto.get(value).isJsonNull())return "";
        if(objeto.get(value).getAsString().equals("null") || objeto.get(value).getAsString() == "null")
            return "";
        return objeto.get(value).getAsString();
    }

}
