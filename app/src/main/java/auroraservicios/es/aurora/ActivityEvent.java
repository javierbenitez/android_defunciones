package auroraservicios.es.aurora;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import adapters.EventAdapter;
import modelos.Event;
import modelos.Subsection;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class ActivityEvent extends AppCompatActivity implements View.OnClickListener {

    private static final String URL_MAIN = "http://www.servicios.auroraservicios.es/uploads/images/";
    private static final String URL_EVENT = URL_MAIN+"eventos/";
    private static final float APPBAR_ELEVATION = 14f;

    private String stringCp, pathVideo, subsection;
    private boolean b = false;
    private Event event;

    private Toolbar toolbar;
    private TextView tvAsociation, tvLocality, tvTitle, tvSubtitle, tvDescription, tvPlace, tvDateEvent, tvLink;
    private LinearLayout llButtons, llImg, llToolbar;
    private VideoView videoView;
    private ImageButton ibPlay, ibPause, ibStop;
    private Button bData;
    private ScrollView scView;
    private ImageView ivImg1, ivImg2, ivImg3;

    private ArrayList itemsEvent;
    private EventAdapter eventAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_event);

        int orientation = getResources().getConfiguration().orientation;
        int totalImgs = 0;

        toolbar = findViewById(R.id.toolbarEv);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        event = getIntent().getExtras().getParcelable("event");
        subsection = getIntent().getStringExtra("subsection");
        stringCp = getIntent().getStringExtra("cp");

        scView = findViewById(R.id.scView);
        bData = findViewById(R.id.bData);
        ibPlay = findViewById(R.id.bPlay);
        ibPause = findViewById(R.id.bPause);
        ibStop = findViewById(R.id.bStop);
        ivImg1 = findViewById(R.id.ivImg1);
        ivImg2 = findViewById(R.id.ivImg2);
        ivImg3 = findViewById(R.id.ivImg3);
        tvAsociation = findViewById(R.id.tvAsociation);
        tvTitle = findViewById(R.id.tvTitle);
        tvSubtitle = findViewById(R.id.tvSubtitle);
        tvLocality = findViewById(R.id.tvLocality);
        tvPlace = findViewById(R.id.tvPlace);
        tvDescription = findViewById(R.id.tvDescription);
        tvDateEvent = findViewById(R.id.tvDateEvent);
        videoView = findViewById(R.id.vvEvent);
        llButtons = findViewById(R.id.llButtons);
        llToolbar = findViewById(R.id.llToolbar);
        llImg = findViewById(R.id.llImg);
        tvLink = findViewById(R.id.tvLink);

        bData.setOnClickListener(this);
        ibStop.setOnClickListener(this);
        ibPause.setOnClickListener(this);
        ibPlay.setOnClickListener(this);

        tvTitle.setText(event.getTitle());
        tvSubtitle.setText(event.getSubtitle());
        tvDescription.setText(event.getDescription());
        tvPlace.setText(event.getLocation());
        tvLocality.setText("En " + event.getLocalidad());

        if(subsection.equals("Asociaciones")){
            tvAsociation.setVisibility(View.VISIBLE);
            tvAsociation.setText("\""+event.getAsociation()+"\"");
        }else{
            tvAsociation.setVisibility(View.GONE);
        }

        if(event.getDateEndEvent() == "" || event.getDateEndEvent() == null || event.getDateEndEvent().indexOf("/") < 0){
            tvDateEvent.setText("El "+event.getDateEvent()+", todo el día");
        }else{
            String dateEvent = "Desde las "+event.getTimeEvent()+" del "+ event.getDateEvent()+
                    " hasta las "+event.getTimeEndEvent()+" del " +event.getDateEndEvent();
            tvDateEvent.setText(dateEvent);
        }

        if(event.getVideo() != "" && event.getVideo() != null) {
            pathVideo = URL_EVENT+event.getPath()+event.getVideo();
            playVideoView(pathVideo);
        }else{
            bData.setVisibility(View.GONE);
            scView.setVisibility(View.VISIBLE);
            videoView.setVisibility(View.GONE);
            llButtons.setVisibility(View.GONE);
            llImg.setVisibility(View.GONE);
        }

        if(!event.getImage().equals("")){
            totalImgs++;
        }

        if(!event.getImage2().equals("")){
            totalImgs++;
        }

        if(!event.getImage3().equals("")){
            totalImgs++;
        }

        if(event.getImage() == null || event.getImage() == "" || event.getImage().equals(null) || event.getImage().equals("")){
            ivImg1.setVisibility(View.GONE);
        }else{
            String urlPhoto1;
            if(subsection.equals("Asociaciones")){
                urlPhoto1 = URL_MAIN+event.getPath()+event.getImage();
            }else{
                urlPhoto1 = URL_EVENT+event.getPath()+event.getImage();
            }
            urlPhoto1 = urlPhoto1.replaceAll(" ", "%20");
            if(totalImgs == 1){

                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                int width = metrics.widthPixels; // ancho absoluto en pixels
                int height = metrics.heightPixels;
                int mitad = (int) Math.round(((width-580)/(2)));
                LinearLayout.LayoutParams params01 = (LinearLayout.LayoutParams)ivImg1.getLayoutParams();
                params01.width = 500;
                params01.height = 700;
                params01.setMarginStart(mitad);
                ivImg1.setLayoutParams(params01);
            }else{
                ViewGroup.LayoutParams paramsX = (ViewGroup.LayoutParams) ivImg1.getLayoutParams();
                paramsX.width = 500;
                paramsX.height = 700;
                ivImg1.setLayoutParams(paramsX);
            }

            ivImg1.setVisibility(View.VISIBLE);
            Picasso.get()
                    .load(urlPhoto1)
                    .fit()
                    .into(ivImg1);
        }

        if(event.getImage2() == null || event.getImage2() == "" || event.getImage2().equals(null) || event.getImage2().equals("")){
            ivImg2.setVisibility(View.GONE);
        }else{
            String urlPhoto2;
            if(subsection.equals("Asociaciones")){
                urlPhoto2 = URL_MAIN+event.getPath()+event.getImage2();
            }else{
                urlPhoto2 = URL_EVENT+event.getPath()+event.getImage2();
            }
            urlPhoto2 = urlPhoto2.replaceAll(" ", "%20");
            ViewGroup.LayoutParams params2 = (ViewGroup.LayoutParams) ivImg2.getLayoutParams();
            params2.width = 500;
            params2.height = 700;
            ivImg2.setLayoutParams(params2);
            ivImg2.setVisibility(View.VISIBLE);
            Picasso.get()
                    .load(urlPhoto2)
                    .fit()
                    .into(ivImg2);
        }

        if(event.getImage3() == null || event.getImage3() == "" || event.getImage3().equals(null) || event.getImage3().equals("")){
            ivImg3.setVisibility(View.GONE);
        }else{
            String urlPhoto3;
            if(subsection.equals("Asociaciones")){
                urlPhoto3 = URL_MAIN+event.getPath()+event.getImage3();
            }else{
                urlPhoto3 = URL_EVENT+event.getPath()+event.getImage3();
            }
            urlPhoto3 = urlPhoto3.replaceAll(" ", "%20");
            ViewGroup.LayoutParams params3 = (ViewGroup.LayoutParams) ivImg3.getLayoutParams();
            params3.width = 500;
            params3.height = 700;
            ivImg3.setLayoutParams(params3);
            ivImg3.setVisibility(View.VISIBLE);
            Picasso.get()
                    .load(urlPhoto3)
                    .fit()
                    .into(ivImg3);
        }

        ivImg1.setOnClickListener(this);
        ivImg2.setOnClickListener(this);
        ivImg3.setOnClickListener(this);

        if(event.getLink() != "" && event.getLink() != null) {
            tvLink.setText(event.getLink());
        }else{
            tvLink.setVisibility(View.GONE);
        }

        itemsEvent = new ArrayList();

        new WebServiceCompany().execute("info_event");
    }

    private void playVideoView(String pathVideo) {
        videoView = findViewById(R.id.vvEvent);
        try {
            videoView.setVideoPath(pathVideo);
            videoView.start();
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }


        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                return false;
            }
        });
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                //do something when video is ready to play
                mediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                    @Override
                    public void onSeekComplete(MediaPlayer mp) {
                        //TODO: Your code here
                        videoView.start();
                    }
                });
            }
        });
        videoView.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menuWeb:
                openWebAurora();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openWebAurora(){
        String url = "https://servicios.auroraservicios.es";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        this.startActivity(i);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putInt("age", 24);
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        int age = savedInstanceState.getInt("age");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        final int rotation = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (rotation) {
            case Surface.ROTATION_0:
            case Surface.ROTATION_180:
                originalSize();
                bData.setVisibility(View.VISIBLE);
                break;
            case Surface.ROTATION_90:
            case Surface.ROTATION_270:
            default:
                fullScreen();
                bData.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        Intent i;
        switch(view.getId()){
            case R.id.bData:
                if(!b){
                    b = true;
                    bData.setText("Cerrar datos");
                    scView.setVisibility(View.VISIBLE);
                    videoView.stopPlayback();
                    videoView.setVisibility(View.GONE);
                    llButtons.setVisibility(View.GONE);
                    llImg.setVisibility(View.GONE);
                }else{
                    b = false;
                    bData.setText("Ver datos");
                    scView.setVisibility(View.GONE);
                    videoView.setVisibility(View.VISIBLE);
                    llButtons.setVisibility(View.VISIBLE);
                    llImg.setVisibility(View.VISIBLE);
                    videoView.start();
                }
                break;
            case R.id.bPlay:
                videoView.start();
                break;
            case R.id.bStop:
                videoView.stopPlayback();
                videoView.resume();
                break;
            case R.id.bPause:
                videoView.pause();
                break;
            case R.id.ivImg1:
                i = new Intent(ActivityEvent.this, ActivityImageEvent.class);
                i.putExtra("item", (Parcelable) event);
                i.putExtra("subsection", subsection);
                i.putExtra("n", 1);
                startActivity(i);
                break;
            case R.id.ivImg2:
                i = new Intent(ActivityEvent.this, ActivityImageEvent.class);
                i.putExtra("item", (Parcelable) event);
                i.putExtra("subsection", subsection);
                i.putExtra("n", 2);
                startActivity(i);
                break;
            case R.id.ivImg3:
                i = new Intent(ActivityEvent.this, ActivityImageEvent.class);
                i.putExtra("item", (Parcelable) event);
                i.putExtra("subsection", subsection);
                i.putExtra("n", 3);
                startActivity(i);
                break;
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final int rotation = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (rotation) {
            case Surface.ROTATION_0:
            case Surface.ROTATION_180:
                toolbar.setVisibility(View.VISIBLE);
//                originalSize();
                break;
            case Surface.ROTATION_90:
            default:
                View decorView = getWindow().getDecorView();
                int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN;
                decorView.setSystemUiVisibility(uiOptions);
                break;
        }

        return super.onTouchEvent(event);
    }

    public class WebServiceCompany extends AsyncTask<String, Integer, Void> {

        String urlConsulta = "http://www.servicios.auroraservicios.es/api/v1/events/event/info" +
                "?event="+ event.getId();
        URL connectURL;

        @Override
        protected void onPreExecute() {
            try {
                connectURL = new URL(urlConsulta);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... objects) {

            try {
                //            ------------------ CODIGO GROBAL----------------------------
                HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");//GET
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.connect();

                //extraemos los datos
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String resultadoConsulta = new String();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    resultadoConsulta += line;
                }

                JsonParser parser = new JsonParser();
                JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
                try {
                    itemsEvent.clear();
                } catch (Exception e) {
                }

                for (JsonElement elemento : jsonArray) {
                    JsonObject objeto = elemento.getAsJsonObject();
                    Subsection subsection = new Subsection(objeto.get("subsection").getAsString());
                    Event event = new Event(objeto.get("title").getAsString());
//                    event.setSubsection(subsection);
//                    event.setDescription(checkValueNull(objeto, "subtitle"));
//                    event.setDescription(checkValueNull(objeto, "description"));
//                    event.setVideo(checkValueNull(objeto,"path"));
//                    event.setVideo(checkValueNull(objeto,"video"));
//                    event.setVideo(checkValueNull(objeto,"url_video"));
//                    event.setLocation(objeto.get("location").getAsString());
//                    event.setLocalidad(objeto.get("localidad").getAsString());
//                    event.setImage(checkValueNull(objeto, "image1"));
//                    event.setImage2(checkValueNull(objeto, "image2"));
//                    event.setImage3(checkValueNull(objeto, "image3"));
//                    event.setImage3(checkValueNull(objeto, "date_event"));
//                    event.setImage3(checkValueNull(objeto, "date_end_event"));
//                    event.setImage3(checkValueNull(objeto, "time_event"));
//                    event.setImage3(checkValueNull(objeto, "time_end_event"));
                    itemsEvent.add(event);
                }
                eventAdapter = new EventAdapter(ActivityEvent.this, itemsEvent, stringCp);
                connection.disconnect();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

        }
    }

    private void fullScreen(){
        if(event.getVideo() != "" && event.getVideo() != null) {
            toolbar.setVisibility(View.GONE);
            getSupportActionBar().hide();
            llToolbar.setVisibility(View.GONE);
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            ConstraintLayout.LayoutParams params2 = (ConstraintLayout.LayoutParams) videoView.getLayoutParams();
            params2.width = MATCH_PARENT;
            params2.height = MATCH_PARENT;
            params2.leftMargin = 0;
            params2.rightMargin = 0;
            params2.topMargin = 0;
            MediaController mediaController = new MediaController(this);
            videoView.setMediaController(mediaController);
            videoView.setLayoutParams(params2);
        }
    }

    private void originalSize(){
        if(event.getVideo() != "" && event.getVideo() != null) {
            toolbar.setVisibility(View.VISIBLE);
            getSupportActionBar().show();
            llToolbar.setVisibility(View.VISIBLE);
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            ConstraintLayout.LayoutParams params2 = (ConstraintLayout.LayoutParams) videoView.getLayoutParams();
            params2.width = MATCH_PARENT;
            params2.height = (int) (200 * metrics.density);
            params2.topMargin = 200;
            videoView.setLayoutParams(params2);
        }
    }


}
