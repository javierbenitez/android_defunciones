package auroraservicios.es.aurora;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class Insertar_Cp_existente extends AsyncTask<Object, Integer, Void> {

    private Devolver_Esta_CP respuesta;

    String urlConsulta = "https://auroraservicios.es/librerias/Insertar_cp_existente.php?";
    String charset = "UTF-8";
    URL connectURL;
    String id;
    Context yo;
    Valores valores;


    ArrayList<String> Lista_CP;


    public Insertar_Cp_existente(Devolver_Esta_CP respuesta, Context yo, ArrayList<String> Lista_CP) {
        this.respuesta = respuesta;
        this.yo=yo;
        valores=new Valores(this.yo);
        this.Lista_CP=Lista_CP;
        Log.i("Insertar_cp","Consutructor terminado");
    }

    @Override
    protected void onPreExecute() {
//        try {
//            connectURL = new URL(urlConsulta+"cp="+stringSubsection);
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    protected Void doInBackground(Object... objects) {

        try {
            Log.i("Insertar_cp","Tamaño de la lista - "+Lista_CP.size());

            for (int i=0;i<Lista_CP.size();i++){
                connectURL = new URL(urlConsulta+"cp="+Lista_CP.get(i));
                Log.i("Insertar_cp","URL conectada - "+connectURL.toString());

                HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");//GET
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.connect();
                //extraemos los datos
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String resultadoConsulta = new String();
                connection.disconnect();
                Log.i("Insertar_cp","Carga terminada");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
//        respuesta.inserccion_Completa(true);
//        Toast.makeText(context, "Descargado", Toast.LENGTH_SHORT).show();
        Log.i("Comprobacion_cp","Mensaje de respuesta enviado");
        Log.i("Elejir_CP","Termina insertar");

    }



}
