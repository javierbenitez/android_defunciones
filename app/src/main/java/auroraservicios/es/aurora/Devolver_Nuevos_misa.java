package auroraservicios.es.aurora;

public interface Devolver_Nuevos_misa {
    void primer_CP_misa(Boolean listo);
    void segundo_CP_misa(Boolean listo);
    void tercer_CP_misa(Boolean listo);
    void cuarto_CP_misa(Boolean listo);
}
