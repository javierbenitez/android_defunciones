package auroraservicios.es.aurora;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import modelos.Publicacion;

public class Adaptador extends BaseAdapter {
    ArrayList<Publicacion> Lista_Publicaciones;
    Publicacion publicacion;
    Activity activity;

    public Adaptador(Activity activity,ArrayList<Publicacion> lista_Publicaciones) {
        Lista_Publicaciones = lista_Publicaciones;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return Lista_Publicaciones.size();
    }

    @Override
    public Publicacion getItem(int position) {
        publicacion=Lista_Publicaciones.get(position);
        return publicacion;
    }

    @Override
    public long getItemId(int position) {
        publicacion=Lista_Publicaciones.get(position);
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view=convertView;
        if (view==null){
            LayoutInflater layoutInflater=(LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=layoutInflater.inflate(R.layout.publicacion_lista,null);
        }
        publicacion = Lista_Publicaciones.get(position);

        TextView Nombre,Apellidos,E_Apodo,Apodo,Fecha_fallecido,Fecha_entierro,Lugar_Entierro,
                Hora_Entierro,Edad, E_Direccion,Direccion;

//        ImageView Foto;

//        Foto=view.findViewById(R.id.IV_Foto);

        Nombre=view.findViewById(R.id.TV_Nombre);
        Apellidos=view.findViewById(R.id.TV_Apellidos);
        Apodo=view.findViewById(R.id.TV_Apodo);
        E_Apodo=view.findViewById(R.id.TV_E_Apodo);
        Fecha_fallecido=view.findViewById(R.id.TV_Fecha_fallecido);
        Fecha_entierro=view.findViewById(R.id.TV_Fecha_entierro);
        Lugar_Entierro=view.findViewById(R.id.TV_Lugar_Entierro);
        Hora_Entierro=view.findViewById(R.id.TV_Hora_Entierro);
        Edad=view.findViewById(R.id.TV_Edad);
        E_Direccion = view.findViewById(R.id.TV_E_Direccion);
        Direccion = view.findViewById(R.id.TV_Direccion);

//        String url="https://auroraservicios.es/"+publicacion.getFoto();
//        Log.i("Cosneguir_Lista_url","url - "+url);
//        Picasso.get()
//                .load(url)
//                .fit()
//                .centerInside()
//                .into(Foto);


        Nombre.setText(publicacion.getNombre());
        Apellidos.setText(publicacion.getApellidos());
        if (publicacion.getApodo().trim().equals("")){
            E_Apodo.setVisibility(View.GONE);
            Apodo.setVisibility(View.GONE);
        }else {
            Apodo.setText(publicacion.getApodo());
        }

        if (publicacion.getDireccion().trim().equals("")){
            E_Direccion.setVisibility(View.GONE);
            Direccion.setVisibility(View.GONE);
        }else {
            Direccion.setText(publicacion.getDireccion());
        }

        Fecha_fallecido.setText(publicacion.getFecha_fallecimiento());
        Fecha_entierro.setText(publicacion.getCeremonia());
        Lugar_Entierro.setText(publicacion.getLugar());
        Hora_Entierro.setText(publicacion.getHora());
        Edad.setText(publicacion.getEdad());
        Direccion.setText(publicacion.getDireccion());

        return view;
    }
}
