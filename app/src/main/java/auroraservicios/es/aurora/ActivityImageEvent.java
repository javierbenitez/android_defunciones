package auroraservicios.es.aurora;

import android.content.Context;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.VideoView;

import com.squareup.picasso.Picasso;

import java.net.URL;

import modelos.Event;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class ActivityImageEvent extends AppCompatActivity {


    private static final String URL_MAIN = "http://servicios.auroraservicios.es/uploads/images/";
    private static final String URL_EVENT = URL_MAIN+"eventos/";
    private ImageButton imageButton;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_image);

        toolbar = findViewById(R.id.toolbarEv);
        imageButton = findViewById(R.id.imageButton);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        Event event = getIntent().getExtras().getParcelable("item");
        String subsection = getIntent().getStringExtra("subsection");
        String image = event.getImage();
        int i = getIntent().getExtras().getInt("n");
        if(i == 2) {
            image = event.getImage2();
        }else if(i == 3) {
            image = event.getImage3();
        }

        String pathComplete;
        if(subsection.equals("Asociaciones")){
            pathComplete = URL_MAIN+event.getPath()+image;
        }else{
            pathComplete =URL_EVENT+ event.getPath() + image;
        }

        Picasso.get()
                .load(pathComplete)
                .fit()
                .into(imageButton);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
