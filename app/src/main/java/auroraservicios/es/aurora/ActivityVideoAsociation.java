package auroraservicios.es.aurora;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import adapters.EventAdapter;
import modelos.Event;
import modelos.Subsection;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class ActivityVideoAsociation extends AppCompatActivity implements View.OnClickListener {


    private static final String URL_CONSULTA = "http://www.servicios.auroraservicios.es/uploads/images/asociaciones/";
    private VideoView videoView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_video_asociation);

        String video = getIntent().getExtras().getString("video_src");
        String pathAsociation = getIntent().getExtras().getString("path_asociation");
        String pathVideo = URL_CONSULTA + pathAsociation + video;

        playVideoView(pathVideo);
    }

    private void playVideoView(String pathVideo) {
        videoView = findViewById(R.id.vvVideo);
        try {
            videoView.setVideoPath(pathVideo);
            videoView.start();
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }


        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                return false;
            }
        });
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                //do something when video is ready to play
                mediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                    @Override
                    public void onSeekComplete(MediaPlayer mp) {
                        //TODO: Your code here
                        videoView.start();
                        View decorView = getWindow().getDecorView();
                        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_FULLSCREEN;
                        decorView.setSystemUiVisibility(uiOptions);
                    }
                });
            }
        });
        videoView.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {
                return false;
            }
        });
    }



    @Override
    public void onClick(View view) {
//        switch(view.getId()){
//            case R.id.bData:
//                if(!b){
//                    b = true;
//                    bData.setText("Cerrar datos");
//                    scView.setVisibility(View.VISIBLE);
//                    videoView.stopPlayback();
//                    videoView.setVisibility(View.GONE);
//                    llButtons.setVisibility(View.GONE);
//                    llImg.setVisibility(View.GONE);
//                }else{
//                    b = false;
//                    bData.setText("Ver datos");
//                    scView.setVisibility(View.GONE);
//                    videoView.setVisibility(View.VISIBLE);
//                    llButtons.setVisibility(View.VISIBLE);
//                    llImg.setVisibility(View.VISIBLE);
//                    videoView.start();
//                }
//                break;
//            case R.id.bPlay:
//                videoView.start();
//                break;
//            case R.id.bStop:
//                videoView.stopPlayback();
//                videoView.resume();
//                break;
//            case R.id.bPause:
//                videoView.pause();
//                break;
//        }
    }

}
