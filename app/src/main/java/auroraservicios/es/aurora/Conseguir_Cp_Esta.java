package auroraservicios.es.aurora;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import modelos.Publicacion;

public class Conseguir_Cp_Esta extends AsyncTask<Object, Integer, Void> {

    private Devolver_Esta_CP respuesta;

    String urlConsulta = "https://auroraservicios.es/librerias/CP_Esta.php?";
    String charset = "UTF-8";
    URL connectURL;
    String id;
    boolean Estado=false;
    Context yo;
    Base_de_datos base_de_datos;
    Valores valores;
    String CP;
    boolean existe_1,existe_2,existe_3;


    ArrayList<String> Lista_CP=new ArrayList<>();

    Publicacion publicacion;

    public Conseguir_Cp_Esta(Devolver_Esta_CP respuesta, Context yo) {
        this.respuesta = respuesta;
        this.yo=yo;
        base_de_datos=new Base_de_datos(this.yo);
        valores=new Valores(this.yo);
        Log.i("Comprobacion_cp","Consutructor terminado");
    }

    @Override
    protected void onPreExecute() {
//        try {
//            connectURL = new URL(urlConsulta+"cp="+stringSubsection);
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    protected Void doInBackground(Object... objects) {

        try {
            for (int i=0;i<3;i++){
                if (i==0){
                    CP=valores.get_Primer_cp();
                }else if (i==1){
                    CP=valores.get_Segundo_cp();
                }else{
                    CP=valores.get_Tercer_cp();
                }
                connectURL = new URL(urlConsulta+"cp="+CP);
                Log.i("Comprobacion_cp","URL conectada - "+connectURL.toString());

                HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");//GET
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.connect();
                //extraemos los datos
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String resultadoConsulta = new String();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    resultadoConsulta += line;
                }
                JsonParser parser = new JsonParser();
                Log.d("Probando ", resultadoConsulta);
                JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
                try {
                    Lista_CP.clear();
                }catch (Exception e){
                }
                base_de_datos.eliminar(base_de_datos.CP_4);
                for (JsonElement elemento: jsonArray) {
                    JsonObject objeto = elemento.getAsJsonObject();
                    Log.w("Objeto", String.valueOf(objeto));
                    if(objeto.has("stringSubsection")){
                        String publicidad=objeto.get("stringSubsection").getAsString();
                        Lista_CP.add(publicidad);
                        Log.i("Comprobacion_cp","stringSubsection descargadp - "+publicidad);
                    }
                }
                connection.disconnect();
                Log.i("Comprobacion_cp","Tamaño de lista - "+Lista_CP.size());
                Log.i("Comprobacion_cp","Descarga terminada");

                if (i==0){
                    if (Lista_CP.size()>0){
                        existe_1=true;
                    }else{
                        existe_1=false;
                    }
                }else if (i==1){
                    if (Lista_CP.size()>0){
                        existe_2=true;
                    }else{
                        existe_2=false;
                    }
                }else{
                    if (Lista_CP.size()>0){
                        existe_3=true;
                    }else{
                        existe_3=false;
                    }
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        Estado=true;
        Log.i("Elejir_CP","Termina de comprobar");
        respuesta.onTaskCompleted(existe_1,existe_2,existe_3);
//        Toast.makeText(context, "Descargado", Toast.LENGTH_SHORT).show();
        Log.i("Comprobacion_cp","Mensaje de respuesta enviado");
    }



}
