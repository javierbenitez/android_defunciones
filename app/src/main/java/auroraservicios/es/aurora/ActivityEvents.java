package auroraservicios.es.aurora;

import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import adapters.EventAdapter;
import modelos.Asociation;
import modelos.Event;
import modelos.Subsection;

public class ActivityEvents extends AppCompatActivity implements Devolver_Listado_Publicidad {

    private GridView gvEvents;
    private String stringSubsection, stringCp, stringDate;

    private TextView tvSubsection, tvNoEvents, tvDay;

    private ArrayList itemsEvents;
    private EventAdapter eventAdapter;

    //Publicidad
    private Conseguir_Lista_Publicidad conseguir_lista_publicidad;
    private ViewPager Banner;
    private ArrayList<String> Fotos;
    private Display display;
    private int currentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarEvents);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        stringSubsection = getIntent().getExtras().getString("subsection");
        stringCp = getIntent().getExtras().getString("cp");
        stringDate = getIntent().getExtras().getString("month");
        String month = getNameMonth(stringDate);

        // Publicidad
        conseguir_lista_publicidad=new Conseguir_Lista_Publicidad(this,this, stringCp);
        conseguir_lista_publicidad.execute();
        Banner = findViewById(R.id.banner);
        Fotos = new ArrayList<>();
        display = getWindowManager().getDefaultDisplay();

        gvEvents = findViewById(R.id.gvEvents);
        itemsEvents = new ArrayList();
        tvSubsection = findViewById(R.id.tvSubsection);
        tvNoEvents = findViewById(R.id.tvNoEvents);
        tvDay = findViewById(R.id.tvDay);
        tvSubsection.setText(stringSubsection +" de "+stringCp);
        tvDay.setText(month);

        new WebServiceEvents().execute(stringSubsection);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menuWeb:
                openWebAurora();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openWebAurora(){
        String url = "https://servicios.auroraservicios.es";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        this.startActivity(i);
    }

    private String getNameMonth(String numberMonth){
        String stringMonth = "";
        switch (numberMonth) {
            case "1":  stringMonth = "Enero";
            case "01":  stringMonth = "Enero";
                break;
            case "2":  stringMonth  = "Febrero";
            case "02":  stringMonth  = "Febrero";
                break;
            case "3":  stringMonth = "Marzo";
            case "03":  stringMonth = "Marzo";
                break;
            case "4":  stringMonth = "Abril";
            case "04":  stringMonth = "Abril";
                break;
            case "5":  stringMonth = "Mayo";
            case "05":  stringMonth = "Mayo";
                break;
            case "6":  stringMonth = "Junio";
            case "06":  stringMonth = "Junio";
                break;
            case "7":  stringMonth = "Julio";
            case "07":  stringMonth = "Julio";
                break;
            case "8":  stringMonth = "Agosto";
            case "08":  stringMonth = "Agosto";
                break;
            case "9":  stringMonth = "Septiembre";
            case "09":  stringMonth = "Septiembre";
                break;
            case "10": stringMonth = "Octubre";
                break;
            case "11": stringMonth = "Noviembre";
                break;
            case "12": stringMonth = "Diciembre";
                break;
        }
        return stringMonth;
    }

    public class WebServiceEvents extends AsyncTask<String, Integer, Void> {

        String urlConsulta = "";
        URL connectURL;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(String... objects) {

            if(objects[0].equals("Asociaciones")){
                urlConsulta = "http://www.servicios.auroraservicios.es/api/v1/asociations";
            }else{
                urlConsulta = "http://www.servicios.auroraservicios.es/api/v1/events";
            }
            try {
                String encodedurl = urlConsulta+
                        "?cp="+URLEncoder.encode(stringCp, "utf-8")+
                        "&s="+URLEncoder.encode(stringSubsection,"UTF-8")+
                        "&month="+URLEncoder.encode(stringDate,"UTF-8");
                Log.w("URL Consulta", encodedurl);
                connectURL = new URL(encodedurl);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            try {
                //            ------------------ CODIGO GROBAL----------------------------
                HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");//GET
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.connect();

                //extraemos los datos
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String resultadoConsulta = new String();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    resultadoConsulta += line;
                }

                JsonParser parser = new JsonParser();
                JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
                try {
                    itemsEvents.clear();
                } catch (Exception e) {
                }

                for (JsonElement elemento : jsonArray) {
                    JsonObject objeto = elemento.getAsJsonObject();
                    Subsection subsection = new Subsection(objeto.get("subsection").getAsString());
                    Event event = new Event(objeto.get("title").getAsString());
                    if(objects[0].equals("Asociaciones")) {
                        Log.w("Asociacion", String.valueOf(objeto.get("asociation").getAsString()));
                        event.setAsociation(objeto.get("asociation").getAsString());
                    }
                    event.setId(objeto.get("id").getAsInt());
                    event.setTitle(checkValueNull(objeto, "title"));
                    event.setSubtitle(checkValueNull(objeto, "subtitle"));
                    event.setLocation(checkValueNull(objeto, "location"));
                    event.setDescription(checkValueNull(objeto, "description"));
//                    event.setVideo(checkValueNull(objeto, "video"));
                    if(checkValueNull(objeto, "video") == ""){
                        if(checkValueNull(objeto, "url_video") == ""){
                            event.setVideo(null);
                        }else{
                            event.setVideo(objeto.get("url_video").getAsString());
                        }
                    }else{
                        event.setVideo(objeto.get("video").getAsString());
                    }
                    event.setSubsection(subsection);
                    event.setLocalidad(checkValueNull(objeto, "localidad"));
                    event.setImage(checkValueNull(objeto, "image1"));
                    event.setImage2(checkValueNull(objeto, "image2"));
                    event.setImage3(checkValueNull(objeto, "image3"));
                    event.setPath(checkValueNull(objeto, "path"));
                    event.setCp(checkValueNull(objeto, "cp"));
                    event.setDateEvent(checkValueNull(objeto, "date_event"));
                    event.setDateEndEvent(checkValueNull(objeto, "date_end_event"));
                    event.setTimeEvent(checkValueNull(objeto, "time_event"));
                    event.setTimeEndEvent(checkValueNull(objeto, "time_end_event"));
                    event.setLink(checkValueNull(objeto, "link"));
                    itemsEvents.add(event);
                }
                eventAdapter = new EventAdapter(ActivityEvents.this, itemsEvents, stringCp);
                connection.disconnect();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(itemsEvents.size() == 0){
                tvNoEvents.setVisibility(View.VISIBLE);
            }else{
                gvEvents.setAdapter(eventAdapter);
            }
        }
    }

    private String checkValueNull(JsonObject objeto, String value){
        if(!objeto.has(value) || objeto.get(value).isJsonNull())return "";
        if(objeto.get(value).getAsString().equals("null") || objeto.get(value).getAsString() == "null")
            return "";
        return objeto.get(value).getAsString();
    }

    @Override
    public void onTaskCompleted_publicidad(ArrayList<String> Publicidad) {
        if (Publicidad!=null){

            Fotos = Publicidad;
            if(Fotos.size() < 1) {
                Banner.setVisibility(View.GONE);
            }else{
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.height = FrameLayout.LayoutParams.MATCH_PARENT; //left, top, right, bottom
                Banner.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
                Point size = new Point();
                display.getSize(size);
            }

            Banner.setAdapter(new Adaptador_Banner(this,Fotos));

            Banner.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    super.onPageScrolled(position, positionOffset, positionOffsetPixels);

                }

                @Override
                public void onPageSelected(final int position) {
                    super.onPageSelected(position);
                    currentPage=position;
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                    super.onPageScrollStateChanged(state);
                }
            });
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == Fotos.size()) {
                        currentPage = 0;
                    }else{
                        currentPage++;
                    }
                    Banner.setCurrentItem(currentPage, true);
                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 5000, 5000);
        }
    }
}
