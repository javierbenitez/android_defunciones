package auroraservicios.es.aurora;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Valores {
    //    ----------------------

    public final String INICIO="INSERTAR_CODIGOS";
    public final String BOTONES="MOSTRAR_BOTONES";
    public final String LISTADO="LISTADO";
    public final String LISTADO_VACIO="LISTADO_VACIO";
    public final String PUBLICACION="PUBLICACION";
    public final String VIDEO="VIDEO";
    public final String SECCIONES = "SECCIONES";
    public final String NECROLOGICAS = "NECROLOGICAS";
    public final String CREAR_MISA = "CREAR_MISA";

    //    ----------------------
    SharedPreferences valores;
    SharedPreferences.Editor editor;
    Context context;


    public Valores(Context context) {
        valores= PreferenceManager.getDefaultSharedPreferences(context);
         editor = valores.edit();
    }
    public  void  set_Primer_cp(String cp){
        editor.putString("Primer_cp",cp);
        editor.commit();
    }

    public String get_Primer_cp(){
        return valores.getString("Primer_cp","ninguno");
    }

    //-------------------------

    public  void  set_Segundo_cp(String cp){
        editor.putString("Segundo_cp",cp);
        editor.commit();
    }

    public String get_Segundo_cp(){
        return valores.getString("Segundo_cp","ninguno");
    }
    //-------------------------

    public  void  set_Tercer_cp(String cp){
        editor.putString("Tercer_cp",cp);
        editor.commit();
    }

    public String get_Tercer_cp(){
        return valores.getString("Tercer_cp","ninguno");
    }

    //-------------------------

    public String get_Guarto_cp(){
        return valores.getString("Cuarto_cp","Global");
    }
    //-------------------------


    public  void  set_Primer_Inicio(Boolean inicio){
        editor.putBoolean("Primer_inicio",inicio);
        editor.commit();
    }

    public Boolean get_Primer_Inicio(){
        return valores.getBoolean("Primer_inicio",true);
    }

    public Boolean get_P_I(String nombre){
        return valores.getBoolean("P_I_"+nombre,true);
    }

    public void set_P_I(String nombre){
        editor.putBoolean("P_I_"+nombre,false);
        editor.commit();
    }

    public void setPrimerInicioImagen(String s, Boolean b){
        editor.putBoolean(s, b);
        editor.commit();
    }

    public Boolean getPrimerInicioImagen(String s){
        return valores.getBoolean(s, true);
    }
}
