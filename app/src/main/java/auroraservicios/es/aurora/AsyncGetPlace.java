package auroraservicios.es.aurora;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import modelos.Place;
import modelos.Publicacion;

public class AsyncGetPlace extends AsyncTask<Object, Integer, Void> {

    private ItemsPlace itemsPlace;

        String urlConsulta = "http://www.servicios.auroraservicios.es/api/v1/places/place/imagesPlace";
//    String urlConsulta = "https://www.auroraservicios.es/jbdodev1/auroraservicios/web/app_dev.php/api/v1/places/place/imagesPlace";
    String urlConsulta2 = "http://www.servicios.auroraservicios.es/api/v1/places/partsOfPlace/partOfPlace/imagesPlace";
//    String urlConsulta2 = "https://www.auroraservicios.es/jbdodev1/auroraservicios/web/app_dev.php/api/v1/places/partsOfPlace/partOfPlace/imagesPlace";

    String urlFinal = "";
    URL connectURL;
    int idPlace;
    boolean Estado = false, isPart;
    Context context;
    Valores valores;
    String url;

    ArrayList<String> imagesPlace = new ArrayList<>();

    Place place;

    public AsyncGetPlace(ItemsPlace itemsPlace, Context context, int idPlace, boolean isPart) {
        this.itemsPlace = itemsPlace;
        this.context = context;
        valores = new Valores(this.context);
        this.idPlace = idPlace;
        this.isPart = isPart;
    }

    @Override
    protected void onPreExecute() {
        try {
            if(isPart){
                urlFinal = urlConsulta2+"?part="+idPlace;
            }else {
                urlFinal = urlConsulta+"?place="+idPlace;
            }
            connectURL = new URL(urlFinal);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Void doInBackground(Object... objects) {

        try {

            HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("GET");//GET
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.connect();
            //extraemos los datos
            InputStream inputStream = new BufferedInputStream(connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String resultadoConsulta = new String();
            String line = "";
            while ((line = reader.readLine()) != null) {
                resultadoConsulta += line;
            }
            JsonParser parser = new JsonParser();
            JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
            try {
                imagesPlace.clear();
            }catch (Exception e){
            }
            for (JsonElement elemento: jsonArray) {
                JsonObject objeto = elemento.getAsJsonObject();
                String image = objeto.get("path_place").getAsString()+objeto.get("image").getAsString();
//                image = image+"__"+objeto.get("image").getAsString();
                imagesPlace.add(image);
            }
            connection.disconnect();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        Estado=true;
        itemsPlace.onTaskCompletedImages(imagesPlace);
//        Toast.makeText(context, "Descargado", Toast.LENGTH_SHORT).show();
    }


    public Place getPlace() {
        return place;
    }

}
