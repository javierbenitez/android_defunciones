package auroraservicios.es.aurora;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import adapters.CompanyAdapter;
import modelos.Company;
import modelos.Subsection;

public class ActivityCompanies extends AppCompatActivity {

    private GridView gvCompanies;
    String stringSubsection, stringCp;

    TextView tvSubsection;

    private ArrayList itemsCompany;
    private CompanyAdapter companyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_companies);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarSection);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        stringSubsection = getIntent().getExtras().getString("subsection");
        stringCp = getIntent().getExtras().getString("cp");

        gvCompanies = findViewById(R.id.gvCompananies);
        itemsCompany = new ArrayList();
        tvSubsection = findViewById(R.id.tvSubsection);
        tvSubsection.setText(stringSubsection +" de "+stringCp);

        new WebServiceSsubsections().execute("Subsections");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public class WebServiceSsubsections extends AsyncTask<String, Integer, Void> {

        String urlConsulta = "http://www.servicios.auroraservicios.es/api/v1/companies";
        URL connectURL;

        @Override
        protected void onPreExecute() {

            try {
                String encodedurl = urlConsulta+
                        "?cp="+URLEncoder.encode(stringCp, "utf-8")+
                        "&s="+URLEncoder.encode(stringSubsection,"UTF-8");
                connectURL = new URL(encodedurl);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... objects) {

            try {
                //            ------------------ CODIGO GROBAL----------------------------
                HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");//GET
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.connect();

                //extraemos los datos
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String resultadoConsulta = new String();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    resultadoConsulta += line;
                }

                JsonParser parser = new JsonParser();
                JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
                try {
                    itemsCompany.clear();
                } catch (Exception e) {
                }

                for (JsonElement elemento : jsonArray) {
                    JsonObject objeto = elemento.getAsJsonObject();
                    Subsection subsection = new Subsection(objeto.get("subsection").getAsString());
                    Company company = new Company(objeto.get("name").getAsString());
                    company.setId(objeto.get("id").getAsInt());
                    company.setDescription(checkValueNull(objeto, "description"));
                    company.setSchedule(checkValueNull(objeto, "schedule"));
                    company.setNameContact(checkValueNull(objeto, "name_contact"));
                    company.setLastNameContact(checkValueNull(objeto, "lastname_contact"));
                    company.setPhone1(checkValueNull(objeto, "phone1"));
                    company.setPhone2(checkValueNull(objeto, "phone2"));
                    company.setAddress(checkValueNull(objeto, "address"));
                    company.setUrl(checkValueNull(objeto, "url"));
                    company.setSubsection(subsection);
                    company.setLocalidad(checkValueNull(objeto, "localidad"));
                    company.setImage(checkValueNull(objeto, "image"));
                    company.setPath(checkValueNull(objeto, "path"));
                    company.setCp(checkValueNull(objeto, "cp"));
                    itemsCompany.add(company);
                }
                companyAdapter = new CompanyAdapter(ActivityCompanies.this, itemsCompany, stringCp);
                connection.disconnect();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            gvCompanies.setAdapter(companyAdapter);
        }
    }

    private String checkValueNull(JsonObject objeto, String value){
        if(!objeto.has(value) || objeto.get(value).isJsonNull())return "";
        if(objeto.get(value).getAsString().equals("null") || objeto.get(value).getAsString() == "null")
            return "";
        return objeto.get(value).getAsString();
    }
}
