package auroraservicios.es.aurora;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class Listar_Misas extends AppCompatActivity implements Devolver_Listado_Misas , Devolver_Listado_Publicidad {

    Adaptador_misa adaptador;

    ViewPager Banner;

    ArrayList<String> Fotos;

    TextView Codigo_Postal;

    GridView Listado;

    Base_de_datos_misa base_de_datos;

    ArrayList<Misa> Lista_Misas;

    String CP, GAID;

    ImageView IV_Vacio;

    TextView tvSection;

    TextView TV_Vacio;

    private int currentPage = 0;

    Valores Valores;

    Display display;

    Conseguir_Lista_Misas conseguir_lista_misas;
    Conseguir_Lista_Publicidad conseguir_lista_publicidad;

    ProgressDialog progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar__publicaciones_misas);

        Toolbar toolbar = findViewById(R.id.toolbarCeremony);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        display = getWindowManager().getDefaultDisplay();

        Valores=new Valores(this);

        TV_Vacio=findViewById(R.id.TV_Vacio);

        IV_Vacio=findViewById(R.id.IV_Vacio);

        CP=getIntent().getExtras().getString("cp");

        Codigo_Postal=findViewById(R.id.TV_CP);

        tvSection = findViewById(R.id.tvSection);
        tvSection.setText("Misas de "+CP);

        Listado=findViewById(R.id.GV_Listado);

        base_de_datos = new Base_de_datos_misa(this);

        conseguir_lista_misas=new Conseguir_Lista_Misas(this,this);
        conseguir_lista_misas.execute();
        conseguir_lista_publicidad=new Conseguir_Lista_Publicidad(this,this,CP);
        conseguir_lista_publicidad.execute();


        progress = new ProgressDialog(this);

        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setTitle("Actualizando lista");
        progress.setMessage("Espera unos instantes");
        progress.setCancelable(false);
        progress.show();


        Listado.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent iniciar=new Intent(Listar_Misas.this,Ver_Misa.class);
                Log.i("Pasar_Parametros","Identificador enviado - "+adaptador.getItem(position).getIdentificador());
                iniciar.putExtra("publicacion", adaptador.getItem(position));
                startActivity(iniciar);
            }
        });

        Banner=findViewById(R.id.banner);
        Fotos=new ArrayList<>();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskCompleted(ArrayList<Misa> Misas,ArrayList<String> Publicidad) {
        if (Misas!=null){
            Lista_Misas=base_de_datos.getMisas(CP);
            adaptador=new Adaptador_misa(this,Lista_Misas);
            Listado.invalidateViews();
            Listado.setAdapter(adaptador);

            progress.dismiss();
            if (adaptador.getCount()==0){
                IV_Vacio.setVisibility(View.VISIBLE);
                TV_Vacio.setVisibility(View.VISIBLE);
                Listado.setVisibility(View.GONE);
            }

//            if (Valores.get_P_I(Valores.LISTADO)){
//                new dialogo_tutorial_fotos(this ,Valores.LISTADO);
//            }
        }

    }

    @Override
    public void onTaskCompleted_publicidad(ArrayList<String> Publicidad) {
        if (Publicidad!=null){

            Fotos = Publicidad;
            if(Fotos.size() < 1) {
                Banner.setVisibility(View.GONE);
            }else{
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.height = FrameLayout.LayoutParams.MATCH_PARENT; //left, top, right, bottom
                Banner.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
                Point size = new Point();
                display.getSize(size);
                int heightGridView = (int) ((Math.ceil(size.y)/5)*3.25);

                ViewGroup.LayoutParams layoutParams = Listado.getLayoutParams();
                layoutParams.height = heightGridView;
                Log.e("Alto: ", String.valueOf(heightGridView));
                Listado.setLayoutParams(layoutParams);
            }
            Banner.setAdapter(new Adaptador_Banner(this,Fotos));

            Banner.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    super.onPageScrolled(position, positionOffset, positionOffsetPixels);

                }

                @Override
                public void onPageSelected(final int position) {
                    super.onPageSelected(position);
                    currentPage=position;
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                    super.onPageScrollStateChanged(state);
                }
            });
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == Fotos.size()) {
                        currentPage = 0;
                    }else{
                        currentPage++;
                    }
                    Banner.setCurrentItem(currentPage, true);
                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 5000, 5000);
        }
    }
}
