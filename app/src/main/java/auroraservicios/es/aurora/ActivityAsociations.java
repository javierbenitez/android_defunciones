package auroraservicios.es.aurora;

import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import adapters.AsociationAdapter;
import adapters.PlaceAdapter;
import modelos.Asociation;

public class ActivityAsociations extends AppCompatActivity {

    private GridView gvAsociations;
    private String stringSection, stringCp;

    private TextView tvSection;

    private ArrayList itemsAsociation;
    private AsociationAdapter asociationAdapter;

    //Publicidad
    private Conseguir_Lista_Publicidad conseguir_lista_publicidad;
    private ViewPager Banner;
    private ArrayList<String> Fotos;
    private Display display;
    private int currentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asociations);

        Toolbar toolbar = findViewById(R.id.toolbarAsociations);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        stringSection = getIntent().getExtras().getString("section");
        stringCp = getIntent().getExtras().getString("cp");

        gvAsociations = findViewById(R.id.gvAsociations);
        itemsAsociation = new ArrayList();
        tvSection = findViewById(R.id.tvSection);
        tvSection.setText(stringSection +" de "+stringCp);

        new WebServiceSsubsections().execute("Asociations");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menuWeb:
                openWebAurora();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openWebAurora(){
        String url = "https://servicios.auroraservicios.es";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        this.startActivity(i);
    }

    public class WebServiceSsubsections extends AsyncTask<String, Integer, Void> {

        String urlConsulta = "http://www.servicios.auroraservicios.es/api/v1/asociations";
        URL connectURL;

        @Override
        protected void onPreExecute() {

            try {
                String encodedurl = urlConsulta+
                        "?cp="+URLEncoder.encode(stringCp, "utf-8")+
                        "&s="+URLEncoder.encode(stringSection,"UTF-8");
                connectURL = new URL(encodedurl);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... objects) {

            try {
                //            ------------------ CODIGO GROBAL----------------------------
                HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");//GET
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.connect();

                //extraemos los datos
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String resultadoConsulta = new String();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    resultadoConsulta += line;
                }

                JsonParser parser = new JsonParser();
                JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
                try {
                    itemsAsociation.clear();
                } catch (Exception e) {
                }

                for (JsonElement elemento : jsonArray) {
                    JsonObject objeto = elemento.getAsJsonObject();
//                    Section section = new Section(objeto.get("section").getAsString());
                    Asociation asociation = new Asociation();
                    asociation.setId(objeto.get("id").getAsInt());
                    asociation.setTitle(checkValueNull(objeto, "title"));
                    asociation.setDescription(checkValueNull(objeto, "description"));
                    asociation.setLocation(checkValueNull(objeto, "location"));
                    asociation.setImage1(checkValueNull(objeto, "image1"));
                    asociation.setImage2(checkValueNull(objeto, "image2"));
                    asociation.setImage3(checkValueNull(objeto, "image3"));
                    asociation.setPath(checkValueNull(objeto, "path"));
                    asociation.setVideo(checkValueNull(objeto, "video"));
                    itemsAsociation.add(asociation);
                }
                asociationAdapter = new AsociationAdapter(ActivityAsociations.this,
                        itemsAsociation, stringCp);
                connection.disconnect();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            gvAsociations.setAdapter(asociationAdapter);
        }
    }

    private String checkValueNull(JsonObject objeto, String value){
        if(!objeto.has(value) || objeto.get(value).isJsonNull())return "";
        if(objeto.get(value).getAsString().equals("null") || objeto.get(value).getAsString() == "null")
            return "";
        return objeto.get(value).getAsString();
    }

}
