package auroraservicios.es.aurora;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

import adapters.PlaceAdapter;
import modelos.Place;
import modelos.Subsection;

public class ActivityNewspaper extends AppCompatActivity {

    String completePath = "";
    String urlpdf = "http://www.servicios.auroraservicios.es/uploads/brochures/";
    String section, cp, path, newspaper, idNewspaper;

    TextView tvNewspaper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdfviewer);

        Toolbar toolbar = findViewById(R.id.toolbarNewspaper);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        section = getIntent().getExtras().getString("section");
        cp = getIntent().getExtras().getString("cp");
        path = getIntent().getExtras().getString("path");
        newspaper = getIntent().getExtras().getString("newspaper");
        idNewspaper = String.valueOf(getIntent().getExtras().getInt("id_newspaper"));

        Log.w("ID prensa", idNewspaper);
        completePath = urlpdf + path + newspaper;

        tvNewspaper = findViewById(R.id.tvNewspaper);
        tvNewspaper.setText(section+" de "+cp);

        WebView webView = findViewById(R.id.wvPDF);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon){
                super.onPageStarted(view, url, favicon);

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

            }
        });

        new WebServicePDF().execute(completePath, idNewspaper);

        //Carga url de .PDF en WebView  mediante Google Drive Viewer.
        webView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + completePath);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class WebServicePDF extends AsyncTask<String, Integer, Void> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(String... objects) {

            String urlConsulta = "http://www.servicios.auroraservicios.es/api/v1/newspaper/newspaper/info";
            URL connectURL = null;
            String encodedurl = "";
            try {
                encodedurl = urlConsulta+
                        "?newspaper="+ URLEncoder.encode(String.valueOf(objects[1]),"UTF-8");
                connectURL = new URL(encodedurl);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            try {
                //            ------------------ CODIGO GROBAL----------------------------
                HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");//GET
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.connect();

                //extraemos los datos
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String resultadoConsulta = new String();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    resultadoConsulta += line;
                }


                connection.disconnect();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

        }
    }
}
