package auroraservicios.es.aurora;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import adapters.SectionAdapter;
import modelos.Section;

public class ActivitySections extends AppCompatActivity {

    private GridView gvSections;
    String CP;
    TextView TV_CP;

    private ArrayList itemsSection;
    private SectionAdapter sectionAdapter;
    private Valores valores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sections);

        Toolbar toolbar = findViewById(R.id.toolbarSection);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        valores = new Valores(this);

        CP = getIntent().getExtras().getString("cp");

//        if (valores.getPrimerInicioImagen("P_I_Secciones")) {
//            new dialogo_tutorial_fotos(this , valores.SECCIONES);
//        }else{
//            valores.setPrimerInicioImagen("P_I_Secciones", false);
//        }

        gvSections = findViewById(R.id.gvSections);
        itemsSection = new ArrayList();

        TV_CP=findViewById(R.id.TV_CP);
        TV_CP.setText(TV_CP.getText()+" "+CP);

        try {
            itemsSection.clear();
        } catch (Exception e) {
        }

//        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP){
//            // do something for phones running an SDK before lollipop
//            Section necrologicas = new Section("Necrológicas");
//            necrologicas.setPath("5406098195Byas/");
//            necrologicas.setImage("Necrológicas.png");
//            itemsSection.add(necrologicas);
//        }

        new WebServiceSections().execute("Sections");

        gvSections.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        valores.setPrimerInicioImagen("P_I_Secciones", false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menuWeb:
                openWebAurora();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openWebAurora(){
        String url = "https://servicios.auroraservicios.es";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        this.startActivity(i);
    }

    public class WebServiceSections extends AsyncTask<String, Integer, Void> {

        String urlConsulta = "http://www.servicios.auroraservicios.es/api/v1/section";
//        String urlConsulta = "https://www.auroraservicios.es/jbdodev1/auroraservicios/web/app_dev.php/api/v1/section";
        String urlConsulta2 = "http://www.servicios.auroraservicios.es/api/v1/newspaper";
        URL connectURL;
        URL connectURL2;

        @Override
        protected void onPreExecute() {

            try {
                String encodedurl = urlConsulta+
                        "?cp="+URLEncoder.encode(CP, "utf-8")+
                        "&asoc=1";
                String encodedurl2 = urlConsulta2+
                        "?cp="+URLEncoder.encode(CP, "utf-8");
                connectURL = new URL(encodedurl);
                connectURL2 = new URL(encodedurl2);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... objects) {
            try {
                //            ------------------ CODIGO GROBAL----------------------------
                HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
                HttpURLConnection connection2 = (HttpURLConnection) connectURL2.openConnection();

                connection.setDoInput(true);
                connection2.setDoInput(true);
                connection.setDoOutput(true);
                connection2.setDoOutput(true);
                connection.setUseCaches(false);
                connection2.setUseCaches(false);
                connection.setRequestMethod("GET");//GET
                connection2.setRequestMethod("GET");//GET
                connection.setReadTimeout(10000);
                connection2.setReadTimeout(10000);
                connection.setConnectTimeout(10000);
                connection2.setConnectTimeout(10000);
                connection.connect();
                connection2.connect();

                //extraemos los datos
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                InputStream inputStream2 = new BufferedInputStream(connection2.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                BufferedReader reader2 = new BufferedReader(new InputStreamReader(inputStream2));
                String resultadoConsulta = new String();
                String resultadoConsulta2 = new String();
                String line = "";
                String line2 = "";
                while ((line = reader.readLine()) != null) {
                    resultadoConsulta += line;
                }
                while ((line2 = reader2.readLine()) != null) {
                    resultadoConsulta2 += line2;
                }

                JsonParser parser = new JsonParser();
                JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
                JsonParser parser2 = new JsonParser();
                JsonArray jsonArray2 = parser2.parse(resultadoConsulta2).getAsJsonArray();

                for (JsonElement elemento : jsonArray) {
                    JsonObject objeto = elemento.getAsJsonObject();
                    Section section = new Section(objeto.get("name").getAsString());
                    section.setImage(checkValueNull(objeto, "image"));
                    section.setPath(checkValueNull(objeto, "path"));
                    section.setNewspaper("");
                    section.setBrochure(false);
                    itemsSection.add(section);
                }

                for (JsonElement elemento2 : jsonArray2) {
                    JsonObject objeto2 = elemento2.getAsJsonObject();
                    Section newspaper = new Section(checkValueNull(objeto2, "name"));
                    newspaper.setId(Integer.parseInt(checkValueNull(objeto2, "id")));
                    newspaper.setImage(null);
                    newspaper.setPath(checkValueNull(objeto2, "path"));
                    newspaper.setNewspaper(checkValueNull(objeto2, "brochure"));
                    newspaper.setBrochure(true);
                    itemsSection.add(newspaper);
                }

                connection.disconnect();
                connection2.disconnect();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            sectionAdapter = new SectionAdapter(ActivitySections.this, itemsSection, CP);
            gvSections.setAdapter(sectionAdapter);
        }

    }

    private String checkValueNull(JsonObject objeto, String value){
        if(objeto.get(value).isJsonNull())return "";
        if(objeto.get(value).getAsString().equals("null") || objeto.get(value).getAsString() == "null")
            return "";
        return objeto.get(value).getAsString();
    }

}
