package auroraservicios.es.aurora;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Adaptador_misa extends BaseAdapter {
    ArrayList<Misa> Lista_Misas;
    Misa misa;
    Activity activity;

    public Adaptador_misa(Activity activity, ArrayList<Misa> Lista_Misas) {
        this.Lista_Misas = Lista_Misas;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return Lista_Misas.size();
    }

    @Override
    public Misa getItem(int position) {
        misa=Lista_Misas.get(position);
        return misa;
    }

    @Override
    public long getItemId(int position) {
        misa=Lista_Misas.get(position);
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view=convertView;
        if (view==null){
            LayoutInflater layoutInflater=(LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=layoutInflater.inflate(R.layout.publicacion_lista_misas,null);
        }
        misa=Lista_Misas.get(position);

        TextView Nombre,Apellidos,E_Apodo,Apodo,Fecha_fallecido,Fecha_entierro,Lugar_Entierro,Hora_Entierro,Edad;

        ImageView Foto;

        Foto=view.findViewById(R.id.IV_Foto);

        Nombre=view.findViewById(R.id.TV_Nombre);
        Apellidos=view.findViewById(R.id.TV_Apellidos);
        Apodo=view.findViewById(R.id.TV_Apodo);
        E_Apodo=view.findViewById(R.id.TV_E_Apodo);
        Fecha_fallecido=view.findViewById(R.id.TV_Fecha_fallecido);
        Fecha_entierro=view.findViewById(R.id.TV_Fecha_entierro);
        Lugar_Entierro=view.findViewById(R.id.TV_Lugar_Entierro);
        Hora_Entierro=view.findViewById(R.id.TV_Hora_Entierro);
        Edad=view.findViewById(R.id.TV_Edad);

        Nombre.setText(misa.getNombre());
        Apellidos.setText(misa.getApellidos());
        if (misa.getApodo().trim().equals("")){
            E_Apodo.setVisibility(View.GONE);
            Apodo.setVisibility(View.GONE);
        }else {
            Apodo.setText(misa.getApodo());
        }
//        Apodo.setText(publicacion.getApodo());
        Fecha_fallecido.setText(misa.getFecha_fallecimiento());
        Fecha_entierro.setText(misa.getCeremonia());
        Lugar_Entierro.setText(misa.getLugar());
        Hora_Entierro.setText(misa.getHora());
        Edad.setText(misa.getEdad());

        return view;
    }
}
