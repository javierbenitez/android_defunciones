package auroraservicios.es.aurora;

import java.io.Serializable;

public class Misa implements Serializable {
    String identificador,nombre, apellidos, apodo, fecha_fallecimiento, marido, hijos,
            hijos_politicos, hermanos, hermanos_politicos, familiares, pareja, ceremonia, lugar,
            hora, edad, aniversario, premium, cp1, cp2, cp3, viudo, lugar2, ceremonia2, hora2, padres, foto, foto2;

    public Misa(String identificador, String nombre, String apellidos, String apodo, String fecha_fallecimiento,
                String marido, String hijos, String hijos_politicos, String hermanos,
                String hermanos_politicos, String familiares, String pareja, String ceremonia, String lugar,
                String hora, String aniversario, String padres) {
        this.identificador = identificador;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.apodo = apodo;
        this.fecha_fallecimiento = fecha_fallecimiento;
        this.marido = marido;
        this.hijos = hijos;
        this.hijos_politicos = hijos_politicos;
        this.hermanos = hermanos;
        this.hermanos_politicos = hermanos_politicos;
        this.familiares = familiares;
        this.pareja = pareja;
        this.ceremonia = ceremonia;
        this.lugar = lugar;
        this.hora = hora;
        this.aniversario = aniversario;
        this.padres = padres;
    }

    public Misa(String aniversario, String nombre, String apellidos, String apodo, String fecha_fallecimiento, String edad,
                String premium, String viudo, String hijos, String hijos_politicos, String hermanos,
                String hermanos_politicos, String familiares, String pareja, String lugar, String ceremonia,
                String hora, String cp1, String cp2, String cp3, String lugar2, String ceremonia2, String hora2) {
        this.aniversario = aniversario;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.apodo = apodo;
        this.fecha_fallecimiento = fecha_fallecimiento;
        this.edad = edad;
        this.premium = premium;
        this.viudo = viudo;
        this.hijos = hijos;
        this.hijos_politicos = hijos_politicos;
        this.hermanos = hermanos;
        this.hermanos_politicos = hermanos_politicos;
        this.familiares = familiares;
        this.pareja = pareja;
        this.lugar = lugar;
        this.ceremonia = ceremonia;
        this.hora = hora;
        this.cp1 = cp1;
        this.cp2 = cp2;
        this.cp3 = cp3;
        this.lugar2 = lugar2;
        this.ceremonia2 = ceremonia2;
        this.hora2 = hora2;
    }

    public String getFoto2() {
        return foto2;
    }

    public void setFoto2(String foto2) {
        this.foto2 = foto2;
    }

    public String getLugar2() {
        return lugar2;
    }

    public void setLugar2(String lugar2) {
        this.lugar2 = lugar2;
    }

    public String getCeremonia2() {
        return ceremonia2;
    }

    public void setCeremonia2(String ceremonia2) {
        this.ceremonia2 = ceremonia2;
    }

    public String getHora2() {
        return hora2;
    }

    public void setHora2(String hora2) {
        this.hora2 = hora2;
    }

    public String getCp1() {
        return cp1;
    }

    public void setCp1(String cp1) {
        this.cp1 = cp1;
    }

    public String getCp2() {
        return cp2;
    }

    public void setCp2(String cp2) {
        this.cp2 = cp2;
    }

    public String getCp3() {
        return cp3;
    }

    public void setCp3(String cp3) {
        this.cp3 = cp3;
    }

    public String getViudo() {
        return viudo;
    }

    public void setViudo(String viudo) {
        this.viudo = viudo;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public Misa() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getApodo() {
        return apodo;
    }

    public void setApodo(String apodo) {
        this.apodo = apodo;
    }

    public String getFecha_fallecimiento() {
        return fecha_fallecimiento;
    }

    public void setFecha_fallecimiento(String fecha_fallecimiento) {
        this.fecha_fallecimiento = fecha_fallecimiento;
    }

    public String getMarido() {
        return marido;
    }

    public void setMarido(String marido) {
        this.marido = marido;
    }

    public String getHijos() {
        return hijos;
    }

    public void setHijos(String hijos) {
        this.hijos = hijos;
    }

    public String getHijos_politicos() {
        return hijos_politicos;
    }

    public void setHijos_politicos(String hijos_politicos) {
        this.hijos_politicos = hijos_politicos;
    }

    public String getHermanos() {
        return hermanos;
    }

    public void setHermanos(String hermanos) {
        this.hermanos = hermanos;
    }

    public String getHermanos_politicos() {
        return hermanos_politicos;
    }

    public void setHermanos_politicos(String hermanos_politicos) {
        this.hermanos_politicos = hermanos_politicos;
    }

    public String getFamiliares() {
        return familiares;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public void setFamiliares(String familiares) {
        this.familiares = familiares;
    }

    public String getPareja() {
        return pareja;
    }

    public void setPareja(String pareja) {
        this.pareja = pareja;
    }


    public String getCeremonia() {
        return ceremonia;
    }

    public void setCeremonia(String ceremonia) {
        this.ceremonia = ceremonia;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getAniversario() {
        return aniversario;
    }

    public void setAniversario(String aniversario) {
        this.aniversario = aniversario;
    }

    public String getPadres() {
        return padres;
    }

    public void setPadres(String padres) {
        this.padres = padres;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    @Override
    public String toString() {
        return "Misa{" +
                "identificador='" + identificador + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", apodo='" + apodo + '\'' +
                ", fecha_fallecimiento='" + fecha_fallecimiento + '\'' +
                ", marido='" + marido + '\'' +
                ", hijos='" + hijos + '\'' +
                ", hijos_politicos='" + hijos_politicos + '\'' +
                ", hermanos='" + hermanos + '\'' +
                ", hermanos_politicos='" + hermanos_politicos + '\'' +
                ", familiares='" + familiares + '\'' +
                ", pareja='" + pareja + '\'' +
                ", ceremonia='" + ceremonia + '\'' +
                ", lugar='" + lugar + '\'' +
                ", hora='" + hora + '\'' +
                ", edad='" + edad + '\'' +
                ", aniversario='" + aniversario + '\'' +
                '}';
    }
}
