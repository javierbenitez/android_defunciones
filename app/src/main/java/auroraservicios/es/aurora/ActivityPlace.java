package auroraservicios.es.aurora;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import adapters.BannerTourismAdapter;
import adapters.PlaceAdapter;
import modelos.Event;
import modelos.Place;
import modelos.Subsection;

public class ActivityPlace extends AppCompatActivity implements View.OnClickListener, ItemsPlace {

    private static final String URL_MAIN = "http://www.servicios.auroraservicios.es/uploads/images/";
//    private static final String URL_MAIN = "https://www.auroraservicios.es/jbdodev1/auroraservicios/web/uploads/images/";
    private static final String URL_PlACE = URL_MAIN+"lugares/";
    private static final float APPBAR_ELEVATION = 14f;

    private String stringCp, subsection;
    private boolean isPLAYING, c;
    private Place place;

    private Toolbar toolbar;
    private TextView tvDescription, tvPlace;
    private ImageButton ibShare, ibAudio, ibMap;
    private Button bMoreInfo;
    private LinearLayout llBanner;

    // Banner
    private AsyncGetPlace getPlace;
    private ArrayList<String> Fotos;
    private ViewPager Banner;
    private Display display;
    private int currentPage = 0;
    // Fin Banner

    private ArrayList itemsPlace;
    private PlaceAdapter placeAdapter;
    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place);

        toolbar = findViewById(R.id.toolbarPlace);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        place = getIntent().getExtras().getParcelable("place");
        subsection = getIntent().getStringExtra("subsection");
        stringCp = getIntent().getStringExtra("cp");

        tvPlace = findViewById(R.id.tvTitle);
        tvDescription = findViewById(R.id.tvDescription);
        ibShare = findViewById(R.id.ibShare);
        ibAudio = findViewById(R.id.ibAudio);
        ibMap = findViewById(R.id.ibMap);
        bMoreInfo = findViewById(R.id.bMoreInfo);
        llBanner = findViewById(R.id.llBanner);

        if(place.getTotalParts() < 1){
            bMoreInfo.setVisibility(View.GONE);
        }

        // Banner
        getPlace = new AsyncGetPlace(this,this, place.getId(), false);
        getPlace.execute();
        Banner = findViewById(R.id.vpBanner);
        Fotos = new ArrayList<>();
        display = getWindowManager().getDefaultDisplay();
        // Fin Banner

        ibShare.setOnClickListener(this);
        ibAudio.setOnClickListener(this);
        ibMap.setOnClickListener(this);
        bMoreInfo.setOnClickListener(this);

        tvDescription.setText(place.getDescription());
        tvPlace.setText(place.getTitle());

        itemsPlace = new ArrayList();

//        new WebServicePlace().execute("info_place");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menuWeb:
                openWebAurora();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openWebAurora(){
        String url = "https://servicios.auroraservicios.es";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        this.startActivity(i);
    }

    @Override
    public void onClick(View view) {
        Intent i;
        switch(view.getId()){
            case R.id.ibShare:
                if(isPLAYING) {
                    stopPlaying();
                }
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Aurora Servicios");
                    String shareMessage= "Échale un vistazo a esta app, es muy interesante.\n\n";
                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "Elíge uno"));
                } catch(Exception e) {
                    //e.toString();
                }
                break;
            case R.id.ibMap:
                if(!place.getLatitud().equals("") && !place.getLongitud().equals("")) {
                    if(isPLAYING) {
                        stopPlaying();
                    }
                    String latitud = place.getLatitud();
                    String longitud = place.getLongitud();
                    String uri = "http://maps.google.com/maps?saddr=" + latitud + "," + longitud
                            + "&daddr=" + latitud + "," + longitud;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    startActivity(intent);
                }else{
                    Toast.makeText(ActivityPlace.this,
                            "No hay ubicación añadida para este lugar.",
                            Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.ibAudio:
                if(!place.getAudio().equals("")){
                    String pathAudio = URL_PlACE+place.getPath()+place.getAudio();
                    reproMedia(pathAudio);
                }else{
                    Toast.makeText(ActivityPlace.this,
                            "Este lugar no tiene la opción de audio añadida.",
                            Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.bMoreInfo:
                if(isPLAYING) {
                    stopPlaying();
                }
                i = new Intent(this, ActivityPartsOfPlace.class);
                i.putExtra("cp", stringCp);
                i.putExtra("subsection", subsection);
                i.putExtra("place", (Parcelable) place);
                startActivity(i);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(isPLAYING) {
            stopPlaying();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isPLAYING) {
            stopPlaying();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(isPLAYING) {
            stopPlaying();
        }
    }


    private void reproMedia(String path) {
        if (!isPLAYING) {
            mp = new MediaPlayer();
            isPLAYING = true;
            try {
                mp.setDataSource(path);
                mp.prepare();
                playPlaying();
            } catch (IOException e) {
                Log.e("AUDIO", "prepare() failed");
            }
        } else {
            if(!c) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.dialog_title);
                // Add the buttons
                builder.setPositiveButton(R.string.pause, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        pausePlaying();
                    }
                });
                builder.setNegativeButton(R.string.stop, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        stopPlaying();
                    }
                });
                // Set other dialog properties
                // Create the AlertDialog
                AlertDialog dialog = builder.create();
                dialog.show();
            }else{
                c = false;
                isPLAYING = true;
                playPlaying();
            }
        }
    }

    public void playPlaying(){
        mp.start();
    }

    public void pausePlaying() {
        if(!c) {
            c = true;
            mp.pause();
        }
    }

    public void stopPlaying() {
        isPLAYING = false;
        mp.release();
        mp = null;
    }

    public class WebServicePlace extends AsyncTask<String, Integer, Void> {

        String urlConsulta = "http://www.servicios.auroraservicios.es/api/v1/places/place/info" +
                "?place="+ place.getId();
        URL connectURL;

        @Override
        protected void onPreExecute() {
            try {
                connectURL = new URL(urlConsulta);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... objects) {

            try {
                //            ------------------ CODIGO GROBAL----------------------------
                HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");//GET
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.connect();

                //extraemos los datos
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String resultadoConsulta = new String();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    resultadoConsulta += line;
                }

                JsonParser parser = new JsonParser();
                JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
                try {
                    itemsPlace.clear();
                } catch (Exception e) {
                }

                for (JsonElement elemento : jsonArray) {
                    JsonObject objeto = elemento.getAsJsonObject();
                    Subsection subsection = new Subsection(objeto.get("subsection").getAsString());
                    Event event = new Event(objeto.get("title").getAsString());
//                    place.setSubsection(subsection);
//                    place.setDescription(checkValueNull(objeto, "subtitle"));
//                    place.setDescription(checkValueNull(objeto, "description"));
//                    place.setVideo(checkValueNull(objeto,"path"));
//                    place.setVideo(checkValueNull(objeto,"video"));
//                    place.setVideo(checkValueNull(objeto,"url_video"));
//                    place.setLocation(objeto.get("location").getAsString());
//                    place.setLocalidad(objeto.get("localidad").getAsString());
//                    place.setImage(checkValueNull(objeto, "image1"));
//                    place.setImage2(checkValueNull(objeto, "image2"));
//                    place.setImage3(checkValueNull(objeto, "image3"));
//                    place.setImage3(checkValueNull(objeto, "date_event"));
//                    place.setImage3(checkValueNull(objeto, "date_end_event"));
//                    place.setImage3(checkValueNull(objeto, "time_event"));
//                    place.setImage3(checkValueNull(objeto, "time_end_event"));
                    itemsPlace.add(event);
                }
                placeAdapter = new PlaceAdapter(ActivityPlace.this, itemsPlace, stringCp);
                connection.disconnect();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

        }
    }


    @Override
    public void onTaskCompletedImages(ArrayList<String> imagesPlace) {
        if (imagesPlace!=null){

            Fotos = imagesPlace;
            if(Fotos.size() < 1) {
                Banner.setVisibility(View.GONE);
                llBanner.setVisibility(View.GONE);
            }else{
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.height = FrameLayout.LayoutParams.MATCH_PARENT; //left, top, right, bottom
                Banner.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
                Point size = new Point();
                display.getSize(size);
            }

            Banner.setAdapter(new BannerTourismAdapter(this, Fotos));

            Banner.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    super.onPageScrolled(position, positionOffset, positionOffsetPixels);

                }

                @Override
                public void onPageSelected(final int position) {
                    super.onPageSelected(position);
                    currentPage=position;
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                    super.onPageScrollStateChanged(state);
                }
            });
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == Fotos.size()) {
                        currentPage = 0;
                    }else{
                        currentPage++;
                    }
                    Banner.setCurrentItem(currentPage, true);
                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 5000, 5000);
        }
    }
}
