package auroraservicios.es.aurora;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class Adaptador_Banner extends PagerAdapter {

    private ArrayList<String> Imagenes;
    private LayoutInflater inflater;
    private Context context;

    public Adaptador_Banner(Context context, ArrayList<String> Imagenes) {
        this.context = context;
        this.Imagenes=Imagenes;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        int t=Imagenes.size();
        return t;
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View myImageLayout = inflater.inflate(R.layout.banner, view, false);
        ImageButton myImage = myImageLayout.findViewById(R.id.imagen);

        final String[] publicidad = Imagenes.get(position).split("___");
        String foto = "https://auroraservicios.es" + publicidad[0];

        if(foto.indexOf("./") > 0){
            foto = foto.replace("./", "/");
        }

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int width = metrics.widthPixels; // ancho absoluto en pixels
        int height = myImageLayout.getHeight();

        Picasso.get()
                .load(foto)
                .resize(width, height)
                .centerCrop()
                .into(myImage);

        myImage.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                if(!publicidad[1].equals("null")) {
                    try
                    {
                        String urlPubli = publicidad[1].replace("\"", "");
                        Intent i = new Intent(context, WebViewPublish.class);
                        i.putExtra("url", urlPubli);
                        context.startActivity(i);
                    } catch (Exception e){

                    }
                }
            }
        });

        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

}