package auroraservicios.es.aurora;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Inicio extends AppCompatActivity implements Devolver_Esta_CP,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final String LOGTAG = "Aurora Servicios";

    private static final String URL_SET_TOKENDEVICE = "https://auroraservicios.es/librerias/Insertar_token_device_para_notifiaciones_push.php";

    private Context context = this;
    private Button B_Aceptar, B_Cancel;
    private EditText ET_CP1, ET_CP2, ET_CP3;
    private TextView tvCurrentLocation;
    private LinearLayout llLocation;
    private Valores Valores;
    private ProgressDialog progress;

    Conseguir_Cp_Esta conseguir_cp_esta;
    Insertar_Cp_faltante insertar_cp_faltante;
    Insertar_Cp_existente insertar_cp_existente;

    private String faltan = "", postalCode, locality;
    private boolean b;
    ArrayList<String> Lista_CP;
    ArrayList<String> Lista_CP_existennte;

    ProgressDialog pDialog;
    private final int idNotificacion_basica = 001;

    LocationManager locationManager;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private Marker marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        Valores = new Valores(this);
        Lista_CP = new ArrayList<>();
        Lista_CP_existennte= new ArrayList<>();
        pDialog = new ProgressDialog(this);

        String cp1 = getIntent().getStringExtra("cp1");
        String cp2 = getIntent().getStringExtra("cp2");
        String cp3 = getIntent().getStringExtra("cp3");

        if (!Valores.get_Primer_Inicio()) {
            Intent nueva_ventana = new Intent(Inicio.this, Seleccionar_CP.class);
            nueva_ventana.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(nueva_ventana);
        }

        B_Aceptar = findViewById(R.id.B_Aceptar);
        B_Cancel = findViewById(R.id.B_Cancel);
        tvCurrentLocation = findViewById(R.id.tvCurrentLocation);

        ET_CP1 = findViewById(R.id.ET_CP1);
        llLocation = findViewById(R.id.llLocation);
        ET_CP2 = findViewById(R.id.ET_CP2);
        ET_CP3 = findViewById(R.id.ET_CP3);

        if (cp1 != null) {
            ET_CP1.setText(cp1);   /* Hacer lo necesario en caso de haber recibido la fecha */
        }
        if (cp2 != null) {
            ET_CP2.setText(cp2);   /* Hacer lo necesario en caso de haber recibido la fecha */
        }
        if (cp3 != null) {
            ET_CP3.setText(cp3);   /* Hacer lo necesario en caso de haber recibido la fecha */
        }

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);



        ET_CP1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                ET_CP1.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(ET_CP1, InputMethodManager.SHOW_IMPLICIT);
                ET_CP1.setSelection(ET_CP1.getText().length());
                animar(true, llLocation);
                llLocation.setVisibility(View.VISIBLE);
                return true;
            }
        });

        ET_CP2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                ET_CP2.setText("");
                ET_CP2.requestFocus();
                llLocation.setVisibility(View.GONE);
                return false;
            }
        });

        ET_CP3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                ET_CP3.setText("");
                ET_CP3.requestFocus();
                llLocation.setVisibility(View.GONE);
                return false;
            }
        });

        progress = new ProgressDialog(this);

//        if (Valores.get_P_I(Valores.INICIO)){
//            new dialogo_tutorial_fotos(this ,Valores.INICIO);
//        }

        B_Cancel.setOnClickListener(this);
        B_Aceptar.setOnClickListener(this);
        tvCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        Log.d("mylog", "Not granted");
                        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                    } else
                        requestLocation();
                } else
                    requestLocation();
            }
        });

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location mCurrentLocation = locationResult.getLastLocation();
                LatLng myCoordinates = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
//                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myCoordinates, 13.0f));
//                if (marker == null) {
//                    marker = mMap.addMarker(new MarkerOptions().position(myCoordinates));
//                } else
//                    marker.setPosition(myCoordinates);
            }
        };

        if (Build.VERSION.SDK_INT >= 23) {
            Log.d("mylog", "Getting Location Permission");
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.d("mylog", "Not granted");
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else
                requestLocation();
        } else
            requestLocation();
    }

    private void acceptPostalCodes() {
        if (ET_CP1.getText().toString().trim().equals("")){
            Toast.makeText(Inicio.this, "Tienes que poner un código postal en la primera casilla", Toast.LENGTH_SHORT).show();
        }else{
            Valores.set_Primer_cp(ET_CP1.getText().toString());
            if (ET_CP2.getText().toString().trim().equals("")){
                Valores.set_Segundo_cp("vacio");
            }else{
                Valores.set_Segundo_cp(ET_CP2.getText().toString());
            }

            if (ET_CP3.getText().toString().trim().equals("")){
                Valores.set_Tercer_cp("vacio");
            }else{
                Valores.set_Tercer_cp(ET_CP3.getText().toString());
            }

            FirebaseApp.initializeApp(Inicio.this);
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( Inicio.this,  new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    String newToken = instanceIdResult.getToken();
                    String method = "change_token";
                    new WebServiceTask().execute(method, URL_SET_TOKENDEVICE, newToken, ET_CP1.getText().toString(), ET_CP2.getText().toString(), ET_CP3.getText().toString());
                }
            });

            Base_de_datos bd = new Base_de_datos(Inicio.this);
            bd.eliminar(bd.CP_1);
            bd.eliminar(bd.CP_2);
            bd.eliminar(bd.CP_3);

            Valores.set_Primer_Inicio(false);

            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setTitle("Comprobando");
            progress.setMessage("Espera unos instantes");
            progress.setCancelable(false);
            progress.show();
            conseguir_cp_esta = new Conseguir_Cp_Esta(Inicio.this, Inicio.this);
            conseguir_cp_esta.execute();
        }
    }

    private void animar(boolean mostrar, LinearLayout layout)
    {
        AnimationSet set = new AnimationSet(true);
        Animation animation = null;
        if (mostrar)
        {
            //since top until bottom
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, -1.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f
            );
        }
        else
        {    //since bottom until top
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, -1.0f
            );
        }
        //duración en milisegundos
        animation.setDuration(500);
        set.addAnimation(animation);
        LayoutAnimationController controller = new LayoutAnimationController(set, 0.25f);

        layout.setLayoutAnimation(controller);
        layout.startAnimation(animation);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Valores.set_Primer_Inicio(false);
    }

    @Override
    public void onTaskCompleted(Boolean esta_cp, Boolean esta_cp_2, Boolean esta_cp_3) {

        faltan = "";
        if (esta_cp != null && !esta_cp) {
            faltan = faltan + " " + Valores.get_Primer_cp();
            Lista_CP.add(Valores.get_Primer_cp());
        }else{
            Lista_CP_existennte.add(Valores.get_Primer_cp());
        }
        if (esta_cp_2 != null && !esta_cp_2 && !Valores.get_Segundo_cp().equals("vacio")) {
            faltan = faltan + "   " + Valores.get_Segundo_cp();
            Lista_CP.add(Valores.get_Segundo_cp());
        }else{
            Lista_CP_existennte.add(Valores.get_Segundo_cp());
        }
        if (esta_cp_3 != null && !esta_cp_3 && !Valores.get_Tercer_cp().equals("vacio")) {
            faltan = faltan + "   " + Valores.get_Tercer_cp();
            Lista_CP.add(Valores.get_Tercer_cp());
        }else{
            Lista_CP_existennte.add(Valores.get_Tercer_cp());
        }

        insertar_cp_faltante = new Insertar_Cp_faltante(Inicio.this, Inicio.this, Lista_CP);
        insertar_cp_faltante.execute();

        insertar_cp_existente =new Insertar_Cp_existente(Inicio.this,Inicio.this,Lista_CP_existennte);
        insertar_cp_existente.execute();
    }

    @Override
    public void inserccion_Completa(Boolean listo) {
        progress.dismiss();
        Intent nueva_ventana = new Intent(Inicio.this, Seleccionar_CP.class);
        nueva_ventana.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(nueva_ventana);

    }

    public AlertDialog createSimpleDialog(String codigos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Aviso")
                .setMessage("Los códigos postales " + codigos + " estarán operativos en los próximos 30 días")
                .setCancelable(false)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent nueva_ventana = new Intent(Inicio.this, Seleccionar_CP.class);
                        nueva_ventana.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(nueva_ventana);


                    }
                })
                .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

        return builder.create();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass, Context c) {
        ActivityManager manager = (ActivityManager) c.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.B_Aceptar:
                acceptPostalCodes();
                break;
            case R.id.B_Cancel:
                onBackPressed();
                break;
        }
    }

    class WebServiceTask extends AsyncTask<String, Integer, String> {

        Context ctx;
        ProgressDialog pDialog;
        String resultJSON = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Inicio.this);
            pDialog.setMessage("Procesando, espere...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
//            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String method = params[0];
            URL url = null;
            String string = params[1];
            String deviceToken = params[2];
            String cp1 = params[3];
            String cp2 = params[4];
            String cp3 = params[5];
            String resp = null;

            if (method.equals("change_token")) {// POST - Envío de token y codigos postales a la base de datos

                try {
                    url = new URL(string);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.setDoOutput(true);
                    connection.setUseCaches(false);
                    connection.setRequestMethod("POST");
                    connection.setConnectTimeout(30000000);
                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestProperty("Accept", "application/json");
                    connection.connect();

                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("tokenDevice", deviceToken);
                    jsonParam.put("typeOfDevice", "Android");
                    jsonParam.put("cp1", cp1);
                    jsonParam.put("cp2", cp2);
                    jsonParam.put("cp3", cp3);

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    writer.write(jsonParam.toString());
                    writer.flush();
                    writer.close();

                    int response = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();
                    resp = "Code response: "+String.valueOf(response);
                    if (response == HttpURLConnection.HTTP_OK) {

                        String line;
                        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                        while ((line = br.readLine()) != null) {
                            resp = "Response : "+String.valueOf(deviceToken);
                            result.append(line);
                        }

                    }
                    return resp;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            Log.w("Resultado", resp );
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
//            Toast.makeText(Inicio.this, result,Toast.LENGTH_LONG).show();
        }

    }


    private void getCityName(LatLng myCoordinates) {
        String myCity = "";
        Geocoder geocoder = new Geocoder(Inicio .this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(myCoordinates.latitude, myCoordinates.longitude, 1);
            String postalCode = addresses.get(0).getPostalCode();
            String locality = addresses.get(0).getLocality();
            if(!b){
                b = true;
                tvCurrentLocation.setText(postalCode+", "+locality);
            }else{
                b = false;
                ET_CP1.setText(postalCode);
                animar(false, llLocation);
                llLocation.setVisibility(View.GONE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void requestLocation() {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_MEDIUM);
        criteria.setPowerRequirement(Criteria.POWER_MEDIUM);
        String provider = locationManager.getBestProvider(criteria, true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(provider);
        if (location != null && (System.currentTimeMillis() - location.getTime()) <= 10000000 * 2) {
            LatLng myCoordinates = new LatLng(location.getLatitude(), location.getLongitude());
            getCityName(myCoordinates);
        } else {
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.setNumUpdates(1);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            Log.d("mylog", "Last location too old getting new location!");
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.requestLocationUpdates(locationRequest,
                    mLocationCallback, Looper.myLooper());
        }
    }

}
