package auroraservicios.es.aurora;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import modelos.Section;

public class WebServiceSections extends AsyncTask<Object, Integer, Void> {

    String urlConsulta = "http://www.servicios.auroraservicios.es/api/v1/secciones";
    URL connectURL;

    ArrayList<Section> sections = new ArrayList<>();

    Section section;

    @Override
    protected void onPreExecute() {
        try {
            connectURL = new URL(urlConsulta);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Void doInBackground(Object... objects) {

        try {
            //            ------------------ CODIGO GROBAL----------------------------
            HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("GET");//GET
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.connect();
            //extraemos los datos
            InputStream inputStream = new BufferedInputStream(connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String resultadoConsulta = new String();
            String line = "";
            while ((line = reader.readLine()) != null) {
                resultadoConsulta += line;
            }

            JsonParser parser = new JsonParser();
            JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
            try {
                sections.clear();
            } catch (Exception e) {
            }

            for (JsonElement elemento : jsonArray) {
                JsonObject objeto = elemento.getAsJsonObject();
                Section section = new Section(objeto.get("name").getAsString());
                sections.add(section);
            }
            connection.disconnect();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
//        Estado=true;
//        respuesta.onTaskCompleted(Lista_Publicaciones,null);
//        Toast.makeText(context, "Descargado", Toast.LENGTH_SHORT).show();
    }


}
