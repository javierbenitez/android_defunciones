package auroraservicios.es.aurora;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import adapters.SubsectionAdapter;
import modelos.Section;
import modelos.Subsection;

public class ActivitySubsections extends AppCompatActivity  {

    private GridView gvSubsections;
    String stringSection, stringCp;
    private Button bCrearMisa;

    TextView tvSection;

    private ArrayList itemsSubsection;
    private SubsectionAdapter subsectionAdapter;
    private Valores valores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subsections);

        Toolbar toolbar = findViewById(R.id.toolbarSection);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        if (valores.getPrimerInicioImagen("P_I_Necrologicas")) {
//            new dialogo_tutorial_fotos(this , valores.NECROLOGICAS);
//        }else{
//            valores.setPrimerInicioImagen("P_I_Necrologicas", false);
//        }

        stringSection = getIntent().getExtras().getString("section");
        stringCp = getIntent().getExtras().getString("cp");

        gvSubsections = findViewById(R.id.gvSubsections);
//        bCrearMisa = findViewById(R.id.b_CrearMisa);

        itemsSubsection = new ArrayList();

        tvSection = findViewById(R.id.tvSection);
        tvSection.setText(stringSection+" de "+stringCp);

        try {
            itemsSubsection.clear();
        } catch (Exception e) {
        }

        new WebServiceSsubsections().execute("Subsections");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        valores.setPrimerInicioImagen("P_I_Necrologicas", false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menuWeb:
                openWebAurora();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openWebAurora(){
        String url = "https://servicios.auroraservicios.es";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        this.startActivity(i);
    }

    public class WebServiceSsubsections extends AsyncTask<String, Integer, Void> {

        String urlConsulta = "http://www.servicios.auroraservicios.es/api/v1/section/subsection";
//        String urlConsulta = "https://www.auroraservicios.es/jbdodev1/auroraservicios/web/app_dev.php/api/v1/section/subsection";
        URL connectURL;

        @Override
        protected void onPreExecute() {

            try {
                String encodedurl = urlConsulta+
                        "?cp="+URLEncoder.encode(stringCp, "utf-8")+
                        "&s="+URLEncoder.encode(stringSection,"UTF-8")+
                        "&t=1";
                connectURL = new URL(encodedurl);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... objects) {

            try {
                //            ------------------ CODIGO GROBAL----------------------------
                HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");//GET
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.connect();

                //extraemos los datos
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String resultadoConsulta = new String();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    resultadoConsulta += line;
                }

                JsonParser parser = new JsonParser();
                JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();

                for (JsonElement elemento : jsonArray) {
                    JsonObject objeto = elemento.getAsJsonObject();
                    Subsection subsection = new Subsection(objeto.get("name").getAsString());
                    subsection.setImage(checkValueNull(objeto, "image"));
                    subsection.setPath(checkValueNull(objeto, "path"));
                    subsection.setSection(new Section(stringSection));
                    itemsSubsection.add(subsection);
                }

                connection.disconnect();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            subsectionAdapter = new SubsectionAdapter(ActivitySubsections.this, itemsSubsection, stringCp);
            gvSubsections.setAdapter(subsectionAdapter);
        }
    }

    private String checkValueNull(JsonObject objeto, String value){
        if(objeto.get(value).isJsonNull())return "";
        if(objeto.get(value).getAsString().equals("null") || objeto.get(value).getAsString() == "null")
            return "";
        return objeto.get(value).getAsString();
    }
}
