package auroraservicios.es.aurora;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

public class Portada extends AppCompatActivity implements Devolver_Codigo_Version{

    ProgressDialog progress;
    Valores Valores;
    Conseguir_Codigo_Version conseguir_codigo_version;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkIntent(getIntent());

        setContentView(R.layout.activity_portada);
        Valores=new Valores(this);
        conseguir_codigo_version =new Conseguir_Codigo_Version(this,this);
        conseguir_codigo_version.execute();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        checkIntent(intent);
    }

    public void descarga_completada(int Version){

        if (BuildConfig.VERSION_CODE>=Version){
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                        Intent nueva_ventana = new Intent(Portada.this, Inicio.class);
                        startActivity(nueva_ventana);
//                    }
                }
            },2500);
        } else{
            createSimpleDialog().show();
        }
    }
    public AlertDialog createSimpleDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Información");
        builder.setMessage("La aplicación esta desactualizada, por favor actualícela");
        builder.setCancelable(false);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent nueva_ventana = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName()));
                nueva_ventana.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(nueva_ventana);
//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName())));
            }
        });

        return builder.create();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("Version_1.3", "Entra al onRestart");
        Valores = new Valores(this);

        conseguir_codigo_version = new Conseguir_Codigo_Version(this, this);
        conseguir_codigo_version.execute();
    }

    public void checkIntent(Intent intent) {
        Log.w("CLICK ACTION", String.valueOf(intent.hasExtra("click_action")));
        if (intent.hasExtra("click_action")) {
            ClickActionHelper.startActivity(intent.getStringExtra("click_action"), intent.getExtras(), this);
        }
    }
}
