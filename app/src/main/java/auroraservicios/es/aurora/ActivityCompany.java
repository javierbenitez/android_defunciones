package auroraservicios.es.aurora;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import adapters.CompanyAdapter;
import modelos.Company;
import modelos.Subsection;

public class ActivityCompany extends AppCompatActivity {

    private GridView gvInfoCompany;
    String stringSubsection, stringCp;
    private Company company;


    private TextView tvCompany, tvNameContact, tvSchedule, tvPhone1, tvPhone2, tvAddress, tvLocality,
            tvUrl, tv_NameContact, tv_Schedule, tv_Phone1, tv_Phone2, tv_Address, tv_Locality, tv_Url;

    Subsection subsection;
    ImageButton ibCompanyLogo;

    private ArrayList itemsCompany;
    private CompanyAdapter companyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);

        Toolbar toolbar = findViewById(R.id.toolbarCompany);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        company = getIntent().getExtras().getParcelable("company");
        stringCp = getIntent().getStringExtra("cp");

        ibCompanyLogo = findViewById(R.id.ibCompanyLogo);
        tvCompany = findViewById(R.id.tvCompany);
        tvNameContact = findViewById(R.id.tvNameContact);
        tvSchedule = findViewById(R.id.tvSchedule);
        tvPhone1 = findViewById(R.id.tvPhone1);
        tvPhone2 = findViewById(R.id.tvPhone2);
        tvAddress = findViewById(R.id.tvAddress);
        tvUrl = findViewById(R.id.tvUrl);
        tvLocality = findViewById(R.id.tvLocality);

        tv_Schedule = findViewById(R.id.tv_Schedule);
        tv_NameContact = findViewById(R.id.tv_NameContact);
        tv_Phone1 = findViewById(R.id.tv_Phone1);
        tv_Phone2 = findViewById(R.id.tv_Phone2);
        tv_Address = findViewById(R.id.tv_Address);
        tv_Locality = findViewById(R.id.tv_Locality);
        tv_Url = findViewById(R.id.tv_Url);

        String urlPhoto = "https://auroraservicios.es/uploads/images/empresas/"+
                company.getPath()+company.getImage();
        urlPhoto = urlPhoto.replaceAll(" ", "%20");

        Picasso.get()
                .load(urlPhoto)
                .fit()
                .centerCrop()
                .into(ibCompanyLogo);

//        gvInfoCompany = findViewById(R.id.gvInfoCompany);

        itemsCompany = new ArrayList();

        ColorStateList oldColors =  tvCompany.getTextColors(); //save original colors

        tvCompany.setTextColor(Color.WHITE);
        tvCompany.setHintTextColor(Color.WHITE);
        tvCompany.setLinkTextColor(Color.RED);
        if(company.getImage().equals("")){
            tvCompany.setTextColor(oldColors);//restore original colors
        }
        tvCompany.setText(company.getName());

        if(!deleteLabelIfNull(tvNameContact, tv_NameContact, company.getNameContact())){
            tvNameContact.setText(company.getNameContact());
        }

        if(!deleteLabelIfNull(tvSchedule, tv_Schedule, company.getSchedule())){
            tvSchedule.setText(company.getSchedule());
        }

        if(!deleteLabelIfNull(tvPhone1, tv_Phone1, company.getPhone1())){
            tvPhone1.setText(company.getPhone1());
        }

        if(!deleteLabelIfNull(tvPhone2, tv_Phone2, company.getPhone2())){
            tvPhone2.setText(company.getPhone2());
        }

        if(!deleteLabelIfNull(tvAddress, tv_Address, company.getAddress())){
            tvAddress.setText(company.getAddress());
        }

        if(!deleteLabelIfNull(tvLocality, tv_Locality, company.getLocalidad())){
            tvLocality.setText(company.getLocalidad());
        }

        if(!deleteLabelIfNull(tvUrl, tv_Url, company.getUrl())){
            tvUrl.setText(company.getUrl());
        }

        new WebServiceCompany().execute("info_company");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public class WebServiceCompany extends AsyncTask<String, Integer, Void> {

        String urlConsulta = "https://auroraservicios.es/api/v1/companies/company/info" +
                "?company="+ company.getId();
        URL connectURL;

        @Override
        protected void onPreExecute() {
            try {
                connectURL = new URL(urlConsulta);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... objects) {

            try {
                //            ------------------ CODIGO GROBAL----------------------------
                HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");//GET
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.connect();

                //extraemos los datos
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String resultadoConsulta = new String();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    resultadoConsulta += line;
                }
//
//                JsonParser parser = new JsonParser();
//                JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
//                try {
//                    itemsCompany.clear();
//                } catch (Exception e) {
//                }

//                for (JsonElement elemento : jsonArray) {
//                    JsonObject objeto = elemento.getAsJsonObject();
//                    Subsection subsection = new Subsection(objeto.get("subsection").getAsString());
//                    Company company = new Company(objeto.get("name").getAsString());
//                    company.setNameContact(objeto.get("name_contact").getAsString());
//                    company.setLastNameContact(objeto.get("lastname_contact").getAsString());
//                    company.setPhone1(objeto.get("phone1").getAsString());
//                    company.setPhone2(objeto.get("phone2").getAsString());
//                    company.setAddress(objeto.get("address").getAsString());
//                    company.setVideo(objeto.get("url").getAsString());
//                    company.setSubsection(subsection);
//                    company.setLocalidad(objeto.get("localidad").getAsString());
//                    company.setImage(checkValueNull(objeto, "image"));
//                    itemsCompany.add(company);
//                }
//                companyAdapter = new CompanyAdapter(ActivityCompany.this, itemsCompany, stringCp);
                connection.disconnect();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
//            gvInfoCompany.setAdapter(companyAdapter);
        }
    }

    private String checkValueNull(JsonObject objeto, String value){
        if(objeto.get(value).isJsonNull())return "";
        if(objeto.get(value).getAsString().equals("null") || objeto.get(value).getAsString() == "null")
            return "";
        return objeto.get(value).getAsString();
    }

    private boolean deleteLabelIfNull(TextView label, TextView field, String value) {
        if(value.equals("")){
            label.setVisibility(View.GONE);
            field.setVisibility(View.GONE);
            return true;
        }
        return false;
    }
}
