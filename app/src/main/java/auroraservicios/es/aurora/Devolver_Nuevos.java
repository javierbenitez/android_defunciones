package auroraservicios.es.aurora;

public interface Devolver_Nuevos {
    void primer_CP(Boolean listo);
    void segundo_CP(Boolean listo);
    void tercer_CP(Boolean listo);
    void cuarto_CP(Boolean listo);
}
