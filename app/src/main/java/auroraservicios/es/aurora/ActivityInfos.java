package auroraservicios.es.aurora;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import adapters.InfoAdapter;
import modelos.Info;
import modelos.Subsection;

public class ActivityInfos extends AppCompatActivity {

    private ListView lvInfos;
    private TextView tvSubsection;

    private String stringSubsection, stringCp;

    private ArrayList itemsInfos;
    private InfoAdapter infosAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infos);

        Toolbar toolbar = findViewById(R.id.toolbarSection);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        stringSubsection = getIntent().getExtras().getString("subsection");
        stringCp = getIntent().getExtras().getString("cp");

        lvInfos = findViewById(R.id.llvInfos);
        itemsInfos = new ArrayList();
        tvSubsection = findViewById(R.id.tvSubsection);
        tvSubsection.setText(stringSubsection +" de "+stringCp);

        new WebServiceSsubsections().execute("Subsections");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class WebServiceSsubsections extends AsyncTask<String, Integer, Void> {

        String urlConsulta = "http://www.servicios.auroraservicios.es/api/v1/infos";
        URL connectURL;

        @Override
        protected void onPreExecute() {

            try {
                String encodedurl = urlConsulta+
                        "?cp="+URLEncoder.encode(stringCp, "utf-8")+
                        "&s="+URLEncoder.encode(stringSubsection,"UTF-8");

                connectURL = new URL(encodedurl);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... objects) {

            try {
                //            ------------------ CODIGO GROBAL----------------------------
                HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");//GET
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.connect();

                //extraemos los datos
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String resultadoConsulta = new String();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    resultadoConsulta += line;
                }

                JsonParser parser = new JsonParser();
                JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
                try {
                    itemsInfos.clear();
                } catch (Exception e) {
                }

                for (JsonElement elemento : jsonArray) {
                    JsonObject objeto = elemento.getAsJsonObject();
                    Subsection subsection = new Subsection(objeto.get("subsection").getAsString());
                    Info info = new Info();
                    info.setName(checkValueNull(objeto, "name"));
                    info.setId(objeto.get("id").getAsInt());
                    info.setDescription(checkValueNull(objeto, "description"));
                    info.setFilibeg(checkValueNull(objeto, "filibeg"));
                    info.setSchedule(checkValueNull(objeto, "schedule"));
                    info.setPhone1(checkValueNull(objeto, "phone1"));
                    info.setPhone2(checkValueNull(objeto, "phone2"));
                    info.setAddress(checkValueNull(objeto, "address"));
                    info.setUrl(checkValueNull(objeto, "url"));
                    info.setSubsection(subsection);
                    info.setLocalidad(checkValueNull(objeto, "localidad"));
                    info.setImage(checkValueNull(objeto, "image"));
                    info.setPath(checkValueNull(objeto, "path"));
                    info.setCp(checkValueNull(objeto, "cp"));
                    itemsInfos.add(info);
                }
                infosAdapter = new InfoAdapter(ActivityInfos.this, itemsInfos, stringCp);
                connection.disconnect();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            lvInfos.setAdapter(infosAdapter);
        }
    }

    private String checkValueNull(JsonObject objeto, String value){
        if(!objeto.has(value) || objeto.get(value).isJsonNull())return "";
        if(objeto.get(value).getAsString().equals("null") || objeto.get(value).getAsString() == "null")
            return "";
        return objeto.get(value).getAsString();
    }
}
