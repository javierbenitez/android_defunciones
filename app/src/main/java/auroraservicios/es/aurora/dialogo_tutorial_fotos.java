package auroraservicios.es.aurora;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;

public class dialogo_tutorial_fotos {

    Context yo;
    ImageButton Cerrar;
    ImageView Imagen;
    String imag;
    Valores v;

    public final String INICIO="INSERTAR_CODIGOS";
    public final String BOTONES="MOSTRAR_BOTONES";
    public final String LISTADO="LISTADO";
    public final String LISTADO_VACIO="LISTADO_VACIO";
    public final String PUBLICACION="PUBLICACION";
    public final String SECCIONES = "SECCIONES";
    public final String NECROLOGICAS = "NECROLOGICAS";
    public final String CREAR_MISA = "CREAR_MISA";

    boolean pulsado=false;
    public dialogo_tutorial_fotos(Context context,String imagen) {
        this.imag=imagen;
        yo=context;
        v=new Valores(yo);

        final Dialog dialogo=new Dialog(context);
        dialogo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogo.setCancelable(false);
        dialogo.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialogo.setContentView(R.layout.tutorial_imagenes);

        Cerrar=dialogo.findViewById(R.id.IB_Cerrar);

        Imagen=dialogo.findViewById(R.id.IV_Imagen);

        switch (imag){
            case INICIO:
                Imagen.setImageResource(R.drawable.imagen_insertar_codigos);
                break;
            case BOTONES:
                Imagen.setImageResource(R.drawable.imagen_botones_codigos);
                break;
            case LISTADO:
                Imagen.setImageResource(R.drawable.imagen_listado);
                break;
            case LISTADO_VACIO:
                Imagen.setImageResource(R.drawable.imagen_listado_vacio);
                break;
            case PUBLICACION:
                Imagen.setImageResource(R.drawable.imagen_individual_publicacion);
                break;
            case SECCIONES:
                Imagen.setImageResource(R.drawable.imagen_secciones);
                v.setPrimerInicioImagen(imag, true);
                break;
            case NECROLOGICAS:
                Imagen.setImageResource(R.drawable.imagen_necrologicas);
                break;
            case CREAR_MISA:
                Imagen.setImageResource(R.drawable.imagen_crear_misa);
                break;
        }

        Cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                v.set_P_I(imag);
                if (imag.equals(LISTADO)){
                    new dialogo_tutorial_fotos(yo ,LISTADO_VACIO);
                }
            dialogo.dismiss();
            }
        });
        dialogo.show();
    }

}

