package auroraservicios.es.aurora;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import modelos.Publicacion;

public class Ver_Publicacion extends AppCompatActivity {

    Publicacion publicacion;
    TextView E_Nombre,Nombre,E_Apellidos,Apellidos,E_Apodo,Apodo,E_F_Fallecimiento,F_Fallecimineto,E_Edad,Edad, E_Viudo, Viudo,E_Hijos,Hijos,E_Hijos_Politicos,Hijos_Politicos,
            E_Hermanos,Hermanos,E_Hermanos_Politicos,Hermanos_Politicos,E_Familiares,Familiares,E_Pareja,Pareja,E_D_Recibe,D_Recibe,E_D_Despide,D_Despide,E_Lugar_Tanatorio,Lugar_Tanatorio,
            E_Lugar_Iglesia,Lugar_Iglesia,E_Fecha_Ceremonia,Fecha_Ceremonia,E_Hora,Hora, E_direccion, direccion, E_localidadCerem2,
            localidadCerem2, E_iglesiaCerem2, iglesiaCerem2, E_fechaCerem2, fechaCerem2, E_horaCerem2, horaCerem2, E_Padres, tvPadres,
            E_Phone1, phone1, E_Phone2, phone2;

    WebView Texto;

    TextView tvSection;

    ImageView Foto, foto2;
    Valores Valores;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver__publicacion);

        Toolbar toolbar = findViewById(R.id.toolbarD);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvSection = findViewById(R.id.tvSection);
        tvSection.setText("Difunto");

        Valores=new Valores(this);

        publicacion=(Publicacion) getIntent().getExtras().getSerializable("publicacion");

        Foto=findViewById(R.id.IV_Foto);
        foto2 = findViewById(R.id.IV_Foto2);
        String pathFoto1 = publicacion.getFoto();
        if(publicacion.getFoto().indexOf("./") > -1){
            pathFoto1 = publicacion.getFoto().replace("./", "/");
        }
        String url="https://auroraservicios.es"+pathFoto1;

        Log.w("URL PHOTO", url);
        if(!publicacion.getFoto2().equals("")){
            String pathFoto2 = publicacion.getFoto2();
            if(publicacion.getFoto2().indexOf("./") > -1){
                pathFoto2 = publicacion.getFoto2().replace("./", "/");
            }
            String url2="https://auroraservicios.es"+pathFoto2;
            Picasso.get()
                    .load(url2)
                    .fit()
                    .centerInside()
                    .into(foto2);
        }

        Picasso.get()
                .load(url)
                .fit()
                .centerInside()
                .into(Foto);

        Texto=findViewById(R.id.WV_Texto);

        if (publicacion.getTexto().trim().equals("")) {
            Texto.setVisibility(View.GONE);
        }else{
            Texto.loadData(publicacion.getTexto(), "text/html; charset=utf-8", "utf-8");
        }

        E_Nombre=findViewById(R.id.TV_E_Nombre);
        Nombre=findViewById(R.id.TV_Nombre);

        E_Apellidos=findViewById(R.id.TV_E_Apellidos);
        Apellidos=findViewById(R.id.TV_Apellidos);

        E_Apodo=findViewById(R.id.TV_E_Apodo);
        Apodo=findViewById(R.id.TV_Apodo);

        E_direccion = findViewById(R.id.TV_E_Direccion);
        direccion = findViewById(R.id.TV_Direccion);

        E_F_Fallecimiento=findViewById(R.id.TV_E_F_Fallecimiento);
        F_Fallecimineto=findViewById(R.id.TV_F_Fallecimiento);

        E_Edad=findViewById(R.id.TV_E_Edad);
        Edad=findViewById(R.id.TV_Edad);

        E_Padres = findViewById(R.id.TV_E_Padres);
        tvPadres = findViewById(R.id.TV_Padres);

        E_Viudo =findViewById(R.id.TV_E_Viudo);
        Viudo =findViewById(R.id.TV_Viudo);

        E_Hijos=findViewById(R.id.TV_E_Hijos);
        Hijos=findViewById(R.id.TV_Hijos);

        E_Hijos_Politicos=findViewById(R.id.TV_E_Hijos_Politicos);
        Hijos_Politicos=findViewById(R.id.TV_Hijos_Politicos);

        E_Hijos_Politicos=findViewById(R.id.TV_E_Hijos_Politicos);
        Hijos_Politicos=findViewById(R.id.TV_Hijos_Politicos);

        E_Hermanos=findViewById(R.id.TV_E_Hermanos);
        Hermanos=findViewById(R.id.TV_Hermanos);

        E_Hermanos_Politicos=findViewById(R.id.TV_E_Hermanos_Politicos);
        Hermanos_Politicos=findViewById(R.id.TV_Hermanos_Politicos);

        E_Familiares=findViewById(R.id.TV_E_Familiares);
        Familiares=findViewById(R.id.TV_Familiares);

        E_Pareja=findViewById(R.id.TV_E_Pareja);
        Pareja=findViewById(R.id.TV_Pareja);

        E_D_Recibe=findViewById(R.id.TV_E_D_Recibe);
        D_Recibe=findViewById(R.id.TV_D_Recibe);

        E_D_Despide=findViewById(R.id.TV_E_D_Despide);
        D_Despide=findViewById(R.id.TV_D_Despide);

        E_Lugar_Tanatorio=findViewById(R.id.TV_E_Lugar);
        Lugar_Tanatorio=findViewById(R.id.TV_Lugar);
        E_Lugar_Iglesia=findViewById(R.id.TV_E_Lugar_Iglesia);
        Lugar_Iglesia=findViewById(R.id.TV_Lugar_Iglesia);
        E_Fecha_Ceremonia=findViewById(R.id.TV_E_F_Ceremonio);
        Fecha_Ceremonia=findViewById(R.id.TV_F_Ceremonia);
        E_Hora=findViewById(R.id.TV_E_Hora);
        Hora=findViewById(R.id.TV_Hora);

        E_Phone1 = findViewById(R.id.TV_E_Phone1);
        phone1 = findViewById(R.id.TV_Phone1);
        E_Phone2 = findViewById(R.id.TV_E_Phone2);
        phone2 = findViewById(R.id.TV_Phone2);

        E_localidadCerem2 = findViewById(R.id.TV_E_LocalidadCerem2);
        localidadCerem2 = findViewById(R.id.TV_LocalidadCerem2);
        E_iglesiaCerem2 = findViewById(R.id.TV_E_IglesiaCerem2);
        iglesiaCerem2 = findViewById(R.id.TV_IglesiaCerem2);
        E_fechaCerem2 = findViewById(R.id.TV_E_FechaCerem2);
        fechaCerem2 = findViewById(R.id.TV_FechaCerem2);
        E_horaCerem2 = findViewById(R.id.TV_E_HoraCerem2);
        horaCerem2 = findViewById(R.id.TV_HoraCerem2);


        if (publicacion.getApodo().trim().equals("")){
            E_Apodo.setVisibility(View.GONE);
            Apodo.setVisibility(View.GONE);
        }else {
            Apodo.setText(publicacion.getApodo());
        }
        if (publicacion.getDireccion().trim().equals("")){
            E_direccion.setVisibility(View.GONE);
            direccion.setVisibility(View.GONE);
        }else {
            direccion.setText(publicacion.getDireccion());
        }

        if (publicacion.getPadres().trim().equals("")){
            E_Padres.setVisibility(View.GONE);
            tvPadres.setVisibility(View.GONE);
        }else {
            tvPadres.setText(publicacion.getPadres());
        }

        if (publicacion.getMarido().trim().equals("")){
            E_Viudo.setVisibility(View.GONE);
            Viudo.setVisibility(View.GONE);
        }else {
            Viudo.setText(publicacion.getMarido());
        }
        if (publicacion.getHijos().trim().equals("")){
            E_Hijos.setVisibility(View.GONE);
            Hijos.setVisibility(View.GONE);
        }else {
            Hijos.setText(publicacion.getHijos());
        }
        if (publicacion.getHijos_politicos().trim().equals("")){
            E_Hijos_Politicos.setVisibility(View.GONE);
            Hijos_Politicos.setVisibility(View.GONE);
        }else {
            Hijos_Politicos.setText(publicacion.getHijos_politicos());
        }
        if (publicacion.getHermanos().trim().equals("")){
            E_Hermanos.setVisibility(View.GONE);
            Hermanos.setVisibility(View.GONE);
        }else {
            Hermanos.setText(publicacion.getHermanos());
        }
        if (publicacion.getHermanos_politicos().trim().equals("")){
            E_Hermanos_Politicos.setVisibility(View.GONE);
            Hermanos_Politicos.setVisibility(View.GONE);
        }else {
            Hermanos_Politicos.setText(publicacion.getHermanos_politicos());
        }
        if (publicacion.getFamiliares().trim().equals("")){
            E_Familiares.setVisibility(View.GONE);
            Familiares.setVisibility(View.GONE);
        }else {
            Familiares.setText(publicacion.getFamiliares());
        }
        if (publicacion.getPareja().trim().equals("")){
            E_Pareja.setVisibility(View.GONE);
            Pareja.setVisibility(View.GONE);
        }else {
            Pareja.setText(publicacion.getPareja());
        }
        if (publicacion.getD_recibe().trim().equals("")){
            E_D_Recibe.setVisibility(View.GONE);
            D_Recibe.setVisibility(View.GONE);
        }else {
            D_Recibe.setText(publicacion.getD_recibe());
        }
        if (publicacion.getD_despide().trim().equals("")){
            E_D_Despide.setVisibility(View.GONE);
            D_Despide.setVisibility(View.GONE);
        }else {
            D_Despide.setText(publicacion.getD_despide());
        }

        if (publicacion.getPhone1().trim().equals("")){
            E_Phone1.setVisibility(View.GONE);
            phone1.setVisibility(View.GONE);
        }else {
            phone1.setText(publicacion.getPhone1());
        }

        if (publicacion.getPhone2().trim().equals("")){
            E_Phone2.setVisibility(View.GONE);
            phone2.setVisibility(View.GONE);
        }else {
            phone2.setText(publicacion.getPhone2());
        }

        // Inicio Ceremonia 2
        if(isNull(publicacion.getLocalidadCerem2())){
            E_localidadCerem2.setVisibility(View.GONE);
            localidadCerem2.setVisibility(View.GONE);
        }else {
            localidadCerem2.setText(publicacion.getLocalidadCerem2().trim().replace("\"", ""));
        }
        if (isNull(publicacion.getIglesiaCerem2())){
            E_iglesiaCerem2.setVisibility(View.GONE);
            iglesiaCerem2.setVisibility(View.GONE);
        }else {
            iglesiaCerem2.setText(publicacion.getIglesiaCerem2().trim().replace("\"", ""));
        }
        if (isNull(publicacion.getFechaCerem2())){
            E_fechaCerem2.setVisibility(View.GONE);
            fechaCerem2.setVisibility(View.GONE);
        }else {
            fechaCerem2.setText(publicacion.getFechaCerem2().trim().replace("\"", ""));
        }
        if (isNull(publicacion.getHoraCerem2())){
            E_horaCerem2.setVisibility(View.GONE);
            horaCerem2.setVisibility(View.GONE);
        }else {
            horaCerem2.setText(publicacion.getHoraCerem2().trim().replace("\"", ""));
        }
        // Fin de datos de ceremonia 2

        Nombre.setText(publicacion.getNombre());
        Apellidos.setText(publicacion.getApellidos());
        F_Fallecimineto.setText(publicacion.getFecha_fallecimiento());
        Edad.setText(publicacion.getEdad());
        Lugar_Tanatorio.setText(publicacion.getLugar());
        Lugar_Iglesia.setText(publicacion.getLugar_iglesia());
        Fecha_Ceremonia.setText(publicacion.getCeremonia());
        Hora.setText(publicacion.getHora());

        if (Valores.get_P_I(Valores.PUBLICACION)){
            new dialogo_tutorial_fotos(this ,Valores.PUBLICACION);
        }


        phone1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int permissionCheck = ContextCompat.checkSelfPermission(
                        Ver_Publicacion.this, Manifest.permission.CALL_PHONE);
                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Ver_Publicacion.this, new String[]{Manifest.permission.CALL_PHONE}, 225);
                } else {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + phone1.getText().toString()));
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(callIntent);
                }
            }
        });

        phone2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int permissionCheck = ContextCompat.checkSelfPermission(
                        Ver_Publicacion.this, Manifest.permission.CALL_PHONE);
                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Ver_Publicacion.this, new String[]{Manifest.permission.CALL_PHONE}, 225);
                } else {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + phone2.getText().toString()));
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(callIntent);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isNull(String value){
        if(value.equals("") || value == "" || value.equals(null) || value == null){
            return true;
        }
        return false;
    }
}
