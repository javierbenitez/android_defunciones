package auroraservicios.es.aurora;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class P1_misa extends AsyncTask<String, String, String> {
    private Devolver_Nuevos_misa respuesta;

    private static final String NOTIFICATION_CHANNEL_ID_CP_1 = "my_notification_channel_CP_1";
    private static final String NOTIFICATION_CHANNEL_ID_CP_2 = "my_notification_channel_CP_2";
    private static final String NOTIFICATION_CHANNEL_ID_CP_3 = "my_notification_channel_CP_3";
    private static final String NOTIFICATION_CHANNEL_ID_CP_4 = "my_notification_channel_CP_4";
    private static final int Notificacion_ID_CP_1 = 1;
    private static final int Notificacion_ID_CP_2 = 2;
    private static final int Notificacion_ID_CP_3 = 3;
    private static final int Notificacion_ID_CP_4 = 4;
    private int i;
    Valores valores;
    int minutos = 1;
    Base_de_datos_misa base_de_datos;
    Context context;


    public P1_misa(Devolver_Nuevos_misa respuesta, Context context) {
        this.respuesta = respuesta;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        i = 1;
        valores = new Valores(context);
        base_de_datos = new Base_de_datos_misa(context);
    }

    @Override
    protected String doInBackground(String... strings) {
//        Log.i("Servicio_Notificacion_m", "i - " + i);
//        publishProgress("" + i);
//        i++;
//        Log.i("Servicio_Notificacion_m", "i - " + i);
//        publishProgress("" + i);
        try {
            //            ------------------ CODIGO GROBAL----------------------------
            String urlConsulta = "https://auroraservicios.es/librerias/Descargar_identificador_misa.php";
            URL connectURL = new URL(urlConsulta);

            HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("GET");//GET
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.connect();
            //extraemos los datos
            InputStream inputStream = new BufferedInputStream(connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String resultadoConsulta = new String();
            String line = "";
            while ((line = reader.readLine()) != null) {
                resultadoConsulta += line;
            }
            JsonParser parser = new JsonParser();
            JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
            for (JsonElement elemento : jsonArray) {
                JsonObject objeto = elemento.getAsJsonObject();
                Log.i("Servicio_Notificacion_m", "Conecta a la url - " + connection.toString());
                String codigo;
                try {
                    codigo = objeto.get("codigo").getAsString();

                } catch (Exception e) {
                    codigo = "0";
                }
                int Codigo_Web = Integer.parseInt(codigo);
                Log.i("Servicio_Notificacion_m", "Codigo Postal numero Global el maximo web es - " + Codigo_Web);
                int Codigo_local = base_de_datos.Mas_Grande("Grobal");
                Log.i("Servicio_Notificacion_m", "Codigo Postal numero Global el maximo local es - " + Codigo_local);
                if (Codigo_Web > Codigo_local) {
                    Log.i("Servicio_Notificacion_m", "Nuevo fallecido en el cp - Grobal");
                    respuesta.cuarto_CP_misa(true);
                }
            }
            connection.disconnect();
            //            ------------------PRIMER CODIGO POSTAL----------------------------
            connectURL = new URL(urlConsulta + "?cp=" + valores.get_Primer_cp());
            connection = (HttpURLConnection) connectURL.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("GET");//GET
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.connect();
            //extraemos los datos
            inputStream = new BufferedInputStream(connection.getInputStream());
            reader = new BufferedReader(new InputStreamReader(inputStream));
            resultadoConsulta = new String();
            line = "";
            while ((line = reader.readLine()) != null) {
                resultadoConsulta += line;
            }
            parser = new JsonParser();
            jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
            for (JsonElement elemento : jsonArray) {
                JsonObject objeto = elemento.getAsJsonObject();
                Log.i("Servicio_Notificacion_m", "Conecta a la url - " + connection.toString());
                String codigo;
                try {
                    codigo = objeto.get("codigo").getAsString();

                } catch (Exception e) {
                    codigo = "0";
                }
                int Codigo_Web = Integer.parseInt(codigo);
                Log.i("Servicio_Notificacion_m", "Codigo Postal numero 1 el maximo web es - " + Codigo_Web);
                int Codigo_local = base_de_datos.Mas_Grande(valores.get_Primer_cp());
                Log.i("Servicio_Notificacion_m", "Codigo Postal numero 1 el maximo local es - " + Codigo_local);
                if (Codigo_Web > Codigo_local) {
                    Log.i("Servicio_Notificacion_m", "Nuevo fallecido en el cp - " + valores.get_Primer_cp());
                    respuesta.primer_CP_misa(true);
                }
            }
            connection.disconnect();
            //            ------------------SEGUNDO CODIGO POSTAL----------------------------
            connectURL = new URL(urlConsulta + "?cp=" + valores.get_Segundo_cp());
            connection = (HttpURLConnection) connectURL.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("GET");//GET
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.connect();
            //extraemos los datos
            inputStream = new BufferedInputStream(connection.getInputStream());
            reader = new BufferedReader(new InputStreamReader(inputStream));
            resultadoConsulta = new String();
            line = "";
            while ((line = reader.readLine()) != null) {
                resultadoConsulta += line;
            }
            parser = new JsonParser();
            jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();

            for (JsonElement elemento : jsonArray) {
                JsonObject objeto = elemento.getAsJsonObject();
                Log.i("Servicio_Notificacion_m", "Conecta a la url - " + connection.toString());
                String codigo;
                try {
                    codigo = objeto.get("codigo").getAsString();

                } catch (Exception e) {
                    codigo = "0";
                }
                int Codigo_Web = Integer.parseInt(codigo);
                Log.i("Servicio_Notificacion_m", "Codigo Postal numero 2 el maximo web es - " + Codigo_Web);
                int Codigo_local = base_de_datos.Mas_Grande(valores.get_Segundo_cp());
                Log.i("Servicio_Notificacion_m", "Codigo Postal numero 2 el maximo local es - " + Codigo_local);
                if (Codigo_Web > Codigo_local) {
                    Log.i("Servicio_Notificacion_m", "Nuevo fallecido en el cp - " + valores.get_Segundo_cp());
                    respuesta.segundo_CP_misa(true);
                }
            }
            connection.disconnect();
            //            ------------------TERCER CODIGO POSTAL----------------------------
            connectURL = new URL(urlConsulta + "?cp=" + valores.get_Tercer_cp());
            connection = (HttpURLConnection) connectURL.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("GET");//GET
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.connect();
            //extraemos los datos
            inputStream = new BufferedInputStream(connection.getInputStream());
            reader = new BufferedReader(new InputStreamReader(inputStream));
            resultadoConsulta = new String();
            line = "";
            while ((line = reader.readLine()) != null) {
                resultadoConsulta += line;
            }
            parser = new JsonParser();
            jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();

            for (JsonElement elemento : jsonArray) {
                JsonObject objeto = elemento.getAsJsonObject();
                Log.i("Servicio_Notificacion_m", "Conecta a la url - " + connection.toString());
                String codigo;
                try {
                    codigo = objeto.get("codigo").getAsString();

                } catch (Exception e) {
                    codigo = "0";
                }
                int Codigo_Web = Integer.parseInt(codigo);
                Log.i("Servicio_Notificacion_m", "Codigo Postal numero 3 el maximo web es - " + Codigo_Web);
                int Codigo_local = base_de_datos.Mas_Grande(valores.get_Tercer_cp());
                Log.i("Servicio_Notificacion_m", "Codigo Postal numero 3 el maximo local es - " + Codigo_local);
                if (Codigo_Web > Codigo_local) {
                    Log.i("Servicio_Notificacion_m", "Nuevo fallecido en el cp - " + valores.get_Tercer_cp());
                    respuesta.tercer_CP_misa(true);
                }
            }
            connection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
//        Toast.makeText(context, "Numero - " + values[0], Toast.LENGTH_SHORT).show();

    }

}
