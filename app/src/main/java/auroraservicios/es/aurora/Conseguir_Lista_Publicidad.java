package auroraservicios.es.aurora;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import modelos.Publicacion;

public class Conseguir_Lista_Publicidad extends AsyncTask<Object, Integer, Void> {

    private Devolver_Listado_Publicidad respuesta;

    String urlConsulta = "https://auroraservicios.es/librerias/Descargar_publicidad.php?";
    String charset = "UTF-8";
    URL connectURL;
    String id;
    boolean Estado=false;
    Context yo;
    Valores valores;
    String CP;
    String CP2;
    String CP3;
    String url;
    String premium;

    ArrayList<String> Lista_Publicidad=new ArrayList<>();

    Publicacion publicacion;

    public Conseguir_Lista_Publicidad(Devolver_Listado_Publicidad respuesta, Context yo,String CP) {
        this.respuesta = respuesta;
        this.yo=yo;
        valores=new Valores(this.yo);
        this.CP=CP;
    }

    public String getCP() {
        return CP;
    }

    public void setCP(String CP) {
        this.CP = CP;
    }

    public String getCP2() {
        return CP2;
    }

    public void setCP2(String CP2) {
        this.CP2 = CP2;
    }

    public String getCP3() {
        return CP3;
    }

    public void setCP3(String CP3) {
        this.CP3 = CP3;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

    @Override
    protected void onPreExecute() {
        try {
            connectURL = new URL(urlConsulta+"cp="+CP);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Void doInBackground(Object... objects) {

        try {
            //            ------------------ CODIGO GROBAL----------------------------
            HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("GET");//GET
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.connect();
            //extraemos los datos
            InputStream inputStream = new BufferedInputStream(connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String resultadoConsulta = new String();
            String line = "";
            while ((line = reader.readLine()) != null) {
                resultadoConsulta += line;
            }
            JsonParser parser = new JsonParser();
            JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
            try {
                Lista_Publicidad.clear();
            }catch (Exception e){
            }
            for (JsonElement elemento: jsonArray) {
                JsonObject objeto = elemento.getAsJsonObject();
                String publicidad = objeto.get("imagen").getAsString();
                publicidad += "___"+objeto.get("url").toString();
                Lista_Publicidad.add(publicidad);
            }
            connection.disconnect();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        Estado=true;
        respuesta.onTaskCompleted_publicidad(Lista_Publicidad);
//        Toast.makeText(context, "Descargado", Toast.LENGTH_SHORT).show();
    }


    public Publicacion getPublicacion() {
        return publicacion;
    }

}
