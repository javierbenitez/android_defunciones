package auroraservicios.es.aurora;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import modelos.Publicacion;

public class Conseguir_Lista_Misas extends AsyncTask<Object, Integer, Void> {

    private Devolver_Listado_Misas respuesta;

    String urlConsulta = "https://auroraservicios.es/librerias/Descargar_datos_misas.php";
    String charset = "UTF-8";
    URL connectURL;
    String id;
    boolean Estado=false;
    Context yo;
    Base_de_datos_misa base_de_datos;
    Valores valores;


    ArrayList<Misa> Lista_Misas=new ArrayList<>();

    Publicacion publicacion;

    public Conseguir_Lista_Misas(Devolver_Listado_Misas respuesta, Context yo) {
        this.respuesta = respuesta;
        this.yo=yo;
        base_de_datos=new Base_de_datos_misa(this.yo);
        valores=new Valores(this.yo);
    }

    @Override
    protected void onPreExecute() {
        try {
            connectURL = new URL(urlConsulta);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Void doInBackground(Object... objects) {

        try {
            //            ------------------ CODIGO GROBAL----------------------------
            HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("GET");//GET
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.connect();
            //extraemos los datos
            InputStream inputStream = new BufferedInputStream(connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String resultadoConsulta = new String();
            String line = "";
            while ((line = reader.readLine()) != null) {
                resultadoConsulta += line;
            }
            JsonParser parser = new JsonParser();
            JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
            try {
                Lista_Misas.clear();
            }catch (Exception e){
            }
            base_de_datos.eliminar(base_de_datos.CP_4);
            for (JsonElement elemento: jsonArray) {
                Misa publicacion = setDataMisa(elemento);
                base_de_datos.insertar(publicacion,base_de_datos.CP_4);
                Lista_Misas.add(publicacion);
            }
            connection.disconnect();
            //            ------------------PRIMER CODIGO POSTAL----------------------------
            connectURL = new URL(urlConsulta+"?cp="+valores.get_Primer_cp());
            connection = (HttpURLConnection) connectURL.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("GET");//GET
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.connect();
            //extraemos los datos
            inputStream = new BufferedInputStream(connection.getInputStream());
            reader = new BufferedReader(new InputStreamReader(inputStream));
            resultadoConsulta = new String();
            line = "";
            while ((line = reader.readLine()) != null) {
                resultadoConsulta += line;
            }
            parser = new JsonParser();
            jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
            try {
                Lista_Misas.clear();
            }catch (Exception e){
            }
            base_de_datos.eliminar(base_de_datos.CP_1);
            for (JsonElement elemento: jsonArray) {
                Misa publicacion = setDataMisa(elemento);
                base_de_datos.insertar(publicacion,base_de_datos.CP_1);
                Lista_Misas.add(publicacion);
            }
            connection.disconnect();
            //            ------------------SEGUNDO CODIGO POSTAL----------------------------
            connectURL = new URL(urlConsulta+"?cp="+valores.get_Segundo_cp());
            connection = (HttpURLConnection) connectURL.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("GET");//GET
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.connect();
            //extraemos los datos
            inputStream = new BufferedInputStream(connection.getInputStream());
            reader = new BufferedReader(new InputStreamReader(inputStream));
            resultadoConsulta = new String();
            line = "";
            while ((line = reader.readLine()) != null) {
                resultadoConsulta += line;
            }
            parser = new JsonParser();
            jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
            try {
                Lista_Misas.clear();
            }catch (Exception e){
            }
            base_de_datos.eliminar(base_de_datos.CP_2);
            for (JsonElement elemento: jsonArray) {
                Misa publicacion = setDataMisa(elemento);
                base_de_datos.insertar(publicacion,base_de_datos.CP_2);
                Lista_Misas.add(publicacion);
            }
            connection.disconnect();
            //            ------------------TERCER CODIGO POSTAL----------------------------
            connectURL = new URL(urlConsulta+"?cp="+valores.get_Tercer_cp());
            connection = (HttpURLConnection) connectURL.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("GET");//GET
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.connect();
            //extraemos los datos
            inputStream = new BufferedInputStream(connection.getInputStream());
            reader = new BufferedReader(new InputStreamReader(inputStream));
            resultadoConsulta = new String();
            line = "";
            while ((line = reader.readLine()) != null) {
                resultadoConsulta += line;
            }
            parser = new JsonParser();
            jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
            try {
                Lista_Misas.clear();
            }catch (Exception e){
            }
            base_de_datos.eliminar(base_de_datos.CP_3);
            for (JsonElement elemento: jsonArray) {
                Misa publicacion = setDataMisa(elemento);
                base_de_datos.insertar(publicacion,base_de_datos.CP_3);
                Lista_Misas.add(publicacion);
            }
            connection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @NonNull
    private Misa setDataMisa(JsonElement elemento) {
        JsonObject objeto = elemento.getAsJsonObject();
        Misa publicacion = new Misa();
        publicacion.setIdentificador(checkFieldDataBase(objeto, "identificador"));
        publicacion.setNombre(checkFieldDataBase(objeto, "nombre"));
        publicacion.setApellidos(checkFieldDataBase(objeto, "apellidos"));
        publicacion.setApodo(checkFieldDataBase(objeto, "apodo"));
        publicacion.setFecha_fallecimiento(checkFieldDataBase(objeto, "fecha_fallecimiento"));
        publicacion.setMarido(checkFieldDataBase(objeto, "conyuge"));
        publicacion.setHijos(checkFieldDataBase(objeto, "hijos"));
        publicacion.setHijos_politicos(checkFieldDataBase(objeto, "hijos_politicos"));
        publicacion.setHermanos(checkFieldDataBase(objeto, "hermanos"));
        publicacion.setHermanos_politicos(checkFieldDataBase(objeto, "hermanos_politicos"));
        publicacion.setFamiliares(checkFieldDataBase(objeto, "familiares"));
        publicacion.setPareja(checkFieldDataBase(objeto, "pareja"));
        publicacion.setCeremonia(checkFieldDataBase(objeto, "fecha_ceremonia"));
        publicacion.setLugar(checkFieldDataBase(objeto, "lugar"));
        publicacion.setHora(checkFieldDataBase(objeto, "hora"));
        publicacion.setEdad(checkFieldDataBase(objeto, "edad"));
        publicacion.setAniversario(checkFieldDataBase(objeto, "aniversario"));
        publicacion.setLugar2(checkFieldDataBase(objeto, "lugar2"));
        publicacion.setCeremonia2(checkFieldDataBase(objeto, "fecha_cerem2"));
        publicacion.setHora2(checkFieldDataBase(objeto, "hora2"));
        publicacion.setPadres(checkFieldDataBase(objeto, "padres"));
        return publicacion;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        Estado=true;
        respuesta.onTaskCompleted(Lista_Misas,null);
//        Toast.makeText(context, "Descargado", Toast.LENGTH_SHORT).show();
    }

    /**
     *
     * @param objeto
     * @param value
     * @return
     */
    private String checkFieldDataBase(JsonObject objeto, String value){
        if(!objeto.has(value) || objeto.get(value).isJsonNull())return "";
        if(objeto.get(value).getAsString().equals("null") || objeto.get(value).getAsString() == "null")
            return "";
        return objeto.get(value).getAsString();
    }

}
