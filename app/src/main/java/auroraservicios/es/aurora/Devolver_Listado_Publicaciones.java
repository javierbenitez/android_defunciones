package auroraservicios.es.aurora;

import java.util.ArrayList;

import modelos.Publicacion;

public interface Devolver_Listado_Publicaciones {
    void onTaskCompleted(ArrayList<Publicacion> Publicaciones, ArrayList<String> Publicidad);
    //agregar un campo mas para cuando uno de lso dos sea null hacer algo y si no hacer otra cosa
}
