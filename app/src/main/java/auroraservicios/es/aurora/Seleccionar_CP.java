package auroraservicios.es.aurora;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.constraint.Guideline;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Seleccionar_CP extends AppCompatActivity {

    Button B_CP1,B_CP2,B_CP3,B_Pedir, bCondiciones;

    ImageView B_Configuracion;

    Valores Valores;

    Guideline G_CP1,G_CP2;

    String GAID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccionar__cp);
        Valores=new Valores(this);

        if (!isMyServiceRunning(Servicio.class,Seleccionar_CP.this)){
            Intent iy = new Intent(Seleccionar_CP.this, Servicio.class);
            Seleccionar_CP.this.startService(iy);
        }else{
            Log.i("Servicio_Notificacion","Servicio ya esta creado");
        }

        B_Configuracion=findViewById(R.id.B_Configuracion);

        B_CP1=findViewById(R.id.B_CP1);
        B_CP2=findViewById(R.id.B_CP2);
        B_CP3=findViewById(R.id.B_CP3);
        B_Pedir=findViewById(R.id.B_Pedir);
        bCondiciones = findViewById(R.id.bCondiciones);

        B_CP1.setText(Valores.get_Primer_cp());
        B_CP2.setText(Valores.get_Segundo_cp());
        B_CP3.setText(Valores.get_Tercer_cp());

        if (Valores.get_Segundo_cp().equals("vacio")){
            B_CP2.setVisibility(View.GONE);

        }
        if (Valores.get_Tercer_cp().equals("vacio")){
            B_CP3.setVisibility(View.GONE);
        }

//        if (Valores.get_P_I(Valores.BOTONES)){
//            new dialogo_tutorial_fotos(this ,Valores.BOTONES);
//        }

        B_Configuracion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nueva_ventana=new Intent(Seleccionar_CP.this,Inicio.class);
                if(!B_CP1.getText().toString().equals("") && !B_CP1.getText().toString().equals("vacio")){
                    nueva_ventana.putExtra("cp1", B_CP1.getText().toString());
                }
                if(!B_CP2.getText().toString().equals("") && !B_CP2.getText().toString().equals("vacio")){
                    nueva_ventana.putExtra("cp2", B_CP2.getText().toString());
                }
                if(!B_CP3.getText().toString().equals("") && !B_CP3.getText().toString().equals("vacio")){
                    nueva_ventana.putExtra("cp3", B_CP3.getText().toString());
                }

                startActivity(nueva_ventana);
                Valores.set_Primer_Inicio(true);
            }
        });

        B_CP1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nueva_ventana=new Intent(Seleccionar_CP.this,ActivitySections.class);
                nueva_ventana.putExtra("cp", B_CP1.getText().toString());
                startActivity(nueva_ventana);
            }
        });
        B_CP2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nueva_ventana=new Intent(Seleccionar_CP.this,ActivitySections.class);
                nueva_ventana.putExtra("cp", B_CP2.getText().toString());
                startActivity(nueva_ventana);
            }
        });
        B_CP3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nueva_ventana=new Intent(Seleccionar_CP.this,ActivitySections.class);
                nueva_ventana.putExtra("cp", B_CP3.getText().toString());
                startActivity(nueva_ventana);
            }
        });
        B_Pedir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createSimpleDialog().show();
            }
        });

        bCondiciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://www.auroraservicios.es/condiciones#condiciones-app-android";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
    }


    public AlertDialog createSimpleDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Información");
        builder.setMessage("Para crear una publicación póngase en contacto mediante el número 636 394 483 o el correo electrónico info@auroraservicios.es");
        builder.setCancelable(false);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        return builder.create();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass,Context c) {
        ActivityManager manager = (ActivityManager) c.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)){
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
