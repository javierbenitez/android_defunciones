package auroraservicios.es.aurora;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

import com.squareup.picasso.Picasso;

import modelos.Event;

public class ActivityImage extends AppCompatActivity {


    private static final String URL_MAIN = "http://servicios.auroraservicios.es/uploads/images/";
//    private static final String URL_MAIN = "https://www.auroraservicios.es/jbdodev1/auroraservicios/web/uploads/images/";
    private static final String URL_EVENT = URL_MAIN+"eventos/";
    private static final String URL_PLACE = URL_MAIN+"lugares/";
    private ImageButton imageButton;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_image);

        toolbar = findViewById(R.id.toolbarEv);
        imageButton = findViewById(R.id.imageButton);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        String path = getIntent().getStringExtra("path");
        String image = getIntent().getStringExtra("image");
        String subsection = getIntent().getStringExtra("subsection");


        String pathComplete = "";
        if(subsection.equals("Turismo")){
            pathComplete = URL_PLACE+path+image;
        }else{

        }

        Picasso.get()
                .load(pathComplete)
                .fit()
                .into(imageButton);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
