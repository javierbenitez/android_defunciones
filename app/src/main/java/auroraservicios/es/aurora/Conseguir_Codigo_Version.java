package auroraservicios.es.aurora;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import modelos.Publicacion;

public class Conseguir_Codigo_Version extends AsyncTask<Object, Integer, Void> {

    private Devolver_Codigo_Version respuesta;

    String urlConsulta = "https://auroraservicios.es/librerias/Codigo_Version.php";
    String charset = "UTF-8";
    URL connectURL;
    Context yo;
    Base_de_datos base_de_datos;
    Valores valores;
    int Codigo;


    Publicacion publicacion;

    public Conseguir_Codigo_Version(Devolver_Codigo_Version respuesta, Context yo) {
        this.respuesta = respuesta;
        this.yo=yo;
        base_de_datos=new Base_de_datos(this.yo);
        valores=new Valores(this.yo);
    }

    @Override
    protected void onPreExecute() {
//        try {
//            connectURL = new URL(urlConsulta+"cp="+stringSubsection);
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    protected Void doInBackground(Object... objects) {

        try {
                connectURL = new URL(urlConsulta);

                HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");//GET
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.connect();
                //extraemos los datos
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String resultadoConsulta = new String();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    resultadoConsulta += line;
                }
                JsonParser parser = new JsonParser();
                JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();

                base_de_datos.eliminar(base_de_datos.CP_4);
                for (JsonElement elemento: jsonArray) {
                    JsonObject objeto = elemento.getAsJsonObject();
                    String valor=objeto.get("codigo").getAsString();
                    Codigo = Integer.parseInt(valor);
                }
                connection.disconnect();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        respuesta.descarga_completada(Codigo);
    }



}
