package auroraservicios.es.aurora;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import modelos.Publicacion;

public class Base_de_datos {
    public final String CP_1="publicaciones_1";
    public final String CP_2="publicaciones_2";
    public final String CP_3="publicaciones_3";
    public final String CP_4="publicaciones_4";
    SQLiteDatabase Base_Datos;
    ArrayList<Publicacion> Lista_Publicaciones;
    Publicacion publicacion;
    Context context;
    String Nombre_Base_Datos="Base_Datos_Publicaciones_2";
    Valores valores;
    String tabla_1="CREATE TABLE if not exists [publicaciones_1] (\n" +
            "[identificador] INTEGER  ," +
            "[nombre] TEXT  ,\n" +
            "[apellidos] TEXT  ,\n" +
            "[apodo] TEXT  ,\n" +
            "[fecha_fallecimiento] TEXT  ,\n" +
            "[conyuge] TEXT  ,\n" +
            "[hijos] TEXT  ,\n" +
            "[hijos_politicos] TEXT  ,\n" +
            "[hermanos] TEXT  ,\n" +
            "[hermanos_politicos] TEXT  ," +
            "[familiares] TEXT  ," +
            "[pareja] TEXT  ," +
            "[foto] TEXT  ," +
            "[ceremonia] TEXT  ," +
            "[lugar] TEXT  ," +
            "[hora] TEXT  ," +
            "[texto] TEXT  ," +
            "[d_recibe] TEXT  ," +
            "[d_despide] TEXT  ," +
            "[lugar_iglesia] TEXT  ," +
            "[edad] TEXT  ," +
            "[direccion] TEXT ," +
            "[localidad_cerem2] TEXT ," +
            "[iglesia_cerem2] TEXT ," +
            "[fecha_cerem2] TEXT ," +
            "[hora_cerem2] TEXT ," +
            "[padres] TEXT ," +
            "[foto2] TEXT  ," +
            "[contact_phone1] TEXT  ," +
            "[contact_phone2] TEXT  " +
            ")";
    String tabla_2="CREATE TABLE if not exists [publicaciones_2] (\n" +
            "[identificador] INTEGER  ," +
            "[nombre] TEXT  ,\n" +
            "[apellidos] TEXT  ,\n" +
            "[apodo] TEXT  ,\n" +
            "[fecha_fallecimiento] TEXT  ,\n" +
            "[conyuge] TEXT  ,\n" +
            "[hijos] TEXT  ,\n" +
            "[hijos_politicos] TEXT  ,\n" +
            "[hermanos] TEXT  ,\n" +
            "[hermanos_politicos] TEXT  ," +
            "[familiares] TEXT  ," +
            "[pareja] TEXT  ," +
            "[foto] TEXT  ," +
            "[ceremonia] TEXT  ," +
            "[lugar] TEXT  ," +
            "[hora] TEXT  ," +
            "[texto] TEXT  ," +
            "[d_recibe] TEXT  ," +
            "[d_despide] TEXT  ," +
            "[lugar_iglesia] TEXT  ," +
            "[edad] TEXT  ," +
            "[direccion] TEXT ," +
            "[localidad_cerem2] TEXT ," +
            "[iglesia_cerem2] TEXT ," +
            "[fecha_cerem2] TEXT ," +
            "[hora_cerem2] TEXT ," +
            "[padres] TEXT ," +
            "[foto2] TEXT  ," +
            "[contact_phone1] TEXT  ," +
            "[contact_phone2] TEXT  " +
            ")";
    String tabla_3="CREATE TABLE if not exists [publicaciones_3] (\n" +
            "[identificador] INTEGER  ," +
            "[nombre] TEXT  ,\n" +
            "[apellidos] TEXT  ,\n" +
            "[apodo] TEXT  ,\n" +
            "[fecha_fallecimiento] TEXT  ,\n" +
            "[conyuge] TEXT  ,\n" +
            "[hijos] TEXT  ,\n" +
            "[hijos_politicos] TEXT  ,\n" +
            "[hermanos] TEXT  ,\n" +
            "[hermanos_politicos] TEXT  ," +
            "[familiares] TEXT  ," +
            "[pareja] TEXT  ," +
            "[foto] TEXT  ," +
            "[ceremonia] TEXT  ," +
            "[lugar] TEXT  ," +
            "[hora] TEXT  ," +
            "[texto] TEXT  ," +
            "[d_recibe] TEXT  ," +
            "[d_despide] TEXT  ," +
            "[lugar_iglesia] TEXT  ," +
            "[edad] TEXT  ," +
            "[direccion] TEXT ," +
            "[localidad_cerem2] TEXT ," +
            "[iglesia_cerem2] TEXT ," +
            "[fecha_cerem2] TEXT ," +
            "[hora_cerem2] TEXT ," +
            "[padres] TEXT ," +
            "[foto2] TEXT  ," +
            "[contact_phone1] TEXT  ," +
            "[contact_phone2] TEXT  " +
            ")";
    String tabla_4="CREATE TABLE if not exists [publicaciones_4] (\n" +
            "[identificador] INTEGER  ," +
            "[nombre] TEXT  ,\n" +
            "[apellidos] TEXT  ,\n" +
            "[apodo] TEXT  ,\n" +
            "[fecha_fallecimiento] TEXT  ,\n" +
            "[conyuge] TEXT  ,\n" +
            "[hijos] TEXT  ,\n" +
            "[hijos_politicos] TEXT  ,\n" +
            "[hermanos] TEXT  ,\n" +
            "[hermanos_politicos] TEXT  ," +
            "[familiares] TEXT  ," +
            "[pareja] TEXT  ," +
            "[foto] TEXT  ," +
            "[ceremonia] TEXT  ," +
            "[lugar] TEXT  ," +
            "[hora] TEXT  ," +
            "[texto] TEXT  ," +
            "[d_recibe] TEXT  ," +
            "[d_despide] TEXT  ," +
            "[lugar_iglesia] TEXT  ," +
            "[edad] TEXT  ," +
            "[direccion] TEXT ," +
            "[localidad_cerem2] TEXT ," +
            "[iglesia_cerem2] TEXT ," +
            "[fecha_cerem2] TEXT ," +
            "[hora_cerem2] TEXT ," +
            "[padres] TEXT ," +
            "[foto2] TEXT  ," +
            "[contact_phone1] TEXT  ," +
            "[contact_phone2] TEXT  " +
            ")";

    public Base_de_datos(Context context) {
        this.context = context;
        Base_Datos=context.openOrCreateDatabase(Nombre_Base_Datos,Context.MODE_PRIVATE,null);
        Base_Datos.execSQL("DROP TABLE IF EXISTS "+CP_1);
        Base_Datos.execSQL("DROP TABLE IF EXISTS "+CP_2);
        Base_Datos.execSQL("DROP TABLE IF EXISTS "+CP_3);
        Base_Datos.execSQL("DROP TABLE IF EXISTS "+CP_4);
        Base_Datos.execSQL(tabla_1);
        Base_Datos.execSQL(tabla_2);
        Base_Datos.execSQL(tabla_3);
        Base_Datos.execSQL(tabla_4);

        valores=new Valores(context);

        Lista_Publicaciones=new ArrayList<>();
    }

    public boolean insertar(Publicacion publicacion,String CP){

        Log.i("Cosneguir_Lista_guard","Identificador - "+publicacion.getIdentificador());
        ContentValues valores=new ContentValues();

        valores.put("identificador",publicacion.getIdentificador());
        valores.put("nombre",publicacion.getNombre());
        valores.put("apellidos",publicacion.getApellidos());
        valores.put("apodo",publicacion.getApodo());
        valores.put("direccion",publicacion.getDireccion());
        valores.put("fecha_fallecimiento",publicacion.getFecha_fallecimiento());
        valores.put("conyuge",publicacion.getMarido());
        valores.put("hijos",publicacion.getHijos());
        valores.put("hijos_politicos",publicacion.getHijos_politicos());
        valores.put("hermanos",publicacion.getHermanos());
        valores.put("hermanos_politicos",publicacion.getHermanos_politicos());
        valores.put("familiares",publicacion.getFamiliares());
        valores.put("pareja",publicacion.getPareja());
        valores.put("foto",publicacion.getFoto());
        valores.put("ceremonia",publicacion.getCeremonia());
        valores.put("lugar",publicacion.getLugar());
        valores.put("hora",publicacion.getHora());
        valores.put("texto",publicacion.getTexto());
        valores.put("d_recibe",publicacion.getD_recibe());
        valores.put("d_despide",publicacion.getD_despide());
        valores.put("lugar_iglesia",publicacion.getLugar_iglesia());
        valores.put("edad",publicacion.getEdad());
        valores.put("localidad_cerem2",publicacion.getLocalidadCerem2());
        valores.put("iglesia_cerem2",publicacion.getIglesiaCerem2());
        valores.put("fecha_cerem2",publicacion.getFechaCerem2());
        valores.put("hora_cerem2",publicacion.getHoraCerem2());
        valores.put("padres",publicacion.getPadres());
        valores.put("foto2",publicacion.getFoto2());
        valores.put("contact_phone1",publicacion.getPhone1());
        valores.put("contact_phone2",publicacion.getPhone2());

        Base_Datos.insert(CP,null,valores);
        Log.i("Cosneguir_Lista_guard","Publicacion - "+publicacion.toString()
        );

        return true;
    }
    public boolean eliminar(String CP){
        Base_Datos.delete(CP,null,null);
        return true;
    }
    public ArrayList<Publicacion> getLista_Publicaciones(){

        return Lista_Publicaciones;
    }
    public Publicacion getPublicacion(String DNI){

        return publicacion;
    }
    public ArrayList<Publicacion> getPublicaciones(String CP){
        Lista_Publicaciones.clear();
        Cursor cursor=Base_Datos.rawQuery("select * from publicaciones_4",null);
        Log.i("Cosneguir_Lista_3","entra - "+cursor.getCount());

        if (cursor!=null && cursor.getCount()>0){
            cursor.moveToFirst();
            do {
                setDataPublicacion(cursor);

            }while (cursor.moveToNext());
        }
        cursor.close();
        Cursor segundo_cursor;
        if (CP.equals(valores.get_Primer_cp())){
            segundo_cursor=Base_Datos.rawQuery("select * from publicaciones_1",null);
            Log.i("Cosneguir_Lista_carga_b","Cursor tamaño - "+segundo_cursor.getCount());
            if (segundo_cursor!=null && segundo_cursor.getCount()>0){
                segundo_cursor.moveToFirst();
                Log.i("Cosneguir_Lista_carga_b","Tabla - publicaciones_1");
                do {
                    Log.i("Cosneguir_Lista_carga_b","Posicion tamaño - "+segundo_cursor.getColumnCount());
                    setDataPublicacion(segundo_cursor);
                }while (segundo_cursor.moveToNext());
            }
        }else if (CP.equals(valores.get_Segundo_cp())){
            segundo_cursor=Base_Datos.rawQuery("select * from "+CP_2,null);
            if (segundo_cursor!=null && segundo_cursor.getCount()>0){
                segundo_cursor.moveToFirst();
                do {
                    setDataPublicacion(segundo_cursor);

                }while (segundo_cursor.moveToNext());
            }
        }else if (CP.equals(valores.get_Tercer_cp())){
            segundo_cursor=Base_Datos.rawQuery("select * from "+CP_3,null);
            if (segundo_cursor!=null && segundo_cursor.getCount()>0){
                segundo_cursor.moveToFirst();
                do {
                    setDataPublicacion(segundo_cursor);
                }while (segundo_cursor.moveToNext());
            }
        }
        return Lista_Publicaciones;
    }

    private void setDataPublicacion(Cursor cursor) {
        Publicacion publicacion = new Publicacion();
        publicacion.setIdentificador(cursor.getString(0));
        publicacion.setNombre(cursor.getString(1));
        publicacion.setApellidos(cursor.getString(2));
        publicacion.setApodo(cursor.getString(3));
        publicacion.setFecha_fallecimiento(cursor.getString(4));
        publicacion.setMarido(cursor.getString(5));
        publicacion.setHijos(cursor.getString(6));
        publicacion.setHijos_politicos(cursor.getString(7));
        publicacion.setHermanos(cursor.getString(8));
        publicacion.setHermanos_politicos(cursor.getString(9));
        publicacion.setFamiliares(cursor.getString(10));
        publicacion.setPareja(cursor.getString(11));
        publicacion.setFoto(cursor.getString(12));
        publicacion.setCeremonia(cursor.getString(13));
        publicacion.setLugar(cursor.getString(14));
        publicacion.setHora(cursor.getString(15));
        publicacion.setTexto(cursor.getString(16));
        publicacion.setD_recibe(cursor.getString(17));
        publicacion.setD_despide(cursor.getString(18));
        publicacion.setLugar_iglesia(cursor.getString(19));
        publicacion.setEdad(cursor.getString(20));
        publicacion.setDireccion(cursor.getString(21));
        publicacion.setLocalidadCerem2(cursor.getString(22));
        publicacion.setIglesiaCerem2(cursor.getString(23));
        publicacion.setFechaCerem2(cursor.getString(24));
        publicacion.setHoraCerem2(cursor.getString(25));
        publicacion.setPadres(cursor.getString(26));
        publicacion.setFoto2(cursor.getString(27));
        publicacion.setPhone1(cursor.getString(28));
        publicacion.setPhone2(cursor.getString(29));
        Lista_Publicaciones.add(publicacion);
    }

    public int Mas_Grande(String CP){
        Cursor cursor;
        if (CP.equals(valores.get_Primer_cp())){
            cursor=Base_Datos.rawQuery("select MAX(identificador) from "+CP_1,null);
        }else if(CP.equals(valores.get_Segundo_cp())){
            cursor=Base_Datos.rawQuery("select MAX(identificador) from "+CP_2,null);
        }else if(CP.equals(valores.get_Tercer_cp())){
            cursor=Base_Datos.rawQuery("select MAX(identificador) from "+CP_3,null);
        }else{
            cursor=Base_Datos.rawQuery("select MAX(identificador) from "+CP_4,null);
        }
        if (cursor!=null&&cursor.getCount()>0){
            cursor.moveToFirst();
            try {
                return Integer.parseInt(cursor.getString(0));
            }catch (Exception e){
                return 0;
            }
        }else{
            return 0;
        }
    }
}
