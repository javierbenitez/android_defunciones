package auroraservicios.es.aurora;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class WebPayment extends AppCompatActivity {

    Bundle bundle;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_payment);

        String payment = getIntent().getStringExtra("payment");
        String name = getIntent().getStringExtra("name");
        String lastname = getIntent().getStringExtra("lastname");
        String age = getIntent().getStringExtra("age");
        String date = getIntent().getStringExtra("date");

        WebView myWebView = findViewById(R.id.wvPayment);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.setWebViewClient(new WebViewClient());
        String urlWeb = "https://auroraservicios.es/payment_misa.php?p1="+payment+"&p2="+name+"&p3="+lastname+"&p4="+age+"&p5="+date;
        myWebView.loadUrl(urlWeb);

    }

    class WebServiceTask extends AsyncTask<Misa, Void, String> {

        Context ctx;
        ProgressDialog pDialog;
        String resultJSON = null;

        @Override
        protected String doInBackground(Misa... misas) {

            String text = "";

            return text;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(WebPayment.this);
            pDialog.setMessage("Procesando, espere...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(WebPayment.this, result,Toast.LENGTH_LONG).show();

            pDialog.dismiss();
        }

    }
}
