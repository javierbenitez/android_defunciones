package auroraservicios.es.aurora;

import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import adapters.PlaceAdapter;
import modelos.Place;
import modelos.Subsection;

public class ActivityPartsOfPlace extends AppCompatActivity implements Devolver_Listado_Publicidad {

    private GridView gvPlaces;
    private String stringSubsection, stringCp, encodedurl;

    private TextView tvSubsection, tvNoPlaces, tvTitlePlace;

    private ArrayList itemsPlaces;
    private PlaceAdapter placeAdapter;
    private Place place;

    //Publicidad
    private Conseguir_Lista_Publicidad conseguir_lista_publicidad;
    private ViewPager Banner;
    private ArrayList<String> Fotos;
    private Display display;
    private int currentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);

        Toolbar toolbar = findViewById(R.id.toolbarPlaces);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        stringSubsection = getIntent().getExtras().getString("subsection");
        place = getIntent().getExtras().getParcelable("place");
        stringCp = getIntent().getExtras().getString("cp");

        // Publicidad
        conseguir_lista_publicidad=new Conseguir_Lista_Publicidad(this,this, stringCp);
        conseguir_lista_publicidad.execute();
        Banner = findViewById(R.id.banner);
        Fotos = new ArrayList<>();
        display = getWindowManager().getDefaultDisplay();

        gvPlaces = findViewById(R.id.gvPlaces);
        itemsPlaces = new ArrayList();
        tvSubsection = findViewById(R.id.tvSubsection);
        tvTitlePlace = findViewById(R.id.tvTitlePlace);
        tvNoPlaces = findViewById(R.id.tvNoPlaces);
        tvSubsection.setText("Partes del lugar " +" de "+stringCp);

        tvTitlePlace.setText(place.getTitle());

        new WebServiceSsubsections().execute("Parts_of_place");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menuWeb:
                openWebAurora();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openWebAurora(){
        String url = "https://servicios.auroraservicios.es";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        this.startActivity(i);
    }

    public class WebServiceSsubsections extends AsyncTask<String, Integer, Void> {

        String urlConsulta = "http://www.servicios.auroraservicios.es/api/v1/places/partsOfPlace";
//        String urlConsulta = "https://www.auroraservicios.es/jbdodev1/auroraservicios/web/app_dev.php/api/v1/places/partsOfPlace";
        URL connectURL;

        @Override
        protected void onPreExecute() {

            try {
                encodedurl = urlConsulta+
                        "?p="+URLEncoder.encode(String.valueOf(place.getId()),"UTF-8");
                connectURL = new URL(encodedurl);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... objects) {

            try {
                //            ------------------ CODIGO GROBAL----------------------------
                HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");//GET
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.connect();

                //extraemos los datos
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String resultadoConsulta = new String();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    resultadoConsulta += line;
                }

                JsonParser parser = new JsonParser();
                JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
                try {
                    itemsPlaces.clear();
                } catch (Exception e) {
                }

                for (JsonElement elemento : jsonArray) {
                    JsonObject objeto = elemento.getAsJsonObject();
                    Place place = new Place();
                    Subsection subsection = new Subsection("LUGAR");
                    place.setSubsection(subsection);
                    place.setTitle(checkValueNull(objeto, "place"));
                    place.setId(objeto.get("id").getAsInt());
                    place.setDescription(checkValueNull(objeto, "description"));
                    place.setLatitud(checkValueNull(objeto, "lat"));
                    place.setLongitud(checkValueNull(objeto, "lon"));
                    place.setPath(checkValueNull(objeto, "path"));
                    place.setVideo(checkValueNull(objeto, "video"));
                    place.setAudio(checkValueNull(objeto, "audio"));
                    place.setImage(checkValueNull(objeto, "image"));
                    itemsPlaces.add(place);
                }
                placeAdapter = new PlaceAdapter(ActivityPartsOfPlace.this, itemsPlaces, stringCp);
                connection.disconnect();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(itemsPlaces.size() == 0){
                tvNoPlaces.setVisibility(View.VISIBLE);
            }else{
                gvPlaces.setAdapter(placeAdapter);
            }
        }
    }

    private String checkValueNull(JsonObject objeto, String value){
        if(!objeto.has(value) || objeto.get(value).isJsonNull())return "";
//        if(objeto.get(value).getAsString().equals("null") || objeto.get(value).getAsString() == "null")
//            return "";
        return objeto.get(value).getAsString();
    }

    @Override
    public void onTaskCompleted_publicidad(ArrayList<String> Publicidad) {
        if (Publicidad!=null){

            Fotos = Publicidad;
            if(Fotos.size() < 1) {
                Banner.setVisibility(View.GONE);
            }else{
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.height = FrameLayout.LayoutParams.MATCH_PARENT; //left, top, right, bottom
                Banner.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
                Point size = new Point();
                display.getSize(size);
            }

            Banner.setAdapter(new Adaptador_Banner(this,Fotos));

            Banner.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    super.onPageScrolled(position, positionOffset, positionOffsetPixels);

                }

                @Override
                public void onPageSelected(final int position) {
                    super.onPageSelected(position);
                    currentPage=position;
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                    super.onPageScrollStateChanged(state);
                }
            });
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == Fotos.size()) {
                        currentPage = 0;
                    }else{
                        currentPage++;
                    }
                    Banner.setCurrentItem(currentPage, true);
                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 5000, 5000);
        }
    }
}
