package auroraservicios.es.aurora;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import modelos.Info;
import modelos.Subsection;

public class ActivityInfo extends AppCompatActivity {

    private Info info;
    private ActivityInfo activity;

    private TextView tvName, tvDescription, tvSchedule, tvFilibeg, tvPhone1, tvPhone2, tvAddress, tvLocality, tvUrl,
            tv_Schedule, tv_Filibeg, tv_Phone1, tv_Phone2, tv_Address, tv_Locality, tv_Url;

    private ImageView ivInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        Toolbar toolbar = findViewById(R.id.toolbarInfo);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        info = getIntent().getExtras().getParcelable("info");
        ivInfo = findViewById(R.id.ivInfo);
        tvSchedule = findViewById(R.id.tvSchedule);
        tvFilibeg = findViewById(R.id.tvFilibeg);
        tvPhone1 = findViewById(R.id.tvPhone1);
        tvPhone2 = findViewById(R.id.tvPhone2);
        tvAddress = findViewById(R.id.tvAddress);
        tvUrl = findViewById(R.id.tvUrl);
        tvLocality = findViewById(R.id.tvLocality);

        if(info.getImage() == null || info.getImage() == "" ||
                info.getImage().equals(null) || info.getImage().equals("")){
            ivInfo.setVisibility(View.GONE);
        }else{
            String urlPhoto = "http://www.servicios.auroraservicios.es/uploads/images/info/"+info.getPath()+info.getImage();
            urlPhoto = urlPhoto.replaceAll(" ", "%20");
            Picasso.get()
                    .load(urlPhoto)
                    .resize(240, 240)
                    .centerCrop()
                    .into(ivInfo);
        }

        tvName = findViewById(R.id.tvName);
        tvName.setText(info.getName());

        tvDescription = findViewById(R.id.tvDescription);
        tvDescription.setText(info.getDescription());

        tv_Schedule = findViewById(R.id.tv_Schedule);
        tv_Filibeg = findViewById(R.id.tv_Filibeg);
        tv_Phone1 = findViewById(R.id.tv_Phone1);
        tv_Phone2 = findViewById(R.id.tv_Phone2);
        tv_Address = findViewById(R.id.tv_Address);
        tv_Locality = findViewById(R.id.tv_Locality);
        tv_Url = findViewById(R.id.tv_Url);

        if(!deleteLabelIfNull(tvSchedule, tv_Schedule, info.getSchedule())){
            tvSchedule.setText(info.getSchedule());
        }

        if(!deleteLabelIfNull(tvFilibeg, tv_Filibeg, info.getFilibeg())){
            tvFilibeg.setText(info.getFilibeg());
        }

        if(!deleteLabelIfNull(tvPhone1, tv_Phone1, info.getPhone1())){
            tvPhone1.setText(info.getPhone1());
        }

        if(!deleteLabelIfNull(tvPhone2, tv_Phone2, info.getPhone2())){
            tvPhone2.setText(info.getPhone2());
        }

        if(!deleteLabelIfNull(tvAddress, tv_Address, info.getAddress())){
            tvAddress.setText(info.getAddress());
        }

        if(!deleteLabelIfNull(tvLocality, tv_Locality, info.getLocalidad())){
            tvLocality.setText(info.getLocalidad());
        }

        if(!deleteLabelIfNull(tvUrl, tv_Url, info.getUrl())){
            tvUrl.setText(info.getUrl());
        }

        tvPhone1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int permissionCheck = ContextCompat.checkSelfPermission(
                        ActivityInfo.this, Manifest.permission.CALL_PHONE);
                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    Log.i("Mensaje", "No se tiene permiso para realizar llamadas telefónicas.");
                    ActivityCompat.requestPermissions(ActivityInfo.this, new String[]{Manifest.permission.CALL_PHONE}, 225);
                } else {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + info.getPhone1()));
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(callIntent);
                }
            }
        });

        tvPhone2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int permissionCheck = ContextCompat.checkSelfPermission(
                        ActivityInfo.this, Manifest.permission.CALL_PHONE);
                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    Log.i("Mensaje", "No se tiene permiso para realizar llamadas telefónicas.");
                    ActivityCompat.requestPermissions(ActivityInfo.this, new String[]{Manifest.permission.CALL_PHONE}, 225);
                } else {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + info.getPhone2()));
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(callIntent);
                }
            }
        });

        tvUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try
                {
                    Uri uri = Uri.parse(info.getUrl());
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } catch (Exception e){

                }
            }
        });

        new WebServiceCompany().execute("info_info");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class WebServiceCompany extends AsyncTask<String, Integer, Void> {

        String urlConsulta = "http://www.servicios.auroraservicios.es/api/v1/infos/info" +
                "?info="+ info.getId();
        URL connectURL;

        @Override
        protected void onPreExecute() {
            try {
                connectURL = new URL(urlConsulta);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... objects) {

            try {
                //            ------------------ CODIGO GROBAL----------------------------
                HttpURLConnection connection = (HttpURLConnection) connectURL.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");//GET
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.connect();

                //extraemos los datos
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String resultadoConsulta = new String();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    resultadoConsulta += line;
                }

//                JsonParser parser = new JsonParser();
//                JsonArray jsonArray = parser.parse(resultadoConsulta).getAsJsonArray();
//                try {
////                    itemsCompany.clear();
//                } catch (Exception e) {
//                }

//                for (JsonElement elemento : jsonArray) {
//                    JsonObject objeto = elemento.getAsJsonObject();
//                    Subsection subsection = new Subsection(objeto.get("subsection").getAsString());
//                    Company company = new Company(objeto.get("name").getAsString());
//                    company.setNameContact(objeto.get("name_contact").getAsString());
//                    company.setLastNameContact(objeto.get("lastname_contact").getAsString());
//                    company.setPhone1(objeto.get("phone1").getAsString());
//                    company.setPhone2(objeto.get("phone2").getAsString());
//                    company.setAddress(objeto.get("address").getAsString());
//                    company.setVideo(objeto.get("url").getAsString());
//                    company.setSubsection(subsection);
//                    company.setLocalidad(objeto.get("localidad").getAsString());
//                    company.setImage(checkValueNull(objeto, "image"));
//                    itemsCompany.add(company);
//                }
//                companyAdapter = new CompanyAdapter(ActivityCompany.this, itemsCompany, stringCp);
                connection.disconnect();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

        }
    }

    private String checkValueNull(JsonObject objeto, String value){
        if(objeto.get(value).isJsonNull())return "";
        if(objeto.get(value).getAsString().equals("null") || objeto.get(value).getAsString() == "null")
            return "";
        return objeto.get(value).getAsString();
    }

    private boolean deleteLabelIfNull(TextView label, TextView field, String value) {
        if(value.equals("")){
            label.setVisibility(View.GONE);
            field.setVisibility(View.GONE);
            return true;
        }
        return false;
    }

}
