package auroraservicios.es.aurora;

public class TokenUsuario {

    private String token;
    private String soDevice;

    public TokenUsuario() {
    }

    public TokenUsuario(String token, String soDevice) {
        this.token = token;
        this.soDevice = soDevice;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSoDevice() {
        return soDevice;
    }

    public void setSoDevice(String soDevice) {
        this.soDevice = soDevice;
    }
}
